# UpSpace QL Syntax Description

- literal: number, string, boolean, date-string, regexp-string
- identifier
- function: identifier + '(' + List(',', expression) + ')'
- object: identifier | function
- chain: object + List('.', object | '[' + expression + ']')
- value: chain | literal

- unary-operators: NOT, -
- arithmetic-operators: *, /, %, ^, +, -
- boolean-operators: ==, <, <=, >, >=, !=, ~=,
- logical-operators: AND, OR

- unary-expression: unary-operator + expression
- arithmetic-expression: expression + arithmetic-operator + expression
- boolean-expression: expression + boolean-operator + expression
- logical-expression: expression + logical-operator + expression

- expression: value | '(' + expression + ')' | unary-expression | arithmetic-expression | boolean-expressions | logical-expression
