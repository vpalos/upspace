import sbtassembly.Plugin.AssemblyKeys
import AssemblyKeys._

assemblySettings

test in assembly := {}

assemblyOption in assembly ~= { _.copy(prependShellScript = Some(defaultShellScript)) }

jarName in assembly := { s"${name.value}" }