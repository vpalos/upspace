package com.oneandone.persistence.upspace.ql

import scopt._

object Main {

    val Name = "UpSpaceQL Parser"
    val Version = "1.0"
    val Title = s"\n[ $Name, Version $Version ]\n"

    val parser = new OptionParser[Options](s"upspace-ql") {

        head(Title)

        opt[String]('p', "parse")
            .valueName("<query>")
            .text("Parse the given query for correctness.")
            .action { (x, c) => c.copy(action = "parse", query = x) }

        version("version").text("prints the current version")

        help("help").text("print this usage information")

    }

    def main(args: Array[String]) {

        parser.parse(args, Options()) map { options =>
            println(Title)

            // Just print some nice data...
            options.action match {
                case "parse" =>
                    println("Parsing...")
                case _ =>
                    throw new IllegalArgumentException("Unsure about the course of action!")
            }

        } getOrElse {
            throw new IllegalArgumentException(s"Unable to understand options. Try '--help' for details!")
        }

    }

}
