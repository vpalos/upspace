package com.oneandone.persistence.upspace.ql

import scala.collection.JavaConverters._
import scala.util.parsing.combinator._

/**
 * UpSpace Query Language parser class.
 */
class QLParser extends JavaTokenParsers {

    // case insensitive terminals
    private class CIString(str: String) {
        def ci: Parser[String] = ("""(?i)\Q""" + str + """\E""").r
    }
    implicit private def stringToCIString(s: String) = new CIString(s)

    // common terminals
    val boolean       = "false".ci | "true".ci
    val number        = floatingPointNumber | decimalNumber | wholeNumber
    val string        = stringLiteral ^^ { _.replaceAll("(^\"|\\\\|\"$)", "") }
    val date          = """\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z""".r

    val literal       = number | boolean | string | date

    val identifier    = ident
    val function      = identifier ~ ("(" ~> repsep(expression, ",") <~ ")")
    val segment       = identifier | function
    val entity        = segment ~ rep("." ~> segment)

    val arithmetic_op = "*" | "/" | "%" | "^" | "+" | "-"
    val boolean_op    = "==" | "<" | "<=" | ">" | ">=" | "!=" | "~=" | "~!="
    val logical_op    = "and".ci | "or".ci
    val binary_ops    = arithmetic_op | boolean_op | logical_op
    val unary_ops     = "not".ci

    val isolated_expr = "(" ~> expression <~ ")"
    val unary_expr    = unary_ops ~ expression
    val binary_expr   = expression ~ binary_ops ~ expression
    val expression: Parser[String]    = literal | entity | isolated_expr | unary_expr | binary_expr

    // doc.abc.lowercase()
}




/**
 * Query parser singleton toolkit.
 */
object QLParser {

    def apply(query:String) = {



        true
    }

}

