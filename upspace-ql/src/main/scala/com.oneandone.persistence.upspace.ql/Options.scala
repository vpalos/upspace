package com.oneandone.persistence.upspace.ql

case class Options(action: String = "",
                   query: String = "")