import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.Keys._

name := "scala-script-poc"

version := "1.0"

packageArchetype.java_application

packageDescription in Debian := "Proof-of-Concept Script"

maintainer in Debian := "Valeriu Paloş"

organization := "com.oneandone"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.2.0"

