package com.oneandone.upspace.handlers

import com.oneandone.upspace.drivers._
import com.oneandone.upspace.helpers._
import org.json4s._
import org.scalatra._
import org.scalatra.swagger._

/**
 * The main UpSpace servlet class.
 * In Scalatra the entire routing mechanism is moved from the
 * regular "web.xml" file into code (i.e. here).
 * @see http://www.scalatra.org/2.2/guides/http/routes.html
 */
class SpaceHandler(name: String, space: Driver)
                  (implicit val swagger: Swagger)
    extends ServletHelper with SwaggerSupport {

    override protected val applicationName = Some(if (name.head == '/') name.tail else name)
    protected val applicationDescription = s"This is the UpSpace API for accessing the '$name' storage space."
    private val jsonField = ModelField("", "Any JSON fields.", DataType("type..."), required = false)
    private val jsonDocumentModel = Model("JsonDocument", "A valid JSON document.", Map("field..." -> jsonField))

    private val validMimePattern = """(^|,)\s*(\*|application)/(\*|json)\s*(,|;|$)""".r
    private val validPathPattern = """^(/[^~][^/]*)+$""".r

    before("*") {
        contentType = formats("json")
        response.headers += ("Access-Control-Allow-Origin" -> "*")

        // todo: make access-control-allow-origin configuratble
        // todo: access control

        val path = sanitizePath(requestPath)

        if (validPathPattern.findFirstIn(path) == None) {
            respond(BadRequest())
        }

        request.header("Accept") match {
            case None => respond(NotAcceptable())
            case Some(header) =>
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(NotAcceptable())
                }
        }

        request.header("Content-Type") match {
            case None => {
                format = "json"
                log.warn("Request lacks 'Content-Type' header; expecting 'application/json'.")
            }
            case Some(header) => {
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(UnsupportedMediaType())
                }
            }
        }

        if (request.body.nonEmpty && !parsedBody.isInstanceOf[JsonAST.JObject]) {
            respond(BadRequest())
        }

        // todo: check charset=UTF-8 in headers
    }

    val helpGet = (apiOperation("getDocs", jsonDocumentModel)
        summary "Retrieve the document at the given path."
        notes s"Retrieves the document corresponding to the given path identifier from the '$name' storage space."
        error Error(415, "The 'Content-Type' header must be 'application/json'.")
        error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
        error Error(404, "The specified document was not found.")
        error Error(400, "Illegal document path or request body is not valid JSON.")
        error Error(200, "The document was retrieved successfully; expect response to be valid JSON.")
        parameter pathParam[String]("path")
            .description("The document path identifier (path segments may not start with tilda, i.e. '~').")
            .defaultValue("a/b/c")
        parameter headerParam[String]("Accept")
            .description("The mime-type accepted by the client.")
            .allowableValues[String](List("application/json", "*/*"))
            .defaultValue("application/json")
    )
    get("/:path", operation(helpGet)) {
        respond {
            val path = sanitizePath(params.getOrElse("path", ""))
            space.get(path)
        }
    }

    val helpPost = (apiOperation("getDocs", jsonDocumentModel)
        summary "Retrieve the document at the given path."
        notes s"Retrieves the document corresponding to the given path identifier from the '$name' storage space."
        error Error(415, "The 'Content-Type' header must be 'application/json'.")
        error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
        error Error(404, "The specified document was not found.")
        error Error(400, "Illegal document path or request body is not valid JSON.")
        error Error(200, "The document was retrieved successfully; expect response to be valid JSON.")
        parameter pathParam[String]("path")
            .description("The document path identifier (path segments may not start with tilda, i.e. '~').")
            .defaultValue("a/b/c")
        parameter headerParam[String]("Accept")
            .description("The mime-type accepted by the client.")
            .allowableValues[String](List("application/json", "*/*"))
            .defaultValue("application/json")
    )
    post("/*", operation(helpPost)) {
        val path = sanitizePath(params("splat"))
        val completePath = request.getContextPath + request.getServletPath + path
        val expire = request.headers.getOrElse("X-Param-Expire", "0").toInt
        val jsonBody = sanitizeJson(parsedBody)

        val result = space.create(path, jsonBody, expire)
        result match {
            case OpSuccess(_) => {
                respond(Created(headers = Map("Location" -> s"$completePath")))
            }
            case _ => {
                respond(result)
            }
        }
    }

    put("/*") {
        val path = sanitizePath(params("splat"))
        val expire = request.headers.getOrElse("X-Param-Expire", "0").toInt
        val merge = request.headers.getOrElse("X-Param-Merge", "false").toBoolean
        val jsonBody = sanitizeJson(parsedBody)
        respond {
            space.update(path, jsonBody, expire, merge)
        }
    }

    delete("/*") {
        respond {
            val path = sanitizePath(params("splat"))
            space.delete(path)
        }
    }

    notFound {
        contentType = null
        resourceNotFound()
    }
}