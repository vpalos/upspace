package com.oneandone.upspace.drivers

import com.couchbase.client._
import com.couchbase.client.protocol.views._
import com.oneandone.upspace.helpers._
import com.typesafe.config._
import java.util.concurrent._
import java.util.Date
import net.spy.memcached._
import org.json4s._
import org.json4s.jackson._
import org.json4s.jackson.JsonMethods._
import org.slf4j._
import scala.collection.JavaConversions._
import scala.Predef._

/**
 * This class hides all Couchbase related functionalities.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(connectionConfig: Config, historyConfig: Config)
                     (implicit user: () => User)
    extends Driver
    with DriverHelper {

    protected val log = LoggerFactory.getLogger(getClass)

    implicit private val formats = DefaultFormats

    private val timeout = connectionConfig.getMilliseconds("timeout")
    private val historyEnabled = historyConfig.getBoolean("enabled")
    private val client = {
        val nodes = connectionConfig.getStringList("nodes")
        val bucket = connectionConfig.getString("bucket")
        val password = connectionConfig.getString("password")
        val uris = nodes map java.net.URI.create
        new CouchbaseClient(uris, bucket, password)
    }

    private def ensureView(designName: String, viewName: String, viewBody: String) {
        try {
            client.getView(designName, viewName)
        } catch {
            case _: InvalidViewException => {
                val designDocument = new DesignDocument(designName)
                val viewDesign = new ViewDesign(viewName, viewBody)
                designDocument.getViews.add(viewDesign)
                client.createDesignDoc(designDocument)
            }
        }
    }

    private def catchFailure(op: => OpResult): OpResult = {
        try {
            op
        } catch {
            case _:TimeoutException => OpTimeout()
            case _:IllegalStateException => OpOverload()
            case _:Throwable => OpFailure()
        }
    }

    private def logHistory(path: String,
                           action: String,
                           timestamp: Long,
                           body: JValue,
                           result: Option[JValue] = None)
                          (params: Map[String, AnyRef]) {
        if (!historyEnabled) {
            return
        }

        val document = Map("action" -> action,
                            "user" -> user().id,
                            "params" -> params,
                            "body" -> body,
                            "result" -> result.orElse(null))

        val document = s"""{ "action": "$action",
                             "user": "${user().id}",
                             "params": ${Serialization.write(params)},
                             "result": ${compact(result)} }"""

        retryOp(5) {
            (cycle) =>
                client.add(s"/~history$path/$timestamp.$cycle", 0, document).get(timeout, TimeUnit.MILLISECONDS)
        } match {
            case 0 => log.error(s"Failed to persist history for document with path : $path")
            case _ =>
        }
    }

    override def get(path: String, expire: Option[Int] = None): OpResult = {
        catchFailure {
            val timestamp = System.currentTimeMillis

            val future = expire match {
                case Some(ts) => client.asyncGetAndTouch(path, ts)
                case None => client.asyncGet(path)
            }

            future.get(timeout, TimeUnit.MILLISECONDS) match {
                case null => OpMissing()
                case data =>
                    logHistory(path, "touch", )
                    OpSuccess(parse(data.toString).noNulls)
            }
        }
    }

    override def create(path: String, json: JValue, expire: Option[Int] = None): OpResult = {
        catchFailure {
            val body = json.noNulls

            val timestamp = System.currentTimeMillis
            val future = expire match {
                case Some(ts) => client.add(path, ts, compact(body))
                case None => client.add(path, compact(body))
            }

            if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                logHistory(path, "create", timestamp, body) {
                    Map("expire" -> expire.toString)
                }
                OpSuccess()
            } else {
                OpConflict()
            }
        }
    }

    override def update(path: String, json: JValue, expire: Option[Int] = None, merge: Option[Boolean] = None): OpResult = {
        catchFailure {
            val timestamp = System.currentTimeMillis
            merge match {
                case Some(true) =>
                    val casValue = client.asyncGets(path).get(timeout, TimeUnit.MILLISECONDS)
                    casValue.getValue match {
                        case null => OpMissing()
                        case data => {
                            val oldJson = parse(data.toString)
                            val casCode = casValue.getCas
                            val newJson = (oldJson merge json).noNulls

                            val future = expire match {
                                case Some(ts) => client.asyncCAS(path, casCode, ts, compact(newJson), client.getTranscoder)
                                case None => client.asyncCAS(path, casCode, compact(newJson), client.getTranscoder)
                            }

                            future.get(timeout, TimeUnit.MILLISECONDS) match {
                                case CASResponse.OK => {
                                    logHistory(path, "update", timestamp, newJson) {
                                        Map("expire" -> expire.toString,
                                            "merge" -> merge.toString,
                                            "mergeBody" -> json)
                                    }
                                    OpSuccess()
                                }
                                case CASResponse.NOT_FOUND => OpMissing()
                                case _ => OpOverload()
                            }
                        }
                    }
                case _ =>
                    val body = json.noNulls

                    val future = expire match {
                        case Some(ts) => client.replace(path, ts, compact(body))
                        case None => client.replace(path, compact(body))
                    }

                    if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                        logHistory(path, "update", timestamp, body) {
                            Map("expire" -> expire.toString)
                        }
                        OpSuccess()
                    } else {
                        OpMissing()
                    }
            }
        }
    }

    override def delete(path: String): OpResult = {
        catchFailure {
            val timestamp = System.currentTimeMillis
            if (client.delete(path).get(timeout, TimeUnit.MILLISECONDS)) {
                logHistory(path, "delete", timestamp, parse("{}")) {
                    Map.empty
                }
                OpSuccess()
            } else {
                OpMissing()
            }
        }
    }

    override def shutdown() {
        client.shutdown(timeout, TimeUnit.MILLISECONDS)
    }

    override def history(path: String, from: Date, to: Date, direction: SortDirection): OpResult = {
        val view = client.getView(historyDesignDocumentName, historyViewName)
        val query = new Query()
        query.setIncludeDocs(true)
        query.setStale(Stale.FALSE)
        direction match {
            case Ascending() => {
                query.setDescending(false)
                query.setRangeStart(ComplexKey.of(path, from.getTime.toString, "0"))
                query.setRangeEnd(ComplexKey.of(path, to.getTime.toString, Long.MaxValue.toString))
            }
            case Descending() => {
                query.setDescending(true)
                query.setRangeStart(ComplexKey.of(path, to.getTime.toString, Long.MaxValue.toString))
                query.setRangeEnd(ComplexKey.of(path, from.getTime.toString, "0"))
            }
        }

        val result : ViewResponse = client.query(view, query)
        // todo move this part in couchbase in reduse
        var historyList = parse("""{"history": []}""")
        for(row <- result) {
            val document = row.getValue
            historyList = historyList merge parse(s"""{"history": [$document]}""")
        }

        OpSuccess(historyList)
    }

    ensureView(historyDesignDocumentName, historyViewName, loadResource(s"views/history-get.js"))
}
