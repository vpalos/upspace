package com.oneandone.upspace.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import java.text.ParseException
import org.scalatra._
import org.scalatra.swagger._
import java.util.Date
import com.oneandone.upspace.managers.ConfigManager

/**
 * Handle Search operations for space servlets.
 */
trait SearchHandler
    extends ServletHelper
    with SearchHelper
    with HelpHelper { self: SpaceServlet =>

    private def catchFailures(op: => OpResult): OpResult = {
        try {
            op
        } catch {
            case e: ParseException => OpComplain(e.getMessage)
        }
    }

    val searchHelp = (apiOperation("globHelp", jsonDocumentModel)
        summary "Retrieve the documents based on filtering rules."

        notes
        """Retrieves a set of document from the database based on a path prefix.
          |Optionally it will also reduce this document based on a set of filtering rules.
          |For now returned documents are always returned in sorted order of their path.
          |To use filtering, you need to pass a query parameter in the form of:
          |<b>[PropertyType:]object.foo.bar=[Operator]value</b>
          |<br />
          |PropertyType is optional and can be one of the following: <br />
          |&nbsp;&nbsp;<b>s</b> - String (Default) <br />
          |&nbsp;&nbsp;<b>n</b> - Number <br />
          |&nbsp;&nbsp;<b>b</b> - Boolean <br />
          |&nbsp;&nbsp;<b>d</b> - Date <br />
          |Operator is also optional and can be one of the following: <br />
          |&nbsp;&nbsp;<b>=</b> - Property <b>equal</b> with the value. (Default)<br />
          |&nbsp;&nbsp;<b>!</b> - Property <b>not equal</b> with the value.<br />
          |&nbsp;&nbsp;<b>~</b> - Property <b>matched</b> with the RegExp value.<br />
          |&nbsp;&nbsp;<b>></b> - Property <b>greater than</b> value.<br />
          |&nbsp;&nbsp;<b><</b> - Property <b>less than</b> value.<br />
          |<br />
          |You can use multiple filter conditions. In that case when you use =, ! and ~,
          |searching returns all documents that matches <b>any</b>(logical or) of the three condition.
          |When using > or <, it returns all documents that match all (logical and) conditions.
          |<br/>
          |Filtering example:<br/>
          |http://localhost:10080/upspace/document/a?n:age=<40<br/>
          |http://localhost:10080/upspace/document/a?s:foo==bar<br/>
          |http://localhost:10080/upspace/document/a?s:tool=hammer<br/>
          |
          |
        """.stripMargin

        error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
        error Error(403, "Authorization error; operation was forbidden (not an authentication issue).")
        error Error(401, "Authentication failed; valid credentials must be supplied.")
        error Error(400, "Malformed request (illegal query rules, invalid JSON body etc.).")
        error Error(202, "The system is indexing your search; retry in a few moments.")
        error Error(200, "The search completed successfully; expect response to be valid JSON.")

        parameter pathParam[String]("prefix")
            .description("The path prefix identifier (path segments may not start with tilda, i.e. '~').")
            .defaultValue("a")
            .required

        parameter queryParam[String]("s:" +
        "foo")
        .description("""The filtering parameter.""")
        .defaultValue("=bar")

        parameter queryParam[String]("direction")
        .description(
        """
          |The direction in which the history entries are sorted after the time they were recorded.
          |Only the first letter is considered (i.e. 'a' and 'asc' are identical).
          |If missing they are sorted descending first being the oldest one."
        """.stripMargin)
        .allowableValues[String](List("asc", "desc"))
        .defaultValue("desc")
        .optional

        parameter queryParam[String]("sort")
        .description("""Not yet implemented.""")

        parameter queryParam[String]("[type:]document.field")
        .description("""Not yet implemented.""")

        parameter queryParam[Boolean]("count")
        .description("""Only return the total number of documents in the result (limit and offset are ignored).""")

        parameter queryParam[Int]("depth")
        .description(
        """Specify the rendering depth of the result: 0 = return an array of document keys,
          | 1 = return a dictionary with all the documents with paths as keys
          | (other values are not yet supported.""")

        parameter queryParam[Int]("limit")
        .description(
        s"""Only return this many documents in the final result
          |(default is ${ConfigManager.options.getInt("search.limit.default")},
          |maximum is ${ConfigManager.options.getInt("search.limit.maximum")}).""".stripMargin)

        parameter queryParam[Int]("offset")
        .description("""Truncate the result set by skipping this amount of documents from the beginning.""")

        parameter headerParam[String]("Accept")
            .description("The mime-type accepted by the client.")
            .allowableValues[String](List("application/json", "*/*"))
            .defaultValue("application/json")
            .required

        parameter headerParam[String]("Authorization")
            .description(
                """
                  |The HTTP Auth header (only required if 'security' enabled).
                  |This value must follow the syntax of the authentication strategy set in 'auth.strategy'.
                  |Only the <a href='http://en.wikipedia.org/wiki/Basic_auth'>'http-basic' strategy</a>
                  |is supported for now (i.e. <i>user:password</i> must be
                  |<a href='http://www.base64encode.org/'>Base64 encoded</a>).
                """.stripMargin)
            .defaultValue("Basic Base64(user:password)")
        )
    get("/~search/:prefix", operation(searchHelp)) { pass() }

    get("/~search/*") {
        respond {
            catchFailures {
                val (rules, sort, direction, offset, limit, depth, count) = compileQuery

                driver.search(sanitizedPath(), rules, sort, direction, offset, limit, depth, count)
            }
        }
    }
    get("/~search/?") {
        respond {
            OpComplain("Unconstrained searches are illegal; please provide a path prefix!")
        }
    }

    post("/~search/*") { MethodNotAllowed() }
    post("/~search/?") { MethodNotAllowed() }
    put("/~search/*") { MethodNotAllowed() }
    put("/~search/?") { MethodNotAllowed() }
    delete("/~search/*") { MethodNotAllowed() }
    delete("/~search/?") { MethodNotAllowed() }

}
