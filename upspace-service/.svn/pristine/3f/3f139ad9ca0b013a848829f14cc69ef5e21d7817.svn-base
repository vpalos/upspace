#
# UpSpace configuration.
#

#
# Service options.
#
options {

    #
    # Expose a Swagger JSON reference under the "/~help" URL
    # (regardless of space configuration). Interpret this URL
    # with a swagger-ui instance (http://swagger.wordnik.com).
    #
    help.enabled = true

    #
    # Expose internal performance and load indicators under the
    # "/~monitor" URL as a JSON structure.
    #
    # todo: monitor.enabled = false

    #
    # Limit how many documents are returned by /~search operations
    # by default (i.e. if the "limit" URL parameter is not specified).
    #
    #
    search.limit.default = 100

    #
    # The maximum allowed value for the limit parameter. A value of -1
    # means that no constraint is enforced. If unspecified, this value
    # falls back to 1000.
    #
    search.limit.maximum = -1

    #
    # Limit the maximum allowed level of sub-document links.
    # The depth query parameter can be no higher than this option.
    #
    render.depth.maximum = 5

    #
    # When the root mode is enabled, the UpSpace service will only
    # expose the specified space at the root of the incoming URLs,
    # i.e. it will no longer read the space from first segment of
    # the URLs and all operations will be applied onto the space
    # specified as root; all other space definitions are discarded.
    #
    root {
        enabled = false
        space = "/default"
    }

}

#
# Configure the spaces to be exposed by the UpSpace service.
# Each space definition contains its own distinct connection
# information, therefore UpSpace can also expose spaces which
# reside on different storage clusters.
#
spaces {

    /domains {
        enabled = true

        connection {
            driver = couchbase

            #
            # Driver connection configuration.
            #
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "domains"
            password = ""

            #
            # Purge search indexes if unused for longer than this period.
            # Default is one week (7days).
            #
            index.lifetime = 1days

            # todo: add parameter for observers (policy.persistTo, replicateTo)

            #
            # Operations taking longer than this will be discarded and a
            # HTTP 503 Service Unavailable response will be returned.
            #
            timeout = 5s
        }

        history {
            enabled = true
            lifetime = 0
        }

        # todo: allowed.paths ?, allowed.patterns ? etc.
    }

}
