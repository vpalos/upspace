package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import com.oneandone.upspace.unit.helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import com.oneandone.upspace.drivers.Driver

@RunWith(classOf[JUnitRunner])
class GetHandlerTest
    extends TestHelper {

    implicit val mockDriver = mock[Driver]

    "GET /document" should {

        mockDriver get("/existing/path", None) returns OpSuccess()
        "return status 200(OK) for an existing document" in {
            get("/existing/path", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).get("/existing/path", None)
            }
        }

        mockDriver get("/encoded/path", None) returns OpSuccess()
        "return status 200(OK) if the url is a valid encoded path" in {
            get("/encoded%2Fpath", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).get("/encoded/path", None)
            }
        }

        mockDriver get("/expire/path", Some(Expire(1))) returns OpSuccess()
        "return status 200(OK) with parameter headers" in {
            get("/expire%2Fpath", Seq.empty, defaultHeaders + ("X-Param-Expire" -> "1")) {
                status must_== 200
                there was one(mockDriver).get("/expire/path", Some(Expire(1)))
            }
        }

        mockDriver get("/missing/path", None) returns OpMissing()
        "return status 404(Not Found) for a missing document" in {
            get("/missing/path", Seq.empty, defaultHeaders) {
                status must_== 404
                there was one(mockDriver).get("/missing/path", None)
            }
        }

        "return status 406 if the Accept header is wrong" in {
            val headers = defaultHeaders + ("Accept" -> "text/html")
            get("/wrong/headers", Seq.empty, headers) {
                status must_== 406
                there was no(mockDriver).get("/wrong/headers", None)
            }
        }

        "return status 400(Bad Request) if the url is not valid" in {
            get(invalidPath, Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).get(invalidPath, None)
            }
        }

        "return status 400(Bad Request) if the url is /" in {
            get("/", Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).get("/", None)
            }
        }

    }

    addServlet(new SpaceServlet("/UTSpace", "/UTSpace", utSpaceConfig), "/*")
}