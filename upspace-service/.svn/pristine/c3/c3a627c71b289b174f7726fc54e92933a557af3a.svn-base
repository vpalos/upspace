package com.oneandone.upspace.helpers

import com.oneandone.upspace.handlers._
import com.oneandone.upspace.managers._
import java.text.ParseException
import java.lang.{Boolean, Double}
import java.util.Date
import org.json4s._
import scala.reflect.ClassTag

//import org.json4s.JsonAST._
import org.scalatra.util.conversion.TypeConverter
import scala.language.implicitConversions

/**
 * Sorting direction support.
 */
abstract class SortDirection
case class SortAscending() extends SortDirection
case class SortDescending() extends SortDirection

class SortDirectionConverter extends TypeConverter[String, SortDirection]{
    def apply(s: String) = {
        s(0).toLower match  {
            case 'a' => Some(SortAscending())
            case 'd' => Some(SortDescending())
            case _ => None
        }
    }
}

/**
  * Hold and enforce a set of checks on a field.
 */
abstract class SearchCriteria {
    val operator: String
    def jsonValues: List[JValue]
    def jsonMinValue: JValue
    def jsonMaxValue: JValue
    val kind: Symbol
    val test: (String) => Boolean
}
case class SearchTypedCriteria[T](operator: String, rawValues: List[String], cast: (String) => T)
                                 (implicit val ordering: Ordering[T], val typeTag: ClassTag[T])
    extends SearchCriteria {
    val baseValues: List[T] = {
        rawValues map {
            (value) =>
                try {
                    cast(value)
                } catch {
                    case _: Exception =>
                        throw new ParseException(s"Failed to parse '${rawValues.mkString(",")}' as [$typeTag]!", 0)
                }
        }
    }.distinct
    val jsonValues = baseValues map {
        (value) =>
            value match {
                case _: Boolean => JBool(value.asInstanceOf[Boolean])
                case _: Double  => JDouble(value.asInstanceOf[Double])
                case _: Date    => JInt(value.asInstanceOf[Date].getTime)
                case _          => JString(value.toString)
            }
    }

    def jsonMinValue: JValue = {
        val index = baseValues.zipWithIndex.minBy(_._1)._2
        jsonValues(index)
    }

    def jsonMaxValue: JValue = {
        val index = baseValues.zipWithIndex.maxBy(_._1)._2
        jsonValues(index)
    }

    private def op(value: String) = {
        val source = cast(value)
        baseValues.map(ordering.compare(_, source))
    }

    private def opEquals (value: String) = op(value).exists(_ == 0)
    private def opGreater(value: String) = op(value).forall(_ < 0)
    private def opLess   (value: String) = op(value).forall(_ > 0)
    private def opStarts (value: String) = baseValues.exists((data) => value.toString.startsWith(data.toString))

    val test: (String) => Boolean = operator match {
        case "=" => opEquals
        case ">" => opGreater
        case "<" => opLess
        case "~" => opStarts
    }

    val kind = operator match {
        case "=" => 'equal
        case "~" => 'start
        case ">" => 'range
        case "<" => 'range
    }
}
case class SearchField(cast: String, name: String) {
    def label = s"$cast:$name"
}

/**
  * Adds search operational facilities into the search handler.
 */
trait SearchHelper { self: SearchHandler =>

    protected val maxLimit = ConfigManager.options.getInt("search.max.limit")
    protected val argNamePattern = """(?i)^([sbdn])\w*:(.*)$""".r
    protected val argValuePattern = """(?i)^([~=><])(.*)$""".r

    protected def compileQuery = {
        val limit = params.get("limit") match {
            case None => maxLimit
            case Some(value) => value.toInt
        }
        val offset = params.get("offset") match {
            case None => 0
            case Some(value) => value.toInt
        }

        val query = multiParams -- List("splat", "sort", "direction", "limit", "offset", "depth")

        val searchRules = query map {
            case (argName, argValues) =>
                val (cast, name) = argName match {
                    case argNamePattern(_cast, _name) => (_cast.toLowerCase, _name)
                    case name: String => ("s", name)
                }
                if (name.isEmpty) {
                    throw new ParseException(s"Search rule '$cast:=${argValues.mkString(",")} does not have a name!", 0)
                }
                val rawOps = argValues.toList map {
                    (argValue) =>
                        val (op, value) = argValue match {
                            case argValuePattern(_op, _value) => (_op, _value)
                            case value: String => ("=", value)
                        }
                        if (value.isEmpty) {
                            throw new ParseException(s"Search rule '$cast:$name=$op' does not have a value!", 0)
                        }
                        (op, value)
                }
                val searchValues = rawOps.groupBy(_._1).map {
                    case (_op, _tuple) =>
                        val rawValues = _tuple.map(_._2)
                        val criteria: SearchCriteria = cast match {
                            case "b" => SearchTypedCriteria[Boolean](_op, rawValues, _.toBoolean)
                            case "n" => SearchTypedCriteria[Double] (_op, rawValues, _.toDouble)
                            case "d" => SearchTypedCriteria[Date]   (_op, rawValues, DateHelper.parse)
                            case  _  => SearchTypedCriteria[String] (_op, rawValues, _.toString)
                        }
                        criteria
                }
                SearchField(cast, name) -> searchValues.toList
        }

        val sortFields = multiParams.get("sort") match {
            case Some(fields) => {
                fields.toList map {
                    _ match {
                        case argNamePattern(_cast, _name) => SearchField(_cast.toLowerCase, _name)
                        case name => SearchField("s", name)
                    }
                }
            }
            case None =>
                List.empty
        }
        val sortDirection = params.get("direction") match {
            case Some(direction) => {
                if (direction.toLowerCase.startsWith("d")) {
                    SortDescending()
                } else {
                    SortAscending()
                }
            }
            case None => SortAscending()
        }

        searchRules foreach {
            case (field, operators) =>
                val kinds = operators.map(_.kind).distinct
                if (kinds.size > 1) {
                    throw new ParseException(
                        s"Multiple rule types on field ${field.label}! " +
                        s"Only one type of rules is allowed per field (i.e. either '==', '=~' or '=>|<').", 0)
                }
        }

        (searchRules, sortFields, sortDirection, offset, limit)
    }

}