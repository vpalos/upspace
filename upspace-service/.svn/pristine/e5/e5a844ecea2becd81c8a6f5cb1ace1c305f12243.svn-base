#
# UpSpace configuration.
#

#
# Service options.
#
options {

    #
    # Expose a Swagger JSON reference under the "/~help" URL
    # (regardless of space configuration). Interpret this URL
    # with a swagger-ui instance (http://swagger.wordnik.com).
    #
    help.enabled = true

    #
    # Expose internal performance and load indicators under the
    # "/~monitor" URL as a JSON structure.
    #
    # todo: monitor.enabled = false

    #
    # Limit the maximum allowed amount of documents that can be
    # returned in a single search query to avoid excessive requests.
    #
    search.limit.default = 100
    search.limit.maximum = 1000

    #
    # Limit the maximum allowed level of document symbolic links rendering depth.
    # The depth query parameter can be no higher than this option.
    #
    render.depth.maximum = 5

    #
    # When the root mode is enabled, the UpSpace service will only
    # expose the specified space at the root of the incoming URLs,
    # i.e. it will no longer read the space from first segment of
    # the URLs and all operations will be applied onto the space
    # specified as root; all other space definitions are discarded.
    #
    root {
        enabled = false
        space = "/domains"
    }

}

#
# Configure the spaces to be exposed by the UpSpace service.
# Each space definition contains its own distinct connection
# information, therefore UpSpace can also expose spaces which
# reside on different storage clusters.
#
spaces {

    /default {
        enabled = false

        connection {
            driver = couchbase

            #
            # Driver connection configuration.
            #
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "default"
            password = ""

            #
            # Extract large result sets in multiple chunks of this size.
            #
            result.chunks = 500

            # todo: add parameter for observers (policy.persistTo, replicateTo)

            #
            # Operations taking longer than this will be discarded and a
            # HTTP 503 Service Unavailable response will be returned.
            #
            timeout = 5s
        }

        history {
            enabled = true
            lifetime = 0
        }

        security {
            enabled = true

            auth.strategy = "http-basic"
            auth.realm = "/default"

            users {
                DomDev {
                    enabled = true
                    password = ""
                    allowed.methods = [ "get", "post", "put", "delete" ]
                    # todo: allowed.paths ?, allowed.patterns ? etc.
                }
            }
        }
    }

    /domains {
        enabled = true

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "domains"
            password = ""
            timeout = 5s
        }

        history {
            enabled = true
            lifetime = 0
        }
    }

}
