package com.oneandone.upspace.handlers

import com.oneandone.upspace.drivers._
import com.oneandone.upspace.helpers._
import org.json4s._
import org.scalatra._

/**
 * The main UpSpace servlet class.
 * In Scalatra the entire routing mechanism is moved from the
 * regular "web.xml" file into code (i.e. here).
 * @see http://www.scalatra.org/2.2/guides/http/routes.html
 */
class SpaceHandler(name: String, space: Driver) extends ServletHelper {

    private val validMimePattern = """(^|,)\s*(\*|application)/(\*|json)\s*(,|;|$)""".r
    private val validPathPattern = """^(/[^~][^/]*)+$""".r

    before("*") {
        contentType = formats("json")

        // todo: access control

        val path = sanitizePath(requestPath)

        if (validPathPattern.findFirstIn(path) == None) {
            respond(BadRequest())
        }

        request.header("Accept") match {
            case None => respond(NotAcceptable())
            case Some(header) =>
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(NotAcceptable())
                }
        }

        request.header("Content-Type") match {
            case None => {
                format = "json"
                warn("Request lacks 'Content-Type' header; expecting 'application/json'.")
            }
            case Some(header) => {
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(UnsupportedMediaType())
                }
            }
        }

        if (request.body.nonEmpty && !parsedBody.isInstanceOf[JsonAST.JObject]) {
            respond(BadRequest())
        }

        // todo: check charset=UTF-8 in headers
    }

    get("/*") {
        respond {
            val path = sanitizePath(params("splat"))
            space.get(path)
        }
    }

    post("/*") {
        val path = sanitizePath(params("splat"))
        val completePath = request.getContextPath + request.getServletPath + path
        val expire = request.headers.getOrElse("X-Param-Expire", "0").toInt
        val jsonBody = sanitizeJson(parsedBody)

        val result = space.create(path, jsonBody, expire)
        result match {
            case OpSuccess(_) => {
                respond(Created(headers = Map("Location" -> s"$completePath")))
            }
            case _ => {
                respond(result)
            }
        }
    }

    put("/*") {
        val path = sanitizePath(params("splat"))
        val expire = request.headers.getOrElse("X-Param-Expire", "0").toInt
        val merge = request.headers.getOrElse("X-Param-Merge", "false").toBoolean
        val jsonBody = sanitizeJson(parsedBody)
        respond {
            space.update(path, jsonBody, expire, merge)
        }
    }

    delete("/*") {
        respond {
            val path = sanitizePath(params("splat"))
            space.delete(path)
        }
    }

    notFound {
        contentType = null
        resourceNotFound()
    }
}