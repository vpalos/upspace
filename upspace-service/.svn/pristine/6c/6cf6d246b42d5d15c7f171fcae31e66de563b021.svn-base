<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.oneandone.persistence.upspace</groupId>
    <artifactId>upspace-persistence-service</artifactId>
    <version>1.0.1-SNAPSHOT</version>
    <inceptionYear>2013</inceptionYear>

    <packaging>war</packaging>

    <properties>

        <scala.version>2.10.3</scala.version>
        <scala.version.suffix>2.10</scala.version.suffix>
        <akka.version>2.2.3</akka.version>
        <spray.version>1.2.0</spray.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    </properties>

    <distributionManagement>

        <repository>
            <id>portal-pom-1und1-stable</id>
            <url>http://mavenrepo.united.domain:8081/nexus/content/repositories/1und1-stable
            </url>
        </repository>

        <snapshotRepository>
            <id>portal-pom-1und1-snapshot</id>
            <url>http://mavenrepo.united.domain:8081/nexus/content/repositories/1und1-snapshots
            </url>
        </snapshotRepository>

    </distributionManagement>

    <scm>
        <connection>scm:svn:https://svn.1and1.org/svn/domaindev/trunk/domain-registration-system/persistence-service
        </connection>
        <developerConnection>
            scm:svn:https://svn.1and1.org/svn/domaindev/trunk/domain-registration-system/persistence-service
        </developerConnection>
        <url>https://svn.1and1.org/svn/domaindev/trunk/domain-registration-system/persistence-service</url>
    </scm>

    <repositories>

        <repository>
            <id>codehaus-snapshots</id>
            <name>Codehaus Snapshots</name>
            <url>http://nexus.codehaus.org/snapshots/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>

        <repository>
            <id>spray</id>
            <name>Spray Snapshots</name>
            <url>http://repo.spray.io/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>

        <repository>
            <id>1und1-releases</id>
            <url>http://mavenrepo.united.domain:8081/nexus/content/groups/1und1-releases</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>

    </repositories>

    <dependencies>

        <!-- Logging. -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.0.13</version>
        </dependency>

        <!-- Configuration support. -->
        <dependency>
            <groupId>com.typesafe</groupId>
            <artifactId>config</artifactId>
            <version>1.0.2</version>
        </dependency>

        <!-- Servlet support. -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>

        <!-- CouchBase driver requirements. -->
        <dependency>
            <groupId>com.couchbase.client</groupId>
            <artifactId>couchbase-client</artifactId>
            <version>1.3.1</version>
        </dependency>

        <dependency>
            <groupId>net.databinder.dispatch</groupId>
            <artifactId>dispatch-core_${scala.version.suffix}</artifactId>
            <version>0.11.0</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Scala language. -->
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-compiler</artifactId>
            <version>${scala.version}</version>
        </dependency>

        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-library</artifactId>
            <version>${scala.version}</version>
        </dependency>

        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scalap</artifactId>
            <version>${scala.version}</version>
        </dependency>

        <!-- Spray framework with JSON support. -->
        <dependency>
            <groupId>io.spray</groupId>
            <artifactId>spray-servlet</artifactId>
            <version>${spray.version}</version>
        </dependency>

        <dependency>
            <groupId>io.spray</groupId>
            <artifactId>spray-routing</artifactId>
            <version>${spray.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>io.spray</groupId>
            <artifactId>spray-testkit</artifactId>
            <version>${spray.version}</version>
        </dependency>

        <dependency>
            <groupId>io.spray</groupId>
            <artifactId>spray-json_${scala.version.suffix}</artifactId>
            <version>1.2.5</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.json4s</groupId>
            <artifactId>json4s-jackson_${scala.version.suffix}</artifactId>
            <version>3.2.5</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scalap</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Akka concurrency support. -->
        <dependency>
            <groupId>com.typesafe.akka</groupId>
            <artifactId>akka-actor_${scala.version.suffix}</artifactId>
            <version>2.2.3</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.typesafe.akka</groupId>
            <artifactId>akka-testkit_${scala.version.suffix}</artifactId>
            <version>2.2.3</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Testing. -->
        <!--<dependency>-->
        <!--<groupId>org.specs2</groupId>-->
        <!--<artifactId>specs2_${scala.version.suffix}</artifactId>-->
        <!--<version>2.2.3</version>-->
        <!--<scope>test</scope>-->
        <!--</dependency>-->

        <!-- Part of the 1&1 remote logging setup. -->
        <!--<dependency>-->
        <!--<groupId>log4j</groupId>-->
        <!--<artifactId>log4j</artifactId>-->
        <!--<version>1.2.17</version>-->
        <!--</dependency>-->
        <!--<dependency>-->
        <!--<groupId>de.schlund.nexdom</groupId>-->
        <!--<artifactId>remote-logger</artifactId>-->
        <!--<version>1.4</version>-->
        <!--<exclusions>-->
        <!--<exclusion>-->
        <!--<groupId>org.slf4j</groupId>-->
        <!--<artifactId>slf4j-simple</artifactId>-->
        <!--</exclusion>-->
        <!--</exclusions>-->
        <!--</dependency>-->

    </dependencies>

    <pluginRepositories>

        <pluginRepository>
            <id>scala-tools.org</id>
            <name>Scala-Tools Maven2 Repository</name>
            <url>http://scala-tools.org/repo-releases</url>
        </pluginRepository>

        <pluginRepository>
            <id>codehaus-snapshots</id>
            <name>Codehaus Snapshots</name>
            <url>http://nexus.codehaus.org/snapshots/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>

    </pluginRepositories>

    <profiles>

        <profile>
            <id>local-default</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.6</version>
                    </plugin>
                </plugins>

                <finalName>upspace</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/local-default</directory>
                    </resource>
                </resources>
            </build>
        </profile>

        <profile>
            <id>hontldregpersmwdev-domains</id>
            <build>
                <finalName>upspace</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/hontldregpersmwdev-domains</directory>
                    </resource>
                </resources>
            </build>
        </profile>

        <profile>
            <id>domains</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.6</version>
                    </plugin>
                </plugins>

                <finalName>domains</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/domains</directory>
                    </resource>
                </resources>
            </build>
        </profile>

        <profile>
            <id>cache</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.6</version>
                    </plugin>
                </plugins>

                <finalName>cache</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/cache</directory>
                    </resource>
                </resources>
            </build>
        </profile>

        <profile>
            <id>test</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.6</version>
                    </plugin>
                </plugins>

                <finalName>test</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/test</directory>
                    </resource>
                </resources>
            </build>
        </profile>

        <profile>
            <id>ntld</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <version>2.6</version>
                    </plugin>
                </plugins>

                <finalName>ntld</finalName>
                <resources>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/resources</directory>
                        <excludes>
                            <exclude>src/main/resources/reference.conf</exclude>
                        </excludes>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/profiles/ntld</directory>
                    </resource>
                </resources>
            </build>
        </profile>

    </profiles>

    <build>

        <finalName>upspace</finalName>
        <sourceDirectory>src/main/scala</sourceDirectory>
        <testSourceDirectory>src/test/scala</testSourceDirectory>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>2.7</version>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.16</version>
                <configuration>
                    <forkCount>0</forkCount>
                    <useSystemClassLoader>false</useSystemClassLoader>

                    <includes>
                        <include>**/*Test.*</include>
                        <include>**/*Suite.*</include>
                    </includes>
                </configuration>
            </plugin>

            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.1.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>testCompile</goal>
                        </goals>
                        <configuration>
                            <jvmArgs>
                                <jvmArg>-Xms128m</jvmArg>
                                <jvmArg>-Xmx1024m</jvmArg>
                            </jvmArgs>
                            <args>
                                <arg>-feature</arg>
                                <arg>-unchecked</arg>
                                <arg>-deprecation</arg>
                            </args>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.codehaus.cargo</groupId>
                <artifactId>cargo-maven2-plugin</artifactId>
                <version>1.4.3</version>
                <configuration>
                    <container>
                        <containerId>jetty9x</containerId>
                        <type>embedded</type>
                    </container>
                    <configuration>
                        <properties>
                            <cargo.servlet.port>10080</cargo.servlet.port>
                        </properties>
                    </configuration>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.1.5</version>
                <configuration>
                    <scalaVersion>${scala.version}</scalaVersion>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
                <version>2.16</version>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <version>2.3</version>
            </plugin>

        </plugins>
    </reporting>
</project>
