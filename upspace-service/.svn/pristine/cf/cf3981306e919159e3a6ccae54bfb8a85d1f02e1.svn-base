package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.oneandone.persistence.upspace.service.helpers._
import scala.language.existentials
import scala.language.implicitConversions
import scala.reflect.ClassTag
import java.util.Date

/**
 * Couchbase criteria checks implementations.
 */
abstract class Check[T, V](control: T) {
    def matches(value: V): Boolean
}
case class EqualCheck[T](control: T)
                        (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) == 0
}
case class NotEqualCheck[T](control: T)
                           (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) != 0
}
case class GreaterThanCheck[T](control: T)
                              (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) > 0
}
case class GreaterThanOrEqualCheck[T](control: T)
                                     (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) >= 0
}
case class LessThanCheck[T](control: T)
                           (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) < 0
}
case class LessThanOrEqualCheck[T](control: T)
                                  (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) <= 0
}
abstract class RegexCheck[T](control: T) extends Check[T, T](control) {
    protected val regex =
        try {
            control.toString.r
        } catch {
            case _: Exception => throw new Complaint(s"Invalid regular expression: '$control'!")
        }
}
case class MatchCheck[T](control: T) extends RegexCheck(control) {
    def matches(value: T): Boolean = regex.findFirstIn(value.toString).nonEmpty
}
case class NotMatchCheck[T](control: T) extends RegexCheck(control) {
    def matches(value: T): Boolean = regex.findFirstIn(value.toString).isEmpty
}

object Check {
    def apply[T](operator: String, control: T)
                (implicit ord: Ordering[T]) = {
        operator match {
            case "==" => EqualCheck[T](control)
            case "!=" => NotEqualCheck[T](control)
            case "<"  => LessThanCheck[T](control)
            case "<=" => LessThanOrEqualCheck[T](control)
            case ">"  => GreaterThanCheck[T](control)
            case ">=" => GreaterThanOrEqualCheck[T](control)
            case "~"  => MatchCheck[T](control)
            case "!~" => NotMatchCheck[T](control)
            case op   => throw new Complaint(s"Unknown operator '$op'!")
        }
    }
}

/**
 * Couchbase formulae and modifiers implementations.
 */
case class Result[T, V](code: String)
                       (implicit _cast: String => T, val ord: Ordering[V], recast: T => V,
                        tagT: ClassTag[T], tagV: ClassTag[V]) {
    def cast(input: Any): V = {
        input match {
            case value: V => value: V
            case value: T => recast(value)
            case _ =>
                try {
                    recast(_cast(input.toString))
                } catch {
                    case _: Exception => throw new Complaint(s"Failed to parse '$input' as required type '$tagT'!")
                }
        }
    }
}

abstract class Transform(formula: Formula) extends CastHelper {

    val prototype = formula.arguments.map(_.getClass)
    val method = try {
        this.getClass.getMethod(formula.modifier, prototype: _*)
    } catch {
        case _: Exception =>
            val invocation = s"${formula.modifier}(${prototype.map(_.getSimpleName).mkString(", ")})"
            throw new Complaint(s"Field transformer '${formula.transform}.$invocation' does not exist!")
    }
    val result = method.invoke(this, formula.arguments.map(_.asInstanceOf[Object]): _*).asInstanceOf[Result[Any, Any]]

    protected def wrap(expression: String) = {
        val path = formula.path.mkString("['", "', '", "']")
        s"""(function(){ var field = extract(doc, $path); return (field === undefined ? null : $expression); })()"""
    }

    protected def field = "field"
}

case class IdTransform() extends Transform(Formula("id", null, "plain", List.empty)) {
    def plain       = Result[String, String](s"meta.id")
}

case class StringTransform(formula: Formula) extends Transform(formula) {
    private def template(add: String = "") = wrap(s"String($field)$add")

    def plain       = Result[String,  String](template())
    def length      = Result[Integer, Integer](template(".length"))
    def lowercase   = Result[String,  String](template(".toLowerCase()"))
    def uppercase   = Result[String,  String](template(".toUpperCase()"))

    def substring(from: Integer, to: Integer) =
        Result[String, String](template(s".substring($from, $to)"))
}

case class BooleanTransform(formula: Formula) extends Transform(formula) {
    private def template(add: String = "") = wrap(s"Boolean($field)$add")
    def plain = Result[Boolean, Boolean](template())
}

case class NumberTransform(formula: Formula) extends Transform(formula) {
    private def template(add: String = "") = wrap(s"Number($field)$add")
    def plain = Result[Double, Double](template())
}

case class DateTransform(formula: Formula) extends Transform(formula) {
    private def template(add: String = "") = wrap(s"new Date($field)$add")

    def plain       = Result[Date,    BigInt ](template(".getTime()"))
    def year        = Result[Integer, Integer](template(".getUTCFullYear()"))
    def month       = Result[Integer, Integer](template(".getUTCMonth() + 1"))
    def day         = Result[Integer, Integer](template(".getUTCDate()"))
    def weekday     = Result[Integer, Integer](template(".getUTCDay()"))
    def hour        = Result[Integer, Integer](template(".getUTCHours()"))
    def minute      = Result[Integer, Integer](template(".getUTCMinutes()"))
    def second      = Result[Integer, Integer](template(".getUTCSeconds()"))
    def millisecond = Result[Integer, Integer](template(".getUTCMilliseconds()"))
}

/**
 * Enforce the generic query components for the CouchBase driver.
 */
trait Formulae {

    protected def transform(formula: Formula): Transform = {
        formula.transform match {
            case "string"  => StringTransform(formula)
            case "boolean" => BooleanTransform(formula)
            case "number"  => NumberTransform(formula)
            case "date"    => DateTransform(formula)
            case unknown   => throw new Complaint(s"Unknown field transformer '$unknown'!")
        }
    }

}
