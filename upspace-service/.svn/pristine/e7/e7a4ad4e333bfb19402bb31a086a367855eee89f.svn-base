package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import com.oneandone.upspace.unit.helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DeleteHandlerTest
    extends TestHelper {

    "DELETE /document" should {

        mockDriver delete "/path/to/delete" returns OpSuccess()
        "return status 200(OK) for an existing document" in {
            delete("/path/to/delete", Seq.empty, defaultHeaders + ("Content-Type" -> "application/json")) {
                status must_== 200
                there was one(mockDriver).delete("/path/to/delete")
            }
        }

        mockDriver delete "/encoded/path" returns OpSuccess()
        "return status 200(OK) if the url is a valid encoded path" in {
            delete("/encoded%2Fpath", Seq.empty, defaultHeaders + ("Content-Type" -> "application/json")) {
                status must_== 200
                there was one(mockDriver).delete("/encoded/path")
            }
        }

        mockDriver delete "/missing/path"  returns OpMissing()
        "return status 404(Not Found) for a missing document" in {
            delete("/missing/path", Seq.empty, defaultHeaders + ("Content-Type" -> "application/json")) {
                status must_== 404
                there was one(mockDriver).delete("/missing/path")
            }
        }

        "return status 400(Bad Request) if the url contains an invalid character" in {
            delete(invalidPath, Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).delete(invalidPath)
            }
        }

        "return status 400(Bad Request) if the url is /" in {
            delete("/", Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).delete("/")
            }
        }
    }

    addServlet(new SpaceServlet("/UTSpace", utSpaceConfig), "/*")
}