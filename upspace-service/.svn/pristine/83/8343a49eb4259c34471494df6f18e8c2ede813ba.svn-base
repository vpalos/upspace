package com.oneandone.upspace.helpers

import akka.actor.ActorSystem
import java.text.ParseException
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.json4s._
import org.scalatra._
import org.scalatra.json._
import org.slf4j.LoggerFactory
import rl._
import scala.concurrent._
import scala.concurrent.duration._
import scala.reflect.ClassTag

trait ServletHelper
    extends ScalatraServlet
    with CastHelper
    with JacksonJsonSupport
    with JValueResult {

    protected val log = LoggerFactory.getLogger(getClass)

    override implicit protected val jsonFormats = castFormats

    protected def logRequest(answer: ActionResult = ActionResult(response.status, Unit, Map.empty))
                            (implicit request: HttpServletRequest, response: HttpServletResponse) {
        val method = request.requestMethod
        val client = request.remoteAddress
        val uri = fullUrl(request.pathInfo, request.parameters, includeContextPath = true, includeServletPath = true)
        val message = s"($client) $method $uri = ${answer.status.code} ${answer.status.message}"

        if (log.isDebugEnabled) {
            val inMark = "\n\t> "
            val outMark = "\n\t< "
            val inHeaders = request.headers.map(pair => pair._1 + ": " + pair._2).mkString(inMark, inMark, "")
            val outHeaders = response.headers.map(pair => pair._1 + ": " + pair._2).mkString(outMark, outMark, "")
            log.info(message + inHeaders + outHeaders)
        } else {
            log.info(message)
        }
    }

    protected def getParamAs[T](name: String, default: T)
                               (implicit request: HttpServletRequest, ev: String => T, typeTag: ClassTag[T]): T = {
        try {
            params.get(name) match {
                case None => default
                case Some(value) => value: T
            }
        } catch {
            case _: Exception =>
                throw new ParseException(s"Failed to parse '$name' parameter as '$typeTag'!", 0)
        }
    }

    protected def reply(answer: ActionResult)
                       (implicit request: HttpServletRequest, response: HttpServletResponse): ActionResult = {
        logRequest(answer)
        answer
    }

    protected def reply(opResult: OpResult)
                       (implicit request: HttpServletRequest, response: HttpServletResponse): ActionResult = {
        reply {
            opResult match {
                case OpSuccess(value: JValue)   => {
                    Ok(compact(value))
                }
                case OpSuccess(value: String)   => {
                    Ok(compact(parse(value).noNulls))
                }
                case OpSuccess(null)            => {
                    Ok()
                }
                case OpComplain(why: String)    => BadRequest(why)
                case OpTimeout(why: String)     => ServiceUnavailable(why)
                case OpOverload(why: String)    => ServiceUnavailable(why)
                case OpMissing(why: String)     => NotFound(why)
                case OpConflict(why: String)    => Conflict(why)
                case OpDeferred(why: String)    => Accepted(why)
                case OpUnsupported(why: String) => NotImplemented(why)
                case OpEmpty()                  => NoContent()
                case OpFailure(why: String)     => {
                    log.error(s"Driver malfunction: $why")
                    InternalServerError()
                }
                case _                          => {
                    log.error(s"Unknown driver response payload: ${Some(opResult)}")
                    InternalServerError()
                }
            }
        }
    }

    private val validPathPattern = """^(/[^~][^/]*)+$""".r

    protected def sanitizedPath(allowRoot: Boolean = false, paramName: String = "splat")
                               (implicit request: HttpServletRequest, response: HttpServletResponse) = {
        val path = params.get(paramName).getOrElse("")
        val decodedPath = if (UrlCodingUtils.isUrlEncoded(path)) {
            UrlCodingUtils.urlDecode(path)
        } else {
            path
        }
        val reducedPath = decodedPath.replaceAll("/{2,}", "/")
        val qualifiedPath = if (reducedPath endsWith "/") {
            reducedPath.init
        } else {
            reducedPath
        }
        val absolutePath = if (qualifiedPath.startsWith("/")) {
            qualifiedPath
        } else {
            "/" + qualifiedPath
        }
        if (validPathPattern.findFirstIn(absolutePath) == None && (absolutePath != "/" || !allowRoot)) {
            halt(reply(BadRequest(s"Document path '$absolutePath' is invalid!")))
        }
        absolutePath
    }

    protected def sanitizedBody()
                               (implicit request: HttpServletRequest, response: HttpServletResponse) = {
        val json = parsedBody
        json match {
            case _: JObject =>
            case _: JArray =>
            case _ => halt(reply(BadRequest("Request body is not valid JSON!")))
        }
        json
    }

}