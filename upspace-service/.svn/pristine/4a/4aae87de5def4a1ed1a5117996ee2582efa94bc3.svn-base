package com.oneandone.upspace

import com.oneandone.upspace.handlers._
import com.oneandone.upspace.managers._
import grizzled.slf4j._
import javax.servlet._
import org.scalatra._

/**
 * The Scalatra bootstrapping code.
 * Although normally the servlet would have been configured inside the "web.xml" file,
 * in Scalatra we have most of the servlet configuration contained in this class.
 * @see http://www.scalatra.org/guides/deployment/configuration.html
 */
class Bootstrap extends LifeCycle with Logging {

    override def init(context: ServletContext) {

        context.initParameters("org.scalatra.environment") = "production"

        Spaces.activeMountPoints foreach {
            (mountPoint) => {
                info(s"Mounting space '$mountPoint' ...")
                context.mount(new SpaceHandler(mountPoint, Spaces(mountPoint)), s"$mountPoint")
            }
        }

        if (Config().getBoolean("options.reference.enabled")) {
            info(s"Mounting reserved space '/~reference' ...")
            context.mount(new ReferenceHandler, "/~reference")
        }
    }

    override def destroy(context: ServletContext) {
        Spaces.shutdown()
    }

}

// fixme: tomcat7 needs: export JAVA_OPTS="-Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true"