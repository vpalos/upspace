package com.oneandone.persistence.upspace.service.actors

import akka.actor._
import akka.event.Logging
import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import com.oneandone.persistence.upspace.service.routes._
import com.typesafe.config._
import org.json4s._
import spray.http._
import spray.http.MediaTypes._
import spray.httpx.Json4sJacksonSupport
import spray.routing._
import spray.routing.PathMatchers._
import spray.routing.authentication._
import spray.routing.directives._

class SpaceActor(val mount: String, val spaceName: String, spaceConfig: Config)
    extends HttpServiceActor
    with Json4sJacksonSupport
    with CreateRoute
    with ReadRoute
    with UpdateRoute
    with DeleteRoute
    with HistoryRoute
    with SearchRoute
    with RoutingHelper
    with ActorLogging {

    implicit val json4sJacksonFormats = DefaultFormats
    implicit val loggingAdapter = log
    implicit val executionContext = this.context.system.dispatcher

    protected val driver = Driver(spaceName, spaceConfig)

//    private val isSecure = spaceConfig.getBoolean("security.enabled")
//    private val authenticator = if (isSecure) {
//        val strategy = spaceConfig.getString("security.strategy")
//        val engine = strategy match {
//            case "http-basic" =>
//
//                def _authenticate =
//                BasicAuth()
//            case _ => throw new Exception(s"Security strategy '$strategy' unsupported!") // Complaint?
//        }
//    }

    protected val validDocumentPath = "^[^~].*$".r

    def receive = runRoute {
        detach() {
            respondWithMediaType(`application/json`) {
                if (spaceConfig.getBoolean("security.enabled")) {
                    //                    val strategy = spaceConfig.getString("security.strategy")
                    //                    val engine = strategy match {
                    //                        case "http-basic" =>
                    //                        case _ =>
                    //                    }
                    //                    authenticate(engine) {
                    //
                    //                    }
                    implicit val user = "fake"
                    route
                } else {
                    implicit val user = ""
                    route
                }
            }
        }
    }

    val exceptionHandler = ExceptionHandler {
        case e: Complaint =>
            complete {
                HttpResponse(400, entity = e.getMessage)
            }
        case e: Exception =>
            complete {
                HttpResponse(500, entity = e.getMessage)
            }
    }

    private def route(implicit user: String): Route = {
        clientIP { ip =>
            logRequestResponse(requestResponseLogger(ip) _) {

                val mountRoute = separateOnSlashes(mount.stripPrefix("/"))

                pathPrefix(mountRoute) {

                    handleExceptions(exceptionHandler) {

                        decompressRequest() {
                            compressResponseIfRequested() {

                                routeReadDocument ~
                                routeCreateDocument ~
                                routeUpdateDocument ~
                                routeDeleteDocument ~
                                routeDocumentHistory ~
                                routeSearchDocuments
                            }
                        }
                    }
                }
            }
        }
    } ~ complete {
        HttpResponse(StatusCodes.MethodNotAllowed)
    }

    def requestResponseLogger(ip: RemoteAddress)(req: HttpRequest): Any => Option[LogEntry] = {
        case res: HttpResponse =>
            val state = s"${res.message.status.value} (${res.message.entity.data.length} bytes)"
            val entry = log.isDebugEnabled match {
                case false => LogEntry(s"($ip) ${req.method} ${req.uri} .. $state", Logging.InfoLevel)
                case true  => LogEntry(s"($ip) $req .. $state", Logging.DebugLevel)
            }
            Some(entry)
        case _ =>
            None
    }

    if (ConfigManager.isRoot) {
        log.info(s"Space '$spaceName' mounted under '$mount' (root mode).")
    } else {
        log.info(s"Space '$spaceName' mounted under '$mount'.")
    }
}

//    implicit def user()
//                     (implicit request: HttpServletRequest, response: HttpServletResponse): User = {
//        super.user match {
//            case user: User => user
//            case _ => User("", ConfigFactory.empty())
//        }
//    }
//
//    before("*") {
//        enforceAuth match {
//            case Some(result: ActionResult) => halt(reply(result))
//            case None =>
//        }
//
//
//    get("/~help/?") {
//        halt(reply(Found("../~help")))
//    }
//
