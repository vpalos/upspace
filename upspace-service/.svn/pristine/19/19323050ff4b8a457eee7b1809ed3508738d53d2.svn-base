package com.oneandone.upspace.drivers

import com.couchbase.client._
import com.typesafe.config._
import java.util.concurrent._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._
import net.spy.memcached._

//case class Timeout()

/**
 * This class hides all Couchbase related functionalities.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(spaceConfig: Config) extends Driver {

    private val timeout = spaceConfig.getMilliseconds("timeout")
    private val client = {
        val nodes = spaceConfig.getStringList("nodes")
        val bucket = spaceConfig.getString("bucket")
        val password = spaceConfig.getString("password")
        val uris = nodes map java.net.URI.create
        new CouchbaseClient(uris, bucket, password)
    }

    private def failureCatcher(op: => OpResult): OpResult = {
        try {
            op
        } catch {
            case _:TimeoutException => OpTimeout()
            case _:IllegalStateException => OpOverload()
            case _:Throwable => OpFailure()
        }
    }

    override def get(path: String): OpResult = {
        failureCatcher {
            client.asyncGet(path).get(timeout, TimeUnit.MILLISECONDS) match {
                case null => OpMissing()
                case data => OpSuccess(parse(data.toString).noNulls)
            }
        }
    }

    override def create(path: String, json: JValue, expire: Int = 0): OpResult = {
        failureCatcher {
            if (client.add(path, expire, compact(json.noNulls)).get(timeout, TimeUnit.MILLISECONDS)) {
                OpSuccess()
            } else {
                OpConflict()
            }
        }
    }

    override def update(path: String, json: JValue, expire: Int = 0, merge: Boolean = false): OpResult = {
        failureCatcher {
            if (merge) {
                val casValue = client.asyncGetAndLock(path, 30).get(timeout, TimeUnit.MILLISECONDS)
                casValue.getValue match {
                    case null => OpMissing()
                    case data => {
                        val oldJson = parse(data.toString)
                        val casCode = casValue.getCas
                        val newJson = oldJson merge json
                        val future = client.asyncCAS(path, casCode, expire, compact(newJson.noNulls), client.getTranscoder)
                        future.get(timeout, TimeUnit.MILLISECONDS) match {
                            case CASResponse.OK => OpSuccess()
                            case CASResponse.NOT_FOUND => OpMissing()
                            case _ => OpOverload()
                        }
                    }
                }
            } else {
                if (client.replace(path, expire, compact(json.noNulls)).get(timeout, TimeUnit.MILLISECONDS)) {
                    OpSuccess()
                } else {
                    OpMissing()
                }
            }
        }
    }

    override def delete(path: String): OpResult = {
        failureCatcher {
            if (client.delete(path).get(timeout, TimeUnit.MILLISECONDS)) {
                OpSuccess()
            } else {
                OpMissing()
            }
        }
    }

    override def shutdown() {
        client.shutdown(timeout, TimeUnit.MILLISECONDS)
    }
}
