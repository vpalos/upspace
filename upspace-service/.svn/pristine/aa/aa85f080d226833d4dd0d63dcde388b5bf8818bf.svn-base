package com.oneandone.upspace

import com.oneandone.upspace.handlers._
import com.oneandone.upspace.managers._
import javax.servlet._
import org.scalatra._
import org.slf4j.LoggerFactory

/**
 * The Scalatra bootstrapping code.
 * Although normally the servlet would have been configured inside the "web.xml" file,
 * in Scalatra we have most of the servlet configuration contained in this class.
 * @see http://www.scalatra.org/guides/deployment/configuration.html
 */
class Bootstrap extends LifeCycle {

    private val log = LoggerFactory.getLogger(getClass)
    implicit protected val swagger = new HelpSwagger

    override def init(context: ServletContext) {
        context.initParameters("org.scalatra.environment") = "production"

        Spaces.activeMountPoints foreach {
            (mountPoint) => {
                log.info(s"Mounting space '$mountPoint' ...")
                context.mount(new SpaceHandler(mountPoint, Spaces(mountPoint)), s"$mountPoint")
            }
        }

        if (Config().getBoolean("options.help.enabled")) {
            log.info(s"Mounting reserved space '/~help' ...")
            context.mount(new HelpHandler, "/~help")
        }
    }

    override def destroy(context: ServletContext) {
        Spaces.shutdown()
    }

}

// fixme: tomcat7 needs: export JAVA_OPTS="-Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true"