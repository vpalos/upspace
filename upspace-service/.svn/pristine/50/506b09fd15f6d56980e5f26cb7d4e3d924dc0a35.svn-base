package com.oneandone.upspace.servlets

import _root_.akka.actor.ActorSystem
import com.oneandone.upspace.drivers._
import com.oneandone.upspace.handlers._
import com.oneandone.upspace.helpers._
import com.typesafe.config._
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.scalatra._
import org.scalatra.swagger._
import org.scalatra.ActionResult
import scala.Some
import scala.concurrent.ExecutionContext

/**
 * The main UpSpace servlet class.
 * In Scalatra the entire routing mechanism is moved from the
 * regular "web.xml" file into code (i.e. here).
 * @see http://www.scalatra.org/2.2/guides/http/routes.html
 */
class SpaceServlet(val spaceId: String, val mountPoint: String, val spaceConfig: Config)
                  (implicit val swagger: Swagger, externalDriver: Driver = null)
    extends {

        protected val theatre = ActorSystem()
        protected implicit val executor: ExecutionContext = theatre.dispatcher

    }
    with ServletHelper
    with SecurityHelper
    with PostHandler
    with GetHandler
    with PutHandler
    with PatchHandler
    with DeleteHandler
    with SearchHandler
    with HistoryHandler
    with HelpHelper
    with SpaceHelper {

    implicit def spaceName(): String = spaceId

    implicit def user()
                     (implicit request: HttpServletRequest, response: HttpServletResponse): User = {
        super.user match {
            case user: User => user
            case _ => User("", ConfigFactory.empty())
        }
    }

    protected val driver = if (externalDriver != null) externalDriver else Driver(spaceConfig)

    protected val validMimePattern = """(^|,)\s*(\*|application)/(\*|json)\s*(,|;|$)""".r

    before("*") {
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true)

        enforceAuth match {
            case Some(result: ActionResult) => halt(reply(result))
            case None =>
        }

        request.header("Accept") match {
            case None => halt(reply(NotAcceptable()))
            case Some(header) =>
                if (validMimePattern.findFirstIn(header) == None) {
                    halt(reply(NotAcceptable()))
                }
        }

        request.header("Content-Type") match {
            case None => {
                format = "json"
                log.warn("Request lacks 'Content-Type' header; expecting 'application/json'.")
            }
            case Some(header) => {
                if (validMimePattern.findFirstIn(header) == None) {
                    halt(reply(UnsupportedMediaType()))
                }
            }
        }

        // todo: check charset=UTF-8 in headers
        contentType = formats("json")
    }

    get("/~help/?") {
        halt(reply(Found("../~help")))
    }

    notFound {
        contentType = null
        resourceNotFound()
    }

    override def shutdown() {
        driver.shutdown()
        theatre.shutdown()
        theatre.awaitTermination()
    }
}
