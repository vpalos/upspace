#
# UpSpace service configuration.
#

#
# Service options.
#
options {

    #
    # Expose a Swagger JSON reference under the "/~help" URL
    # (regardless of space configuration). Interpret this URL
    # using any Swagger viewer (e.g. swagger-ui); for more
    # details visit http://swagger.wordnik.com/.
    #
    help.enabled = true

    #
    # Expose internal performance and load indicators under the
    # "/~monitor" URL as a JSON structure.
    #
    monitor.enabled = false

    #
    # Tune the "Access-Control-Allow-Origin" HTTP header to enable
    # cross-origin resource sharing (i.e. calling the service from
    # hosts other than the one this service is running at). Note
    # that the "/~help" URL (if enabled) always allows all origins.
    #
    allowedOrigins = "*"
}

#
# Configure the spaces to be exposed by the UpSpace service.
# Each space definition contains its own distinct connection
# information, therefore UpSpace can also expose spaces which
# reside on different clusters.
#
spaces {

    #
    # When the root space is enabled (i.e. "/"), the UpSpace service
    # enters "root" mode which will only expose this space at the root
    # of the incoming URLs, i.e. it will no longer read the space from
    # first segment of the URLs; all operations will be applied on this
    # space and all other space definitions will be ignored.
    #
    / {
        enabled = false

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "domains"
            password = ""
            timeout = 5s
        }

        authentication {
            enabled = true
            strategy = "basic"
        }

        history {
            enabled = true
            lifetime = 365d
        }
    }

    /default {
        enabled = false

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "default"
            password = ""
            timeout = 5s
        }

        authentication {
            enabled = true
            strategy = "basic"
        }

        history {
            enabled = false
            lifetime = 0
        }
    }

    /arboreal {
        enabled = false

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "arboreal"
            password = ""
            timeout = 5s
        }

        authentication {
            enabled = true
            strategy = "basic"
        }

        history {
            enabled = true
            lifetime = 5d
        }
    }

    /ntld-configuration {
        enabled = false

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "ntld-configuration"
            password = ""
            timeout = 5s
        }

        authentication {
            enabled = true
            strategy = "basic"
        }

        history {
            enabled = true
            lifetime = 0
        }
    }

    /domains {
        enabled = true

        connection {
            driver = couchbase
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "domains"
            password = ""
            timeout = 5s
        }

        authentication {
            enabled = true
            strategy = "basic"
        }

        history {
            enabled = true
            lifetime = 0
        }
    }

}

#
# The accounts (user/password) allowed to access the UpSpace service.
# Each user is attributed a specific set of roles per-space.
#
users {

    MrDefault {
        enabled = true
        password = ""
        access {
            /default = [ "get", "post", "put", "delete" ]
        }
    }

    UTUser {
        enabled = true
        password = "UTPassword"
        access {
            /default = [ "get", "post", "put", "delete" ]
        }
    }

}
