package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers._
import org.json4s._
import scala.language.postfixOps
import spray.httpx.unmarshalling._
import spray.httpx.unmarshalling.FromStringDeserializers._

trait UpdateRoute { self: SpaceActor =>

    protected def routeUpdateDocument(implicit user: String) =
        put {

            pathPrefixTest(validDocumentPath) { _ =>
                path(RestPath) { _path =>
                    val path = sanitizePath(_path)

                    entity(as[JValue]) { json =>

                        optionalHeaderValueByName("X-Param-Expire") { headerExpire =>
                            optionalHeaderValueByName("X-Param-Merge") { headerMerge =>

                                parameters (
                                    'expire.as[Int] ?,
                                    'merge.as[Boolean] ? headerMerge.getOrElse("false").toBoolean,
                                    'create.as[Boolean] ? false,
                                    'replaceLists.as[Boolean] ? true) {
                                    (expire,
                                     merge,
                                     create,
                                     replaceLists) =>

                                        val _expire = Expire(expire).orElse(Expire(headerExpire))

                                        complete {
                                            driver
                                                .update(path, json, _expire, create, merge, replaceLists)
                                                .map(response)
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

}

// TODO: Remove "X-Param-Expire" (deprecated).
// TODO: Remove "X-Param-Merge" (deprecated).