package com.oneandone.upspace.drivers

import com.couchbase.client._
import com.couchbase.client.protocol.views._
import com.oneandone.upspace.helpers._
import com.oneandone.upspace.drivers.couchbase._
import com.typesafe.config._
import java.util.concurrent._
import net.spy.memcached._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.slf4j._
import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext
import scala.Predef._

/**
 * This class hides all Couchbase related functionality.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(val connectionConfig: Config, val historyConfig: Config)
                     (implicit val spaceName: () => String, val executor: ExecutionContext)
    extends {

        protected val nodes = connectionConfig.getStringList("nodes")
        protected val bucket = connectionConfig.getString("bucket")
        protected val client = {
            val password = connectionConfig.getString("password")
            val uris = nodes map java.net.URI.create
            new CouchbaseClient(uris, bucket, password)
        }

        protected val indexForceUpdate = try {
            connectionConfig.getBoolean("index.forceUpdate") match {
                case true => Stale.FALSE
                case false => Stale.UPDATE_AFTER
            }
        } catch {
            case _: Exception => Stale.FALSE
        }
    }
    with Driver
    with DriverHelper
    with CastHelper
    with Views
    with SymLinks
    with Search
    with History {

    protected val log = LoggerFactory.getLogger(getClass)
    protected val timeout = connectionConfig.getMilliseconds("timeout")

    override def get(userId: String, path: String, depth: Int, expire: Option[Expire] = None): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            val future = expire match {
                case None => client.asyncGet(path)
                case Some(ts) => client.asyncGetAndTouch(path, ts.toAbsolute(nowSeconds))
            }

            future.get(timeout, TimeUnit.MILLISECONDS) match {
                case null =>
                    OpMissing()
                case data =>
                    val document = data match {
                        case cas: CASValue[_] => cas.getValue
                        case _ => data
                    }

                    expire match {
                        case Some(ts) =>
                            logHistory(userId, path, "touch", nowMilliSeconds,
                                "expire" -> {
                                    ts.asDate(nowSeconds) match {
                                        case None => false
                                        case Some(date) => date
                                    }
                                }
                            )
                        case None =>
                    }

                    val renderedDocument = renderDeeper(Map("" -> document.toString), depth - 1)
                    OpSuccess(renderedDocument.get("").get)
            }
        }
    }

    override def create(userId: String, path: String, json: JValue, expire: Option[Expire] = None): OpResult = {
        catchFailures {
            val inputJson = json.noNulls

            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            // fixme: PersistTo.MASTER conflict on UTF-8 keys (DHDOMAIN-6155)
            val future = expire match {
                case None => client.add(path, compact(render(inputJson))/*, PersistTo.MASTER*/)
                case Some(ts) => client.add(path, ts.toAbsolute(nowSeconds), compact(render(inputJson))/*, PersistTo.MASTER*/)
            }

            future.get(timeout, TimeUnit.MILLISECONDS).booleanValue match {
                case true =>
                    expire match {
                        case Some(ts) =>
                            logHistory(userId, path, "create", nowMilliSeconds, "inputJson" -> inputJson,
                                "expire" -> {
                                    ts.asDate(nowSeconds) match {
                                        case None => false
                                        case Some(date) => date
                                    }
                                }
                            )
                        case None =>
                            logHistory(userId, path, "create", nowMilliSeconds, "inputJson" -> inputJson)
                    }
                    OpSuccess()
                case false =>
                    // fixme: OpConflict returned is case of OpOverload (out-of-memory?, temporary failure: code 11)
                    OpConflict()
            }
        }
    }

    override def update(userId: String, path: String, json: JValue, expire: Option[Expire] = None, merge: Option[Boolean] = None): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            merge match {
                case Some(true) =>
                    client.asyncGets(path).get(timeout, TimeUnit.MILLISECONDS) match {
                        case null => OpMissing()
                        case data => {
                            val oldJson = parse(data.getValue.toString)
                            val casCode = data.getCas
                            val newJson = (oldJson merge json).noNulls

                            val future = expire match {
                                case None => client.asyncCAS(path, casCode, compact(render(newJson)), client.getTranscoder)
                                case Some(ts) => client.asyncCAS(path, casCode, ts.toAbsolute(nowSeconds), compact(render(newJson)), client.getTranscoder)
                            }

                            future.get(timeout, TimeUnit.MILLISECONDS) match {
                                case CASResponse.OK => {
                                    expire match {
                                        case Some(ts) =>
                                            if (oldJson == newJson) {
                                                logHistory(userId, path, "update", nowMilliSeconds,
                                                    "inputJson" -> json, "finalJson" -> newJson, "merge" -> merge.orElse(null),
                                                    "expire" -> {
                                                        ts.asDate(nowSeconds) match {
                                                            case None => false
                                                            case Some(date) => date
                                                        }
                                                    }
                                                )
                                            }
                                        case None =>
                                            logHistory(userId, path, "update", nowMilliSeconds,
                                                "inputJson" -> json, "finalJson" -> newJson, "merge" -> merge.orElse(null))
                                    }
                                    OpSuccess()
                                }
                                case CASResponse.NOT_FOUND => OpMissing()
                                case _ => OpOverload()
                            }
                        }
                    }

                case _ =>
                    val body = json.noNulls

                    val future = expire match {
                        case None => client.replace(path, compact(render(body)))
                        case Some(ts) => client.replace(path, ts.toAbsolute(nowSeconds), compact(render(body)))
                    }

                    if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                        expire match {
                            case Some(ts) =>
                                logHistory(userId, path, "update", nowMilliSeconds,
                                    "inputJson" -> json, "merge" -> merge.orElse(null),
                                    "expire" -> {
                                        ts.asDate(nowSeconds) match {
                                            case None => false
                                            case Some(date) => date
                                        }
                                    }
                                )
                            case None =>
                                logHistory(userId, path, "update", nowMilliSeconds,
                                    "inputJson" -> json, "merge" -> merge.orElse(null))
                        }
                        OpSuccess()
                    } else {
                        OpMissing()
                    }
            }
        }
    }

    override def delete(userId: String, path: String): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val future = client.delete(path/*, PersistTo.MASTER*/)

            if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                logHistory(userId, path, "delete", nowMilliSeconds)
                OpSuccess()
            } else {
                OpMissing()
            }
        }
    }

    override def shutdown() {
        shutdownViews()
        client.shutdown()
    }

}
