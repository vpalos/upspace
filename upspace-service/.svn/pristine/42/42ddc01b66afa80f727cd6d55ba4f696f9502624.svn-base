package com.oneandone.upspace.security

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.managers._
import com.typesafe.config._
import org.scalatra._
import org.scalatra.auth.strategy.BasicAuthStrategy
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

/**
 * Authenticator implementing HTTP Basic Auth strategy.
 */
class HttpBasicAuthStrategy(app: ScalatraBase, securityConfig: Config)
    extends BasicAuthStrategy[User](app, securityConfig.getString("auth.realm")) {

    override def name: String = getClass.getName

    private val users = ConfigManager.users(securityConfig)

    def validate(username: String, password: String)
                (implicit request: HttpServletRequest, response: HttpServletResponse): Option[User] = {
        users.getOrElse(username, None) match {
            case userConfig: Config => {
                if (userConfig.getString("password") == password) {
                    Some(User(username, userConfig))
                } else {
                    None
                }
            }
            case _ => None
        }
    }

    override def isValid(implicit request: HttpServletRequest) = true

    protected def getUserId(user: User)(implicit request: HttpServletRequest, response: HttpServletResponse): String = user.id
}