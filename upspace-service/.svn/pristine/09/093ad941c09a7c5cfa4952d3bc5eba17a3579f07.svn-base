package com.oneandone.upspace.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import org.scalatra._
import org.scalatra.swagger._

/**
 * Handles the GET operations for space servlets.
 */
trait GetHandler
    extends ServletHelper
    with HelpHelper { self: SpaceServlet =>

    val getHelp = (apiOperation(s"getHelp${spaceName().replace("/", "_")}", jsonDocumentModel)

        summary "Retrieve the document at the given path."

        notes """Retrieves the document corresponding to the given path identifier from the storage space."""

        error Error(415, "The 'Content-Type' header must be 'application/json'.")
        error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
        error Error(404, "The specified document was not found.")
        error Error(403, "Authorization error; operation was forbidden (not an authentication issue).")
        error Error(401, "Authentication failed; valid credentials must be supplied.")
        error Error(400, "Malformed request (illegal document path, invalid JSON body etc.).")
        error Error(200, "The document was retrieved successfully; expect response to be valid JSON.")

        parameter pathParam[String]("document")
            .description("The document path identifier (path segments may not start with tilda, i.e. '~').")
            .defaultValue("a/b/c")
            .required

        parameter headerParam[Int]("X-Param-Expire")
            .description(
                """
                  |Automatically delete this document after a period of time (always in seconds).
                  |The value 0 (zero) means no expiration; values over 30 days are interpreted as absolute timestamps (UNIX Epoch).
                  |Setting this header parameter will change the expire value for the document.
                  |When this parameter is not used, the expire is left intact.
                """.stripMargin)
            .defaultValue(0)

        parameter headerParam[String]("Accept")
            .description("The mime-type accepted by the client.")
            .allowableValues[String](List("application/json", "*/*"))
            .defaultValue("application/json")
            .required

        parameter headerParam[String]("Authorization")
            .description(
                """
                  |The HTTP Auth header (only required if 'security' enabled).
                  |This value must follow the syntax of the authentication strategy set in 'auth.strategy'.
                  |Only the <a href='http://en.wikipedia.org/wiki/Basic_auth'>'http-basic' strategy</a>
                  |is supported for now (i.e. <i>user:password</i> must be
                  |<a href='http://www.base64encode.org/'>Base64 encoded</a>).
                """.stripMargin)
            .defaultValue("Basic Base64(user:password)")
        )
    get("/:document", operation(getHelp)) { pass() }

    get("/*") {
        respond {
            val path = sanitizedPath()

            val expire = try {
                Expire(request.getIntHeader("X-Param-Expire"))
            } catch {
                case _: NumberFormatException =>
                    respond(BadRequest("Failed to parse 'X-Param-Expire' as integer!"))
                    null.asInstanceOf[Expire]
            }

            driver.get(path, Option(expire))
        }
    }

}
