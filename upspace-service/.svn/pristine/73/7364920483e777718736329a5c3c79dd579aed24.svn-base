package com.oneandone.upspace.servlets

import com.oneandone.upspace.drivers._
import com.oneandone.upspace.handlers._
import com.oneandone.upspace.helpers._
import com.typesafe.config._
import org.json4s._
import org.scalatra._
import org.scalatra.swagger._

/**
 * The main UpSpace servlet class.
 * In Scalatra the entire routing mechanism is moved from the
 * regular "web.xml" file into code (i.e. here).
 * @see http://www.scalatra.org/2.2/guides/http/routes.html
 */
class SpaceServlet(val mountPoint: String, val spaceConfig: Config)
                  (implicit val swagger: Swagger, externalDriver: Driver = null)
    extends ServletHelper
    with SecurityHelper
    with HelpHelper
    with PostHandler
    with GetHandler
    with PutHandler
    with DeleteHandler
    with SearchHandler
    with HistoryHandler {

    implicit def user(): User = {
        super.user match {
            case user: User => user
            case _ => User("", ConfigFactory.empty())
        }
    }

    protected val driver = if (externalDriver != null) externalDriver else Driver(spaceConfig)

    protected val validMimePattern = """(^|,)\s*(\*|application)/(\*|json)\s*(,|;|$)""".r
    protected val validPathPattern = """^(/~history|/~restore|/~search)?(/[^~][^/]*)+$""".r

    before("*") {
        contentType = formats("json")

        enforceAuth match {
            case Some(result: ActionResult) => respond(result)
            case None =>
        }

        val path = sanitizePath(requestPath)

        if (validPathPattern.findFirstIn(path) == None) {
            respond(BadRequest())
        }

        request.header("Accept") match {
            case None => respond(NotAcceptable())
            case Some(header) =>
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(NotAcceptable())
                }
        }

        request.header("Content-Type") match {
            case None => {
                format = "json"
                log.warn("Request lacks 'Content-Type' header; expecting 'application/json'.")
            }
            case Some(header) => {
                if (validMimePattern.findFirstIn(header) == None) {
                    respond(UnsupportedMediaType())
                }
            }
        }

        if (request.body.nonEmpty && !parsedBody.isInstanceOf[JsonAST.JObject]) {
            respond(BadRequest())
        }

        // todo: check charset=UTF-8 in headers
    }

    notFound {
        contentType = null
        resourceNotFound()
    }
}
