package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.drivers._
import com.oneandone.upspace.servlets._
import com.oneandone.upspace.unit.helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GetHandlerTest
    extends TestHelper {

    "GET /document" should {

        mockDriver get "/existing/path" returns OpSuccess()
        "return status 200(OK) for an existing document" in {
            get("/existing/path", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).get("/existing/path")
            }
        }

        mockDriver get "/encoded/path" returns OpSuccess()
        "return status 200(OK) if the url is a valid encoded path" in {
            get("/encoded%2Fpath", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).get("/encoded/path")
            }
        }

        mockDriver get "/missing/path" returns OpMissing()
        "return status 404(Not Found) for a missing document" in {
            get("/missing/path", Seq.empty, defaultHeaders) {
                status must_== 404
                there was one(mockDriver).get("/missing/path")
            }
        }

        "return status 406 if the Accept header is wrong" in {
            val headers = defaultHeaders + ("Accept" -> "text/html")
            get("/wrong/headers", Seq.empty, headers) {
                status must_== 406
                there was no(mockDriver).get("/wrong/headers")
            }
        }

        "return status 400(Bad Request) if the url is not valid" in {
            get(invalidPath, Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).get(invalidPath)
            }
        }

        "return status 400(Bad Request) if the url is /" in {
            get("/", Seq.empty, defaultHeaders) {
                status must_== 400
                there was no(mockDriver).get("/")
            }
        }

    }

    addServlet(new SpaceServlet("/UTSpace", utSpaceConfig), "/*")
}

// todo: test with URLs ending with slash "/" -> should fail