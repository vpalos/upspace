package com.oneandone.upspace.drivers

import com.couchbase.client._
import com.typesafe.config._
import java.util.concurrent._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._
import net.spy.memcached._
import org.slf4j.LoggerFactory

/**
 * This class hides all Couchbase related functionalities.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(connectionConfig: Config, historyConfig: Config) extends Driver {

    protected val log = LoggerFactory.getLogger(getClass)

    private val timeout = connectionConfig.getMilliseconds("timeout")
    private val historyEnabled = historyConfig.getBoolean("enabled")
    //todo decide if this is a date or a period of time and if so in what it is expressed
    private val historyLifetime = historyConfig.getInt("lifetime")
    private val client = {
        val nodes = connectionConfig.getStringList("nodes")
        val bucket = connectionConfig.getString("bucket")
        val password = connectionConfig.getString("password")
        val uris = nodes map java.net.URI.create
        new CouchbaseClient(uris, bucket, password)
    }

    private def catchFailure(op: => OpResult): OpResult = {
        try {
            op
        } catch {
            case _:TimeoutException => OpTimeout()
            case _:IllegalStateException => OpOverload()
            case _:Throwable => OpFailure()
        }
    }

    private def logHistory(path: String,
                           action: String,
                           operationTmsp: Long,
                           user: String,
                           expire: Option[Int],
                           merge: Option[Boolean],
                           requestBody: Option[JValue]) {
        val requestBodyStr = {
            requestBody match {
                case Some(value) => compact(render(value))
                case None => null
            }
        }
        val expireStr = {
            expire match {
                case Some(value) => "\"" + value + "\""
                case None => null
            }
        }
        val mergeStr = {
            merge match {
                case Some(value) => "\"" + value + "\""
                case None => null
            }
        }

        val historyDocument = s""" {"action": "$action", "user": "$user", "expire": $expireStr, "merge": $mergeStr, "request_body": $requestBodyStr} """
        log.debug("Writing history " + historyDocument)
        val addHistorySuccess: Boolean = client.add("/~history" + path + "/" + operationTmsp, historyLifetime, historyDocument).get()
        if( !addHistorySuccess ) {
            log.error(s"Failed to persist history for document with path : $path")
            // todo what about unicity? possible fix add also an UUID to the document name.
            // todo what about timeouts or...?
        }
    }

    override def get(path: String)
                    (implicit user: String): OpResult = {
        catchFailure {
            client.asyncGet(path).get(timeout, TimeUnit.MILLISECONDS) match {
                case null => OpMissing()
                case data => OpSuccess(parse(data.toString).noNulls)
            }
        }
    }

    override def create(path: String, json: JValue, expire: Int = 0)
                       (implicit user: String): OpResult = {
        val operationTmsp = System.nanoTime()
        catchFailure {
            if (client.add(path, expire, compact(json.noNulls)).get(timeout, TimeUnit.MILLISECONDS)) {
                if(historyEnabled)
                    logHistory(path, "create", operationTmsp, user, Some(expire), merge = None, Some(json.noNulls))
                OpSuccess()
            } else {
                OpConflict()
            }
        }
    }

    override def update(path: String, json: JValue, expire: Int = 0, merge: Boolean = false)
                       (implicit user: String): OpResult = {
        val operationTmsp = System.nanoTime()
        catchFailure {
            if (merge) {
                val casValue = client.asyncGetAndLock(path, 30).get(timeout, TimeUnit.MILLISECONDS)
                casValue.getValue match {
                    case null => OpMissing()
                    case data => {
                        val oldJson = parse(data.toString)
                        val casCode = casValue.getCas
                        val newJson = oldJson merge json
                        val future = client.asyncCAS(path, casCode, expire, compact(newJson.noNulls), client.getTranscoder)
                        future.get(timeout, TimeUnit.MILLISECONDS) match {
                            case CASResponse.OK => {
                                if(historyEnabled)
                                    logHistory(path, "update", operationTmsp, user, Some(expire), Some(merge), Some(json))
                                OpSuccess()
                            }
                            case CASResponse.NOT_FOUND => OpMissing()
                            case _ => OpOverload()
                        }
                    }
                }
            } else {
                if (client.replace(path, expire, compact(json.noNulls)).get(timeout, TimeUnit.MILLISECONDS)) {
                    if(historyEnabled)
                        logHistory(path, "update", operationTmsp, user, Some(expire), Some(merge), Some(json))
                    OpSuccess()
                } else {
                    OpMissing()
                }
            }
        }
    }

    override def delete(path: String)
                       (implicit user: String): OpResult = {
        val operationTmsp = System.nanoTime()
        catchFailure {
            if (client.delete(path).get(timeout, TimeUnit.MILLISECONDS)) {
                if(historyEnabled)
                    logHistory(path, "delete", operationTmsp, user, expire = None, merge = None, requestBody = None)
                OpSuccess()
            } else {
                OpMissing()
            }
        }
    }

    override def shutdown() {
        client.shutdown(timeout, TimeUnit.MILLISECONDS)
    }
}
