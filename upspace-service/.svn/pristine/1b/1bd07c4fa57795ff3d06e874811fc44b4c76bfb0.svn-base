package com.oneandone.upspace.helpers

import com.oneandone.upspace.drivers._
import java.util.Date
import scala.io._
import scala.util.matching._
import scala.collection.concurrent.TrieMap

/**
 * Expiration timestamp support.
 */
abstract class Expire {
    def toAbsolute(since: Int): Int = {
        this match {
            case rts: ExpireRelative => since + rts.delta
            case ats: ExpireAbsolute => ats.when
            case nts: ExpireNever => 0
        }
    }
    def asDate(since: Int): Option[Date] = {
        val absolute = toAbsolute(since).toLong * 1000
        if (absolute != 0) {
            Some(new Date(absolute))
        } else {
            None
        }
    }
}
object Expire {
    def apply(ts: Int): Expire = {
        if (ts < 0) {
            null
        } else if (ts == 0) {
            ExpireNever()
        } else if (ts > 30 * 24 * 3600) {
            ExpireAbsolute(ts)
        } else {
            ExpireRelative(ts)
        }
    }
}
case class ExpireRelative(delta: Int) extends Expire
case class ExpireAbsolute(when: Int) extends Expire
case class ExpireNever() extends Expire
// TODO: ExpireAbsolute should take date strings, ExpireRelative should take number of seconds

/**
 * Operation results.
 */
abstract class OpResult
case class OpSuccess    (answer: Any = "") extends OpResult
case class OpComplain   (why: String = "") extends OpResult
case class OpConflict   (why: String = "") extends OpResult
case class OpTimeout    (why: String = "") extends OpResult
case class OpOverload   (why: String = "") extends OpResult
case class OpMissing    (why: String = "") extends OpResult
case class OpDeferred   (why: String = "") extends OpResult
case class OpUnsupported(why: String = "") extends OpResult
case class OpFailure    (why: String = "") extends OpResult
case class OpEmpty      ()                 extends OpResult

/**
 * Utility methods for drivers.
 */
trait DriverHelper { self: Driver =>

    val driverNamePattern = new Regex("""(?i)\.([a-z0-9]+)Driver$""", "name")

    private val resourceCache = TrieMap.empty[String, String]

    def loadResource(resourceName: String): String = {
        resourceCache.get(resourceName) match {
            case Some(data) => data
            case None =>
                val capture = driverNamePattern.findFirstMatchIn(getClass.getName).get
                val driverName = capture.group("name").toLowerCase
                val data = Source.fromURL(getClass.getResource(s"/drivers/$driverName/$resourceName")).mkString
                resourceCache += resourceName -> data
                data
        }
    }

    def retryOp(max: Int, current: Int = 0)(op: (Int) => Boolean): Int = {
        if (current == max || op(current)) {
            return max - current
        }
        retryOp(max, current + 1)(op)
    }

}
