package com.oneandone.upspace.drivers

import com.oneandone.upspace.helpers._
import com.typesafe.config._
import java.util.Date
import org.json4s._
import org.slf4j.LoggerFactory

/**
 * The abstract definition of a storage Driver.
 *
 * Note: All expire timestamps follow the Couchbase specification, meaning when
 *       greater than 30*24*60*60 (i.e. 30 days) the timestamp is interpreted as
 *       an absolute timestamp (from Unix Epoch); 0 (zero) disables expiration.
 *
 * Note: All JSON input and output should be cleared of fields with null values.
 */
abstract class Driver(implicit user: () => User) {

    def glob(path: String): OpResult

    def get(path: String, expire: Option[Expire] = None): OpResult

    def create(path: String, json: JValue, expire: Option[Expire] = None): OpResult

    def update(path: String, json: JValue, expire: Option[Expire] = None, merge: Option[Boolean] = None): OpResult

    def delete(path: String): OpResult

    def search(offset: Int = 0, limit: Int = 0, sortBy: String = null, sortDirection: SortDirection = SortAscending())
              (rules: Map[String, AnyRef]*): OpResult

    def history(path: String, from: Date, to: Date, direction: SortDirection): OpResult

    def restore(path: String, date: Date): OpResult

    def shutdown()
}

/**
 * Spawn a new driver instance.
 */
object Driver {

    protected val log = LoggerFactory.getLogger(getClass)

    def apply(spaceConfig: Config)
             (implicit user: () => User) = {
        val connectionConfig = spaceConfig.getConfig("connection")
        val historyConfig = spaceConfig.getConfig("history")
        val driverType = connectionConfig.getString("driver")

        try {
            driverType match {
                case "couchbase" => new CouchbaseDriver(connectionConfig, historyConfig)
            }
        } catch {
            case e: Exception => {
                log.error(s"Failed to establish '$driverType' connection using configuration: $connectionConfig")
                throw e
            }
        }
    }

}