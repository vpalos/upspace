package com.oneandone.upspace.drivers

import com.couchbase.client._
import com.couchbase.client.protocol.views._
import com.oneandone.upspace.helpers._
import com.typesafe.config._
import java.security.MessageDigest
import java.util.concurrent._
import java.util.Date
import net.spy.memcached._
import org.apache.commons.codec.binary.Hex
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson._
import org.json4s.jackson.JsonMethods._
import org.slf4j._
import scala.collection.JavaConversions._
import scala.Predef._
import scala.collection.concurrent.TrieMap

/**
 * This class hides all Couchbase related functionalities.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(connectionConfig: Config, historyConfig: Config)
                     (implicit spaceName: () => String, user: () => User)
    extends Driver
    with DriverHelper {

    protected val log = LoggerFactory.getLogger(getClass)

    implicit private val formats = DefaultFormats

    private val timeout = connectionConfig.getMilliseconds("timeout")
    private val historyEnabled = historyConfig.getBoolean("enabled")
    private val historyLifetime = historyConfig.getMilliseconds("lifetime")
    private val client = {
        val nodes = connectionConfig.getStringList("nodes")
        val bucket = connectionConfig.getString("bucket")
        val password = connectionConfig.getString("password")
        val uris = nodes map java.net.URI.create
        new CouchbaseClient(uris, bucket, password)
    }

    private val digestCache = TrieMap.empty[String, String]
    private def digestView(keys: String*)(bodies: String*): String = {
        val key = keys.mkString("_")
        val digest = {
            digestCache.get(key) match {
                case Some(value) => value
                case None =>
                    val messageDigest = MessageDigest.getInstance("SHA")
                    val rawDigest = messageDigest.digest(bodies.mkString.getBytes("UTF-8"))
                    val encDigest = Hex.encodeHexString(rawDigest).mkString
                    digestCache += key -> encDigest
                    encDigest
            }
        }
        key + "_" + digest
    }

    private val viewCache = TrieMap.empty[(String, String), View]
    private def renderView(viewName: String, map: String, reduce: String = null)
                          (data: Map[String, String] = Map.empty) = {
        val designDocumentName = digestView(viewName)(map, reduce)

        log.debug(s"Invoking view '$viewName'...")

        try {
            viewCache.get(designDocumentName, viewName) match {
                case Some(view) =>
                    // todo: detect deleted views and invalidate cache
                    view
                case None =>
                    val view = client.getView(designDocumentName, viewName)
                    viewCache += (designDocumentName, viewName) -> view
                    view
            }
        } catch {
            case _: InvalidViewException => {
                val designDocument = new DesignDocument(designDocumentName)
                val viewDesign = reduce match {
                    case null => new ViewDesign(viewName, map)
                    case _ => new ViewDesign(viewName, map, reduce)
                }

                designDocument.setView(viewDesign)
                client.asyncCreateDesignDoc(designDocument)

                // This key is added for future referencing of stale views.
                client.add(s"/~view/$designDocumentName", viewName)
                null
            }
        }
    }
    private def createView(viewName: String, map: String, reduce: String = null) = {
        // todo: clean-up old/stale views from the space
        renderView(viewName, map, reduce)()
    }

    private def catchFailures(op: => OpResult): OpResult = {
        try {
            op
        } catch {
            case _:TimeoutException => OpTimeout()
            case _:IllegalStateException => OpOverload()
            case _:Exception => OpFailure()
        }
    }

    private def logHistory(path: String, action: String, timestamp: Long, values: (String, Any)*) {
        if (!historyEnabled) {
            return
        }

        val givenValues = values.toMap filter(_._2 != null)
        val requiredValues = Map("timestamp" -> DateHelper.format(timestamp),
                                 "space" -> spaceName(),
                                 "action" -> action)

        val json = Serialization.write(givenValues ++ requiredValues ++ {
            if (user().id.nonEmpty) Map("user" -> user().id) else Map.empty
        })
        retryOp(5) {
            (cycle) =>
                val expire = if (historyLifetime == 0) {
                    0
                } else {
                    ((System.currentTimeMillis + historyLifetime)/1000).toInt
                }
                val future = client.add( s"/~history$path/$timestamp.$cycle", expire, json)
                future.get(timeout, TimeUnit.MILLISECONDS)
        } match {
            case 0 => log.error(s"Failed to save history event for document '$path': $json")
            case _ =>
        }
    }

    private def renderDepth(paths: List[String], depth: Int): OpResult = {
        depth match {
            case 0 =>
                OpSuccess(render(paths))
            case 1 =>
                val unsorted = client.getBulk(paths).mapValues {
                    data =>
                        try {
                            parse(data.toString)
                        } catch {
                            case _: Exception => null
                        }
                }
                val sorted = paths.map {
                    path =>
                        path -> unsorted.getOrElse(path, null)
                }
                OpSuccess(new JObject(sorted.filterNot(_ == null)))
            case _ =>
                OpUnsupported("Depth parameter values beyond 1 are not (yet) supported!")
        }
    }

    override def get(path: String, expire: Option[Expire] = None): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            val future = expire match {
                case None => client.asyncGet(path)
                case Some(ts) => client.asyncGetAndTouch(path, ts.toAbsolute(nowSeconds))
            }

            future.get(timeout, TimeUnit.MILLISECONDS) match {
                case null =>
                    OpMissing()
                case data =>
                    val value = data match {
                        case cas: CASValue[_] => cas.getValue
                        case _ => data
                    }
                    val document = parse(value.toString).noNulls
                    expire match {
                        case Some(ts) =>
                            logHistory(path, "touch", nowMilliSeconds,
                                "expire" -> {
                                    ts.asDate(nowSeconds) match {
                                        case None => "false"
                                        case Some(date) => DateHelper.format(date)
                                    }
                                }
                            )
                        case None =>
                    }
                    OpSuccess(document)
            }
        }
    }

    override def create(path: String, json: JValue, expire: Option[Expire] = None): OpResult = {
        catchFailures {
            val inputJson = json.noNulls

            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            // todo: parametrize PersistTo.MASTER
            // fixme: PersistTo.MASTER conflict on UTF-8 keys (DHDOMAIN-6155)
            val future = expire match {
                case None => client.add(path, compact(inputJson)/*, PersistTo.MASTER*/)
                case Some(ts) => client.add(path, ts.toAbsolute(nowSeconds), compact(inputJson)/*, PersistTo.MASTER*/)
            }

            val result = future.get(timeout, TimeUnit.MILLISECONDS).booleanValue
            result match {
                case true =>
                    expire match {
                        case Some(ts) =>
                            logHistory(path, "create", nowMilliSeconds, "inputJson" -> inputJson,
                                "expire" -> {
                                    ts.asDate(nowSeconds) match {
                                        case None => "false"
                                        case Some(date) => DateHelper.format(date)
                                    }
                                }
                            )
                        case None =>
                            logHistory(path, "create", nowMilliSeconds, "inputJson" -> inputJson)
                    }
                    OpSuccess()
                case false =>
                    // fixme: OpConflict returned is case of OpOverload (out-of-memory?, temporary failure: code 11)
                    OpConflict()
            }
        }
    }

    override def update(path: String, json: JValue, expire: Option[Expire] = None, merge: Option[Boolean] = None): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val nowSeconds = (nowMilliSeconds / 1000).toInt

            merge match {
                case Some(true) =>
                    val casValue = client.asyncGets(path).get(timeout, TimeUnit.MILLISECONDS)
                    casValue.getValue match {
                        case null => OpMissing()
                        case data => {
                            val oldJson = parse(data.toString)
                            val casCode = casValue.getCas
                            val newJson = (oldJson merge json).noNulls

                            val future = expire match {
                                case None => client.asyncCAS(path, casCode, compact(newJson), client.getTranscoder)
                                case Some(ts) => client.asyncCAS(path, casCode, ts.toAbsolute(nowSeconds), compact(newJson), client.getTranscoder)
                            }

                            future.get(timeout, TimeUnit.MILLISECONDS) match {
                                case CASResponse.OK => {
                                    expire match {
                                        case Some(ts) =>
                                            if (oldJson == newJson) {
                                                logHistory(path, "update", nowMilliSeconds,
                                                    "inputJson" -> json, "finalJson" -> newJson, "merge" -> merge.orElse(null),
                                                    "expire" -> {
                                                        ts.asDate(nowSeconds) match {
                                                            case None => "false"
                                                            case Some(date) => DateHelper.format(date)
                                                        }
                                                    }
                                                )
                                            }
                                        case None =>
                                            logHistory(path, "update", nowMilliSeconds,
                                                "inputJson" -> json, "finalJson" -> newJson, "merge" -> merge.orElse(null))
                                    }
                                    OpSuccess()
                                }
                                case CASResponse.NOT_FOUND => OpMissing()
                                case _ => OpOverload()
                            }
                        }
                    }

                case _ =>
                    val body = json.noNulls

                    val future = expire match {
                        case None => client.replace(path, compact(body))
                        case Some(ts) => client.replace(path, ts.toAbsolute(nowSeconds), compact(body))
                    }

                    if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                        expire match {
                            case Some(ts) =>
                                logHistory(path, "update", nowMilliSeconds,
                                    "inputJson" -> json, "merge" -> merge.orElse(null),
                                    "expire" -> {
                                        ts.asDate(nowSeconds) match {
                                            case None => "false"
                                            case Some(date) => DateHelper.format(date)
                                        }
                                    }
                                )
                            case None =>
                                logHistory(path, "update", nowMilliSeconds,
                                    "inputJson" -> json, "merge" -> merge.orElse(null))
                        }
                        OpSuccess()
                    } else {
                        OpMissing()
                    }
            }
        }
    }

    override def delete(path: String): OpResult = {
        catchFailures {
            val nowMilliSeconds = System.currentTimeMillis
            val future = client.delete(path/*, PersistTo.MASTER*/)

            if (future.get(timeout, TimeUnit.MILLISECONDS)) {
                logHistory(path, "delete", nowMilliSeconds)
                OpSuccess()
            } else {
                OpMissing()
            }
        }
    }

    def searchView(prefix: String, mapFields: List[SearchField]) = {
        val mapCode = {
            loadResource("views/search-map.js")
                .replaceFirst("%%prefix%%", Serialization.write(prefix))
                .replaceFirst("%%fields%%", Serialization.write(mapFields))
        }
        createView("search" + Serialization.write(mapFields map (_.label)), mapCode)
    }

    def search(prefix: String,
               rules: Map[SearchField, List[SearchRule]],
               sortFields: List[SearchField],
               sortDirection: SortDirection = SortAscending(),
               offset: Int, limit: Int, depth: Int = 0, count: Boolean = false): OpResult = {

        catchFailures {
            val (queryEqualityFields, queryEqualityValues) = {
                rules map {
                    case (_field, _rules) =>
                        val constraint = _rules.find(_.isInstanceOf[SearchRuleEquals])
                        if (constraint.nonEmpty && constraint.get.controlValues.size == 1) {
                            (_field, constraint.get.controlValues.head)
                        } else {
                            (null, null)
                        }
                }
            }.filter(_._1 != null).toList.sortBy(_._1.label).unzip

            val idField = SearchField("_", "id")
            val querySortFields = (sortFields :+ idField).distinct.diff(queryEqualityFields)

            val (queryFilterFields, queryFilterRules) = {
                (rules -- queryEqualityFields).toList.sortBy(_._1.label).unzip
            }

            val constraintField = querySortFields.head
            val (viewPrefix, startKey, endKey) = {
                if (constraintField == idField) {
                    (null, prefix, s"$prefix\uefff")
                } else {
                    val limits = rules.getOrElse(constraintField, List.empty).map {
                        rule => rule match {
                            case gt: SearchRuleGreaterThan => (gt.controlValue, null)
                            case lt: SearchRuleLessThan => (null, lt.controlValue)
                            case _ => (null, null)
                        }
                    }
                    val start = limits.find(_._1 != null).getOrElse((null, null))._1
                    val end = limits.find(_._2 != null).getOrElse((null, null))._2
                    (prefix, start, end)
                }
            }

            // todo: views with spaces in their name (i.e. +) do not get created (always returns 202)
            val viewFields = queryEqualityFields ++ querySortFields ++ queryFilterFields
            val searchViewCode = searchView(viewPrefix, viewFields)
            if (searchViewCode == null) {
                return OpDeferred()
            }

            val (descending, fromKey, toKey) = sortDirection match {
                case SortAscending() => (false, startKey, endKey)
                case SortDescending() => (true, endKey, startKey)
            }

            val query = new Query()
            query.setInclusiveEnd(true)
            query.setStale(Stale.FALSE)
            query.setDescending(descending)
            query.setIncludeDocs(false)
            query.setReduce(false)

            if (fromKey != null) {
                val rangeStart = Serialization.write(queryEqualityValues :+ fromKey)
                log.debug(s"Searching from $rangeStart...")
                query.setRangeStart(rangeStart)
            }
            if (toKey != null) {
                val rangeEnd = Serialization.write(queryEqualityValues :+ toKey)
                log.debug(s"Searching from $rangeEnd...")
                query.setRangeEnd(rangeEnd)
            }

            val pageSize = List(List(offset, limit).max, 1000).min
            val segments = client.paginatedQuery(searchViewCode, query, pageSize)

            val required = offset + limit
            val unneeded = queryEqualityFields.size + querySortFields.size

            // todo: check raw value comparisons on sorting/range filtering
            def advance(segments: Paginator, required: Int): List[String] = {
                if (!segments.hasNext || (!count && required <= 0)) {
                    return List.empty
                }

                log.debug(s"Remaining: $required")

                val rawChunk = segments.next()
                val unfilteredChunk = rawChunk.map {
                    row => (row.getId, parse(row.getKey).values.asInstanceOf[List[Any]].drop(unneeded))
                }
                val filteredChunk = unfilteredChunk.filter {
                    case (_, values) =>
                        values.zip(queryFilterRules) forall {
                            case (value, rule) =>
                                rule.forall(_.matches(value))
                        }
                }

                filteredChunk.map(_._1).toList ::: advance(segments, required - filteredChunk.size)
            }


            val filtered = advance(segments, required)

            if (count) {
                OpSuccess(filtered.size.toString)
            } else {
                renderDepth(filtered.slice(offset, offset + limit), depth)
            }
        }
    }

    private def historyView = {
        createView("history",
            loadResource("views/history-map.js"),
            loadResource("views/history-reduce.js"))
    }

    override def history(path: String, from: Date, to: Date, sortDirection: SortDirection): OpResult = {
        catchFailures {
            val query = new Query()
            query.setIncludeDocs(true)
            query.setInclusiveEnd(true)
            query.setStale(Stale.FALSE)
            query.setReduce(false)
            sortDirection match {
                case SortAscending() => {
                    query.setDescending(false)
                    query.setRangeStart(s"$path/${from.getTime}.0")
                    query.setRangeEnd(s"$path/${to.getTime}.\uefff")
                }
                case SortDescending() => {
                    query.setDescending(true)
                    query.setRangeStart(s"$path/${to.getTime}.\uefff")
                    query.setRangeEnd(s"$path/${from.getTime}.0")
                }
            }

            val historyViewCode = historyView
            if (historyViewCode == null) {
                return OpDeferred()
            }

            val future = client.asyncQuery(historyViewCode, query)
            val result = future.get(timeout, TimeUnit.MILLISECONDS)

            var historyList = parse("[]")
            for(row <- result) {
                historyList = historyList ++ parse(row.getDocument.toString)
            }

            OpSuccess(historyList)
        }
    }

    override def restore(path: String, targetDate: Date): OpResult = {
        catchFailures {
            val query = new Query()
            query.setIncludeDocs(true)
            query.setStale(Stale.FALSE)
            query.setReduce(true)
            query.setInclusiveEnd(true)
            query.setRangeStart(s"$path/")
            query.setRangeEnd(s"$path/${targetDate.getTime}.\uefff")

            val historyViewCode = historyView
            if (historyViewCode == null) {
                return OpDeferred()
            }

            val future = client.asyncQuery(historyViewCode, query)

            val result = future.get(timeout, TimeUnit.MILLISECONDS)
            if(result.iterator.hasNext){
                val value = parse(result.iterator.next.getValue)
                val body = value(3)

                val expireDate = try {
                    Some(DateHelper.parse(value(2).extract[String]))
                } catch {
                    case _: Exception => None // false
                }

                if (body == JNull || (!expireDate.isEmpty && targetDate.after(expireDate.get))) {
                    OpEmpty()
                } else {
                    OpSuccess(body)
                }
            } else {
                OpEmpty()
            }
        }
    }

    override def shutdown() {
        client.shutdown(timeout, TimeUnit.MILLISECONDS)
    }

}
