package com.oneandone.persistence.upspace.service.helpers

import akka.actor._
import com.oneandone.persistence.upspace.service.managers._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import java.net.URLDecoder
import spray.http._

class Complaint(why: String) extends RuntimeException(why)

trait RoutingHelper { self: ActorLogging =>

    protected val isRoot = ConfigManager.options.getBoolean("root.enabled")

    protected def decodeUtf8[T >: String](url: T) = URLDecoder.decode(url.toString, "UTF-8")

    protected def normalize[T >: String](url: T) = "/" + decodeUtf8(url).replaceAll("/+$", "")

    protected def response(opResult: OpResult): HttpResponse = {
        opResult match {
            case OpSuccess(value: String)   => HttpResponse(200, compact(parse(value).noNulls))
            case OpSuccess(value: JValue)   => HttpResponse(200, compact(value))
            case OpSuccess(null)            => HttpResponse(200)
            case OpComplain(why: String)    => HttpResponse(400, why)
            case OpTimeout(why: String)     => HttpResponse(503, why)
            case OpOverload(why: String)    => HttpResponse(503, why)
            case OpMissing(why: String)     => HttpResponse(404, why)
            case OpConflict(why: String)    => HttpResponse(409, why)
            case OpDeferred(why: String)    => HttpResponse(202, why)
            case OpUnsupported(why: String) => HttpResponse(501, why)
            case OpEmpty()                  => HttpResponse(204)
            case OpFailure(why: String)     =>
                log.error(s"Driver malfunction: $why")
                HttpResponse(500, entity = why)
            case _                          =>
                log.error(s"Unknown driver response payload: ${Some(opResult)}")
                HttpResponse(500)
        }
    }

}
