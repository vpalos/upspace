package com.oneandone.upspace.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import org.scalatra.swagger._

/**
 * Handles the "before" interceptor for space servlets.
 */
trait DeleteHandler
    extends ServletHelper
    with HelpHelper { self: SpaceServlet =>

    val deleteHelp = (
        apiOperation("deleteHelp", jsonDocumentModel)

            summary "Delete the document at the given path."

            notes
            """
              |Deletes the document corresponding to the given path identifier from the storage space.
              |The operation will fail if the document does not exist.
            """.stripMargin

            error Error(415, "The 'Content-Type' header must be 'application/json'.")
            error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
            error Error(404, "The specified document was not found.")
            error Error(403, "Authorization error; operation was forbidden (not an authentication issue).")
            error Error(401, "Authentication failed; valid credentials must be supplied.")
            error Error(400, "Malformed request (illegal document path, invalid JSON body etc.).")
            error Error(200, "The document was deleted successfully.")

            parameter {
                pathParam[String]("document")
                    .description("The document path identifier (path segments may not start with tilda, i.e. '~').")
                    .defaultValue("a/b/c")
                    .required
            }

            parameter {
                headerParam[String]("Accept")
                    .description("The mime-type accepted by the client.")
                    .allowableValues[String](List("application/json", "*/*"))
                    .defaultValue("application/json")
                    .required
            }

            parameter {
                headerParam[String]("Authorization")
                    .description {
                        """
                          |The HTTP Auth header (only required if 'security' enabled).
                          |This value must follow the syntax of the authentication strategy set in 'auth.strategy'.
                          |Only the <a href='http://en.wikipedia.org/wiki/Basic_auth'>'http-basic' strategy</a>
                          |is supported for now (i.e. <i>user:password</i> must be
                          |<a href='http://www.base64encode.org/'>Base64 encoded</a>).
                        """.stripMargin
                    }
                    .defaultValue("Basic Base64(user:password)")
            }
        )
    delete("/:document", operation(deleteHelp)) { pass() }


    delete("/*") {
        val path = sanitizedPath()
        val userId = user().id

        new DetachedResult() {
            def body = {

                reply(driver.delete(userId, path))

            }
        }
    }
}
