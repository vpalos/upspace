package com.oneandone.persistence.upspace.service.drivers

import akka.event.LoggingAdapter
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import com.typesafe.config._
import java.util.Date
import org.json4s._
import scala.concurrent._

/**
 * Note: All expire timestamps follow the Couchbase specification, meaning when
 *       greater than 30*24*60*60 (i.e. 30 days) the timestamp is interpreted as
 *       an absolute timestamp (from Unix Epoch); 0 (zero) disables expiration.
 */
abstract class Driver(implicit log: LoggingAdapter, executionContext: ExecutionContext) extends DriverHelper {

    def read(path: String, depth: Int, expire: Option[Expire] = None)
            (implicit user: String): Future[OpResult]

    def create(path: String, json: JValue, expire: Option[Expire] = None)
              (implicit user: String): Future[OpResult]

    def update(path: String, json: JValue,
               expire: Option[Expire] = None, create: Boolean = false,
               merge: Boolean = false, replaceLists: Boolean = true)
              (implicit user: String): Future[OpResult]

    def delete(path: String)
              (implicit user: String): Future[OpResult]

    def search(prefix: String,
               rules: List[Rule],
               sorts: List[FieldExpression],
               direction: Direction = Ascending(),
               offset: Int, limit: Int, depth: Int = 0)
              (implicit user: String): Future[OpResult]

    def history(path: String, from: Date, to: Date, direction: Direction)
               (implicit user: String): Future[OpResult]

    def restore(path: String, date: Date)
               (implicit user: String): Future[OpResult]

    def shutdown()
}

/**
 * Spawn a new driver instance.
 */
object Driver {

    def apply(spaceName: String, spaceConfig: Config)
             (implicit log: LoggingAdapter, executionContext: ExecutionContext) = {

        val defaults = ConfigManager.defaults
        val connectionConfig = spaceConfig.getConfig("connection").withFallback(defaults.getConfig("connection"))
        val historyConfig = spaceConfig.getConfig("history")
        val driverType = connectionConfig.getString("driver")

        try {
            driverType match {
                case "couchbase" => new CouchbaseDriver(spaceName, connectionConfig, historyConfig)
            }
        } catch {
            case e: Exception =>
                log.error(s"Failed to establish '$driverType' connection using configuration: $connectionConfig!")
                throw e
        }
    }

}