package com.oneandone.upspace.managers

import com.typesafe.config._
import scala.collection.JavaConversions._

/**
 * Loads and ensures global access to the UpSpace configuration file(s).
 */
object Config {

    private val config = ConfigFactory.load()

    protected val validSpacePattern = "^/([^~][^/]*)?$".r
    protected val validUserPattern = "^(?i)[a-z][a-z0-9_.-]*$".r

    def getMap(path: String): Map[String, Config] = {
        if (!config.hasPath(path)) {
            return Map.empty
        }
        val items = config.getObject(path).keySet.toSet
        items.foldLeft(Map[String, Config]()) {
            (map, key) => {
                if (validSpacePattern.findFirstIn(key) == None) {
                    throw new IllegalArgumentException(s"Illegal space name '$key'!")
                }
                val config = Config().getConfig(s"$path." + key)
                val enabled = config.getBoolean("enabled")
                if (enabled) {
                    map + (key -> config)
                } else {
                    map
                }
            }
        }
    }

    def apply() = config

    def users = getMap("users") filterKeys {
        (key) => {
            if (validUserPattern.findFirstIn(key) == None) {
                throw new IllegalArgumentException {
                    s"Illegal user name '$key'; must match '$validUserPattern'!"
                }
            }
            true
        }
    }

    def spaces = getMap("spaces") filterKeys {
        (key) => {
            if (validSpacePattern.findFirstIn(key) == None) {
                s"Illegal space name '$key'; must match '$validSpacePattern'!"
            }
            true
        }
    }

}
