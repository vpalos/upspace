package com.oneandone.upspace.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.upspace.drivers._
import dispatch._, Defaults._
import java.security.MessageDigest
import org.apache.commons.codec.binary.Hex
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.concurrent.TrieMap
import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Manage the views in a Couchbase space.
 */
trait Views { self: CouchbaseDriver =>

    private val viewCache = TrieMap.empty[String, View]

    /**
     * Clean-up stale design-documents/views.
     * This relies on contacting the REST API of the first available node in the chain
     * to get all existing design-documents and then purges all views which have not been
     * used for more than a fixed, pre-established amount of time.
     */
    private def purgeViews() {
        val node = nodes.head
        val address = url(s"$node/default/buckets/$bucket/ddocs")
        val future = Http(address OK as.String)

        for (raw <- future) {
            val ddocs: List[String] = (parse(raw) \ "rows" \\ "doc" \\ "meta" \\ "id" \\ classOf[JString]).toList

            // Exclude permanent indexes explicitly.
            val ids = ddocs
                .map(_.stripPrefix("_design/"))
                .filterNot(_.startsWith("dev_permanent_"))
                .filterNot(_.startsWith("permanent_"))

            if (ids.isEmpty) {
                log.info(s"No design documents found, nothing to check for purging at this time.")
            } else {
                log.info(s"Checking available design documents for purging.")
                val allDDocs = ((ids map ("/~index/" + _)) zip ids).toMap
                val oldDDocs = (allDDocs -- client.getBulk(allDDocs.keySet).keySet()).values.toList
                if (oldDDocs.isEmpty) {
                    log.info(s"No obsolete design documents found, nothing to purge at this time.")
                } else {
                    log.warn(s"Purging the following obsolete design documents: ${oldDDocs.mkString(", ")}.")
                    for (oldDDoc <- oldDDocs) {
                        client.asyncDeleteDesignDoc(oldDDoc)
                    }
                    invalidateViews()
                }
            }
        }
    }

    private val indexLifetime = try {
        Math.max(connectionConfig.getMilliseconds("index.lifetime").toLong, 3.hours.toMillis).milliseconds
    } catch {
        case _: Exception => 7.days
    }
    private val purgeTimer = {
        val system = akka.actor.ActorSystem("system")
        system.scheduler.schedule(0 milliseconds, indexLifetime, new Runnable {
            def run() = {
                purgeViews()
            }
        })
    }

    private val digestCache = TrieMap.empty[String, String]
    private def digestView(key: String)(bodies: String*): String = {
        digestCache.get(key) match {
            case Some(value) =>
                value
            case None =>
                val messageDigest = MessageDigest.getInstance("SHA")
                val rawDigest = messageDigest.digest(bodies.mkString.getBytes("UTF-8"))
                val encDigest = Hex.encodeHexString(rawDigest).mkString
                digestCache += key -> encDigest
                encDigest
        }
    }

    protected def obtainView(viewName: String, map: String, reduce: String = null, permanent: Boolean = false) = {
        val designDocumentName = permanent match {
            case true => s"permanent_$viewName"
            case false => s"volatile_$viewName"
        }
        val viewDigest = digestView(viewName)(map, reduce)
        val stampName = s"/~index/$designDocumentName"
        val expireDate = (System.currentTimeMillis / 1000).toInt + indexLifetime.toSeconds

        try {
            viewCache.get(designDocumentName) match {
                case Some(view) =>
                    if (!permanent) {
                        new Thread {
                            client.touch(stampName, expireDate.toInt)
                        }.start()
                    }
                    log.debug(s"Invoking view '$viewName' from run-time cache.")
                    view
                case None =>
                    if (!permanent) {
                        val cas = client.getAndTouch(stampName, expireDate.toInt)
                        val fresh = cas match {
                            case null => false
                            case value => value.getValue.toString == viewDigest
                        }
                        if (!fresh) {
                            client.deleteDesignDoc(designDocumentName)
                            throw new InvalidViewException()
                        }
                    }
                    val view = client.getView(designDocumentName, viewName)
                    viewCache += designDocumentName -> view
                    log.debug(s"Caching and invoking stored view '$viewName' from database.")
                    view
            }
        } catch {
            case _: InvalidViewException => {
                val designDocument = new DesignDocument(designDocumentName)
                val viewDesign = reduce match {
                    case null => new ViewDesign(viewName, map)
                    case _ => new ViewDesign(viewName, map, reduce)
                }
                designDocument.setView(viewDesign)
                if (!permanent) {
                    client.set(stampName, expireDate.toInt, viewDigest)
                }
                client.asyncCreateDesignDoc(designDocument)
                log.warn(s"Creating view '$viewName' into database, discarding current request!")
                null
            }
        }
    }

    protected def invalidateViews() {
        viewCache.clear()
    }

    protected def shutdownViews() {
        purgeTimer.cancel()
        digestCache.clear()
        invalidateViews()
    }

}
