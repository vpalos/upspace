package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.handlers._
import com.oneandone.upspace.drivers._
import com.oneandone.upspace.unit.Defaults
import org.junit.runner._
import org.scalatra.test.specs2._
import org.specs2.runner._
import org.specs2.mock._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import com.oneandone.upspace.servlets.{SpaceServlet, HelpSwagger}
import com.typesafe.config.ConfigFactory

@RunWith(classOf[JUnitRunner])
class SpaceHandlerPostTest
    extends MutableScalatraSpec
    with Mockito
    with Defaults {

    implicit protected val swagger = new HelpSwagger
    implicit protected val user = ""

    val mockSpace = mock[Driver]
    val validRequestBody : JValue = parse( """{"dummy":"data"}""")
    val emptyDocument: JValue = parse("{}")

    "POST /document" should {

        mockSpace create("/new/path", validRequestBody) returns OpSuccess()
        "return 201(Created) without expire" in {
            val headers = defaultHeaders + ("Content-Type" -> "application/json")
            post("/new/path", compact(validRequestBody).getBytes("UTF-8"), headers) {
                status must_== 201
                there was one(mockSpace).create("/new/path", validRequestBody, 0)
            }
        }

        mockSpace create("/new/path/with/expire", validRequestBody, 1) returns OpSuccess()
        "return 201(Created) with expire" in {
            val headers = defaultHeaders + ("Content-Type" -> "application/json",                                            "X-Param-Expire" -> "1")
            post("/new/path/with/expire", compact(validRequestBody).getBytes("UTF-8"), headers) {
                status must_== 201
                there was one(mockSpace).create("/new/path/with/expire", validRequestBody, 1)
            }
        }

        mockSpace create("/empty/request", emptyDocument, 0) returns OpSuccess()
        "return status 200(OK) for an empty request" in {
            val headers = defaultHeaders + ("Content-Type" -> "application/json")
            post("/empty/request", "", headers) {
                status must_== 201
                there was one(mockSpace).create("/empty/request", emptyDocument, 0)
            }
        }

        "return status 400(Bad Request) if the url is not valid" in {
            val headers = defaultHeaders + ("Content-Type" -> "application/json")
            post(invalidPath, compact(validRequestBody).getBytes("UTF-8"), headers) {
                status must_== 400
                there was no(mockSpace).create(invalidPath, validRequestBody, 0)
            }
        }

        "return 415(Unsupported Media Type) if Content-Type header is wrong" in {
            val headers = defaultHeaders + ("Content-Type" -> "text/html")
            post("/new/path/with/bad/header", compact(validRequestBody).getBytes("UTF-8"), headers) {
                status must_== 415
                there was no(mockSpace).create("/new/path/with/bad/header", validRequestBody, 0)
            }
        }

        mockSpace create("/existing/path", validRequestBody) returns OpConflict()
        "return 409(Conflict) if a document with the same path already exists" in {
            val headers = defaultHeaders + ("Content-Type" -> "application/json")
            post("/existing/path", compact(validRequestBody).getBytes("UTF-8"), headers) {
                status must_== 409
                there was one(mockSpace).create("/existing/path", validRequestBody, 0)
            }
        }

        "return 400(BadRequest) if the request viewBody is not a valid JSON" in {
            val invalidJSON : String = """{"dummy":"data}"""
            val headers = defaultHeaders + ("Content-Type" -> "application/json")
            post("/existing/path", invalidJSON .getBytes("UTF-8"), headers) {
                status must_== 400
            }
        }
    }

    //todo: Remove this stupid line
    private val config = ConfigFactory.load("upspace")
    addServlet(new SpaceServlet("/", config), "/*")
}