package com.oneandone.upspace.handlers

import com.oneandone.upspace.helpers._
import com.oneandone.upspace.servlets._
import org.scalatra.swagger._

/**
 * Handles the PUT operations for space servlets.
 */
trait SpacePutHandler
    extends ServletHelper
    with HelpHelper { self: SpaceServlet =>

    put("/*") {
        val path = sanitizePath(params("splat"))
        val expire = request.headers.getOrElse("X-Param-Expire", "0").toInt
        val merge = request.headers.getOrElse("X-Param-Merge", "false").toBoolean
        val jsonBody = sanitizeJson(parsedBody)
        respond {
            space.update(path, jsonBody, expire, merge)
        }
    }

    val putHelp = (apiOperation("putHelp", jsonDocumentModel)
        summary "Update the document which exists at the given path."

        notes """Updates an existing document associated with the given path identifier into the storage space.
                 The operation will fail if the document does not exist at the specified path."""

        error Error(415, "The 'Content-Type' header must be 'application/json'.")
        error Error(406, "The 'Accept' header must allow 'application/json' (i.e. 'Accept: */*').")
        error Error(404, "Specified document does not exist.")
        error Error(400, "Illegal document path or request viewBody is not valid JSON.")
        error Error(200, "The document was updated (i.e. replaced or merged) successfully.")

        parameter bodyParam(jsonDocumentModel)
            .description("The new JSON document viewBody; all null values are removed.")

        parameter pathParam[String]("path")
            .description("The document path identifier (path segments may not start with tilda, i.e. '~').")
            .defaultValue("a/b/c")
            .required

        parameter headerParam[String]("Accept")
            .description("The mime-type accepted by the client.")
            .allowableValues[String](List("application/json", "*/*"))
            .defaultValue("application/json")
            .required

        parameter headerParam[String]("Content-Type")
            .description("The mime-type of the document viewBody.")
            .allowableValues[String](List("application/json"))
            .defaultValue("application/json")
            .required

        parameter headerParam[Int]("X-Param-Expire")
            .description("""Automatically delete this document after a period of time (always in seconds).
                            The value 0 (zero) means no expiration; values over 30 days are interpreted as absolute timestamps (UNIX Epoch).""")
            .defaultValue(0)

        parameter headerParam[Boolean]("X-Param-Merge")
            .description("""If false, the new document replaces the old one (default);
                            if true, the new document is merged over the old one (i.e. old existing fields are preserved).
                            All fields set to null are deleted from the final document.""")
            .defaultValue(false)
        )
    put("/:path", operation(putHelp)) { pass() }


}
