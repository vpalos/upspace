package com.oneandone.upspace.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.upspace.drivers._
import com.oneandone.upspace.helpers._
import javax.xml.bind.DatatypeConverter
import org.json4s.jackson.Serialization
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._

/**
 * Implements search/filtering functionality for the Couchbase driver.
 */
trait Search { self: CouchbaseDriver with Driver =>

    def searchView(prefix: String, mapFields: List[SearchField]) = {
        val mapCode = {
            loadResource("views/search-map.js")
                .replaceFirst("%%prefix%%", Serialization.write(prefix))
                .replaceFirst("%%fields%%", Serialization.write(mapFields))
        }
        val salt = Serialization.write(mapFields.map(_.label))
        obtainView("search_" + DatatypeConverter.printBase64Binary(salt.getBytes), mapCode)
    }

    def search(userId: String,
               prefix: String,
               rules: Map[SearchField, List[SearchRule]],
               sortFields: List[SearchField],
               sortDirection: SortDirection = SortAscending(),
               offset: Int, limit: Int, depth: Int = 0, count: Boolean = false): OpResult = {
        catchFailures {
            val (queryEqualityFields, queryEqualityValues) = {
                rules map {
                    case (_field, _rules) =>
                        val constraint = _rules.find(_.isInstanceOf[SearchRuleEquals])
                        if (constraint.nonEmpty && constraint.get.controlValues.size == 1) {
                            (_field, constraint.get.controlValues.head)
                        } else {
                            (null, null)
                        }
                }
            }.filter(_._1 != null).toList.sortBy(_._1.label).unzip

            val idField = SearchField("_", "id")
            val querySortFields = (sortFields :+ idField).distinct.diff(queryEqualityFields)

            val (queryFilterFields, queryFilterRules) = {
                (rules -- queryEqualityFields).toList.sortBy(_._1.label).unzip
            }

            val constraintField = querySortFields.head
            val (viewPrefix, startKey, endKey) = {
                if (constraintField == idField) {
                    (null, prefix, s"$prefix\uefff")
                } else {
                    val limits = rules.getOrElse(constraintField, List.empty).map {
                        case gt: SearchRuleGreaterThan => (gt.controlValue, null)
                        case lt: SearchRuleLessThan => (null, lt.controlValue)
                        case _ => (null, null)
                    }
                    val start = limits.find(_._1 != null).getOrElse((null, null))._1
                    val end = limits.find(_._2 != null).getOrElse((null, "\uefff"))._2
                    (prefix, start, end)
                }
            }

            val viewFields = queryEqualityFields ++ querySortFields ++ queryFilterFields
            val searchViewCode = searchView(viewPrefix, viewFields)
            if (searchViewCode == null) {
                return OpDeferred()
            }

            val (descending, fromKey, toKey) = sortDirection match {
                case SortAscending() => (false, startKey, endKey)
                case SortDescending() => (true, endKey, startKey)
            }

            val query = new Query()
            query.setInclusiveEnd(true)
            query.setStale(indexForceUpdate)
            query.setDescending(descending)
            query.setIncludeDocs(false)
            query.setReduce(false)

            val rangeStart = Serialization.write {
                if (fromKey != null) {
                    queryEqualityValues :+ fromKey
                } else {
                    queryEqualityValues
                }
            }
            query.setRangeStart(rangeStart)

            val rangeEnd = Serialization.write {
                if (toKey != null) {
                    queryEqualityValues :+ toKey
                } else {
                    queryEqualityValues
                }
            }
            query.setRangeEnd(rangeEnd)

            log.debug(s"Search range: $rangeStart --> $rangeEnd")

            val pageSize = List(100, limit, offset).max
            val segments = client.paginatedQuery(searchViewCode, query, pageSize)

            val required = offset + limit
            val unneeded = queryEqualityFields.size + querySortFields.size

            def advance(segments: Paginator, required: Int): List[String] = {
                if (!segments.hasNext || (!count && required <= 0 && limit > 0)) {
                    return List.empty
                }

                val rawChunk = segments.next()
                val unfilteredChunk = rawChunk.map {
                    row => (row.getId, parse(row.getKey).values.asInstanceOf[List[Any]].drop(unneeded))
                }
                val filteredChunk = unfilteredChunk.filter {
                    case (_, values) =>
                        values.zip(queryFilterRules) forall {
                            case (value, rule) =>
                                rule.forall(_.matches(value))
                        }
                }

                filteredChunk.map(_._1).toList ::: advance(segments, required - filteredChunk.size)
            }

            val filtered = advance(segments, required)

            if (count) {
                OpSuccess(filtered.size.toString)
            } else {
                if (limit < 0) {
                    renderDepth(filtered.drop(offset), depth)
                } else {
                    renderDepth(filtered.slice(offset, offset + limit), depth)
                }
            }
        }
    }


}
