package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.helpers._
import org.junit.runner.RunWith
import org.mockito.Matchers
import org.specs2.runner.JUnitRunner
import com.oneandone.upspace.unit.helpers.TestHelper
import com.oneandone.upspace.servlets.SpaceServlet
import com.oneandone.upspace.drivers.Driver

@RunWith(classOf[JUnitRunner])
class SearchHandlerTest
    extends TestHelper {

    implicit val mockDriver = mock[Driver]

    "GET /~search/prefix" should {

        "return status 406 if the Accept header is wrong" in {
            val headers = defaultHeaders + ("Accept" -> "text/html")
            mockDriver.search(
                Matchers.eq("/wrong/headers"),
                any[Map[SearchField, List[SearchCriteria]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("{'test': 'test'}")
            get("/~search/wrong/headers", Seq.empty, headers) {
                status must_== 406
                there was no(mockDriver).search(
                    Matchers.eq("/wrong/headers"),
                    any[Map[SearchField, List[SearchCriteria]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 200(OK) for an existing document" in {

            mockDriver.search(
                Matchers.eq("/existing/path"),
                any[Map[SearchField, List[SearchCriteria]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("{'test': 'test'}")
            get("/~search/existing/path", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).search(
                    Matchers.eq("/existing/path"),
                    any[Map[SearchField, List[SearchCriteria]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 200(OK) if the url is a valid encoded path" in {
            mockDriver.search(
                Matchers.eq("/encoded/path"),
                any[Map[SearchField, List[SearchCriteria]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("{'test': 'test'}")
            get("/~search/encoded%2Fpath", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).search(
                    Matchers.eq("/encoded/path"),
                    any[Map[SearchField, List[SearchCriteria]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 400(Bad Request) for an unconstrained prefix" in {
            get("/~search", Seq.empty, defaultHeaders) {
                status must_== 400
            }
        }


        "return status 400(Bad Request) for an invalid query (multiple rule types per field)" in {
            get("/~search", Map("foo" -> "=bar", "foo" -> ">23"), defaultHeaders) {
                status must_== 400
            }
        }
    }

    addServlet(new SpaceServlet("/UTSpace", "/UTSpace", utSpaceConfig), "/*")


}
