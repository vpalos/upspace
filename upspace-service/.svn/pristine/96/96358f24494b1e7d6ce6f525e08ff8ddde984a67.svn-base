package com.oneandone.persistence.upspace.service.actors

import akka.actor._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import spray.http._
import spray.servlet.ServletRequestInfoHeader

class DispatcherActor(val prefix: String)
    extends Actor
    with RoutingHelper
    with ActorLogging {

    if (isRoot) {
        val space = ConfigManager.options.getString("root.space")
        val config = ConfigManager.spaces(space)
        context.actorOf(Props(classOf[SpaceActor], space, config), "~root")
    } else {
        ConfigManager.spaces.map {
            case (space, config) =>
                val spaceName = space.tail
                context.actorOf(Props(classOf[SpaceActor], spaceName, config), spaceName)
        }
    }

    // TODO: /~monitor
    // TODO: /~trigger

    /*
        override def init(context: ServletContext) {
            context.initParameters("org.scalatra.environment") = "production"

            spaceServlets foreach {
                case (mountPoint, spaceServlet) =>
                    log.info(s"Exposing space '$mountPoint'!")
                    context.mount(spaceServlet, mountPoint, mountPoint)
            }

            if (spaceServlets.size == 0) {
                log.error("Could not find any enabled spaces in the configuration.")
            }

            if (ConfigManager.options.getBoolean("help.enabled")) {
                log.info(s"Mounting reserved space '/~help' into servlet context!")
                context.mount(new HelpServlet, "/~help", "/~help")
            }
        }

        override def destroy(context: ServletContext) {
            for (spaceServlet <- spaceServlets.values) {
                spaceServlet.shutdown()
            }
        }
    */

    private val prefixLength = prefix.length
    private val spacePattern = "^/([^~/]*)(/.*)$".r

    def receive = {
        case Timedout(request: HttpRequest) =>
            sender ! HttpResponse(503, "Operation took too long to complete!")

        case request: HttpRequest =>

            request.header[ServletRequestInfoHeader].get.hsRequest.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true)

            try {

                val (space, path) = {
                    val innerPath = request.uri.path.dropChars(prefixLength).toString()
                    if (isRoot) {
                        ("~root", innerPath)
                    } else {
                        innerPath match {
                            case spacePattern(s, p) => (s, p)
                            case _ => throw new Exception(s"URI path '${request.uri.path}' is invalid.")
                        }
                    }
                }

                context.child(space) match {
                    case Some(target) => target forward request.copy(uri = request.uri.copy(path = Uri.Path(path)))
                    case None => throw new Exception(s"Space '$space' is not mounted.")
                }

            } catch {
                case e: Exception =>
                    sender ! HttpResponse(404, e.getMessage)
            }
    }

}