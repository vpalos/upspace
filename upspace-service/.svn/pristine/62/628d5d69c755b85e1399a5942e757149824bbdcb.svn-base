package com.oneandone.upspace.unit.handlers

import com.oneandone.upspace.helpers._
import org.junit.runner.RunWith
import org.mockito.Matchers
import org.specs2.runner.JUnitRunner
import com.oneandone.upspace.unit.helpers.TestHelper
import com.oneandone.upspace.servlets.SpaceServlet
import com.oneandone.upspace.drivers.Driver

@RunWith(classOf[JUnitRunner])
class SearchHandlerTest
    extends TestHelper {

    implicit val mockDriver = mock[Driver]

    "GET /~search/prefix" should {

        "return status 406 if the Accept header is wrong" in {
            val headers = defaultHeaders + ("Accept" -> "text/html")
            mockDriver.search(
                any[String],
                Matchers.eq("/wrong/headers"),
                any[Map[SearchField, List[SearchRule]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("""{"test": "test"}""")
            get("/~search/wrong/headers", Seq.empty, headers) {
                status must_== 406
                there was no(mockDriver).search(
                    any[String],
                    Matchers.eq("/wrong/headers"),
                    any[Map[SearchField, List[SearchRule]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 200(OK) for an existing document" in {

            mockDriver.search(
                any[String],
                Matchers.eq("/existing/path"),
                any[Map[SearchField, List[SearchRule]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("""{"test": "test"}""")
            get("/~search/existing/path", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).search(
                    any[String],
                    Matchers.eq("/existing/path"),
                    any[Map[SearchField, List[SearchRule]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 200(OK) if the url is a valid encoded path" in {
            mockDriver.search(
                any[String],
                Matchers.eq("/encoded/path"),
                any[Map[SearchField, List[SearchRule]]],
                any[List[SearchField]],
                any[SortDirection],
                anyInt, anyInt, anyInt, any[Boolean]) returns OpSuccess("""{"test": "test"}""")
            get("/~search/encoded%2Fpath", Seq.empty, defaultHeaders) {
                status must_== 200
                there was one(mockDriver).search(
                    any[String],
                    Matchers.eq("/encoded/path"),
                    any[Map[SearchField, List[SearchRule]]],
                    any[List[SearchField]],
                    any[SortDirection],
                    anyInt, anyInt, anyInt, any[Boolean])
            }
        }

        "return status 400(Bad Request) for an invalid query (empty field name)" in {
            get("/~search", Map("" -> "=bar"), defaultHeaders) {
                status must_== 400
            }
        }

        "return status 400(Bad Request) for an invalid query (invalid value)" in {
            get("/~search", Map("b:foo" -> "=bar"), defaultHeaders) {
                status must_== 400
            }
        }

        "return status 400(Bad Request) for an invalid query (empty value)" in {
            get("/~search", Map("b:foo" -> ""), defaultHeaders) {
                status must_== 400
            }
        }

    }

    addServlet(new SpaceServlet("/UTSpace", "/UTSpace", utSpaceConfig), "/*")
}

// TODO: switch from using Mockito to another mock framework (e.g. ScalaMock)