package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import java.util.Date
import javax.xml.bind.DatatypeConverter
import org.json4s.jackson.Serialization
import org.json4s.jackson.JsonMethods._
import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.language.implicitConversions
import scala.reflect.ClassTag
import scala.util._

/**
 * Implements search/filtering functionality for the Couchbase driver.
 */
trait Search extends Formulae { self: CouchbaseDriver with Driver =>

    def searchView(prefix: String, fields: List[String]) = {
        val mapCode = {
            loadResource("views/search-map.js")
                .replaceFirst("%%prefix%%", Serialization.write(prefix))
                .replaceFirst("%%fields%%", fields.mkString("[", ",", "]"))
        }
        val salt = digestView(fields.toString())(fields.toString())
        obtainView("search_" + salt, mapCode)
    }

    def search(prefix: String,
               rules: List[Rule],
               sorts: List[Formula],
               direction: Direction = Ascending(),
               offset: Int, limit: Int, depth: Int = 0, count: Boolean = false)
              (implicit user: String): Future[OpResult] = {

        val promise = Promise[OpResult]()

        val rulesTransforms = rules.groupBy { rule =>
            transform(rule.formula)
        } mapValues {
            _.unzip(rule => (null, rule.criteria))._2
        }
        val sortTransforms = sorts.map(transform)

        val (equalFormulas, equalValues) = {
            rulesTransforms map {
                case (_transform, _criteria) =>
                    _criteria.find(_.operator == "==") match {
                        case Some(criteria) =>
                            (_transform, _transform.result.cast(criteria.operand))
                        case None =>
                            (null, null)
                    }
            }
        }.filter(_._1 != null).toList.sortBy(_._1.result.code).unzip

        val sortFormulas = (sortTransforms :+ IdTransform()).distinct.diff(equalValues)
        
        val filterTransforms = {
            (rulesTransforms -- equalFormulas) map {
                case (_transform, _list) =>
                    _transform -> {
                        _list map { criteria =>
                            Check(criteria.operator, _transform.result.cast(criteria.operand))(_transform.result.ord)
                        }
                    }
            }
        }.toList.sortBy(_._1.result.code)
        val (filterFormulas, filterCheckLists) = filterTransforms.unzip
        val filterChecks = filterCheckLists.flatten

        val (viewPrefix, startKey, endKey) = sortFormulas.head match {
            case IdTransform() =>
                (null, prefix, s"$prefix\uefff")
            case constraint =>
                val limits = rulesTransforms.getOrElse(constraint, List.empty).map {
                    case Criteria(">", value) => (value, null)
                    case Criteria("<", value) => (null, value)
                    case _ => (null, null)
                }
                val start = limits.find(_._1 != null).getOrElse((null, null))._1
                val end   = limits.find(_._2 != null).getOrElse((null, "\uefff"))._2
                (prefix, start, end)
        }

        val (descending, fromKey, toKey) = direction match {
            case Ascending()  => (false, startKey, endKey)
            case Descending() => (true, endKey, startKey)
        }

        val query = new Query()
        query.setInclusiveEnd(true)
        query.setStale(indexStale)
        query.setDescending(descending)
        query.setIncludeDocs(false)
        query.setReduce(false)

        val rangeStart = Serialization.write {
            if (fromKey != null) {
                equalValues :+ fromKey
            } else {
                equalValues
            }
        }
        val rangeEnd = Serialization.write {
            if (toKey != null) {
                equalValues :+ toKey
            } else {
                equalValues
            }
        }
        query.setRangeStart(rangeStart)
        query.setRangeEnd(rangeEnd)

        def executeQuery(view: View) {
            val pageSize = List(100, limit, offset).max
            // fixme: could we make paginatedQuery be completely asynchronous (event-based)?
            val segments = client.paginatedQuery(view, query, pageSize)

            val required = offset + limit
            val unneeded = equalFormulas.size + sortFormulas.size

            //@tailrec
            def advance(segments: Paginator, required: Int): List[String] = {
                if (!segments.hasNext || (!count && required <= 0 && limit > 0)) {
                    return List.empty
                }

                val rawChunk = segments.next()
                val unfilteredChunk = rawChunk.map {
                    row => (row.getId, parse(row.getKey).values.asInstanceOf[List[Any]].drop(unneeded))
                }
                val filteredChunk = unfilteredChunk.filter {
                    case (_, values) =>
                        values.zip(filterTransforms) forall {
                            case (_value, (transform, checks)) =>
                                val value = transform.result.cast(_value)
                                checks.forall(_.matches(value))
                        }
                }

                filteredChunk.map(_._1).toList ::: advance(segments, required - filteredChunk.size)
            }

            val filtered = advance(segments, List(required, limit).min)

            promise.success {
                if (count) {
                    OpSuccess(filtered.size.toString)
                } else {
                    // fixme: renderBulkDocumentDepth should be asynchronous
                    if (limit < 0) {
                        renderBulkDocumentDepth(filtered.drop(offset), depth)
                    } else {
                        renderBulkDocumentDepth(filtered.slice(offset, offset + limit), depth)
                    }
                }
            }
        }

        val viewFields = equalFormulas.map(t => (t.result.code, false)) ++
                         sortFormulas.map(t => (t.result.code, true)) ++
                         filterFormulas.map(t => (t.result.code, false))
        searchView(viewPrefix, viewFields.map(f => s"{ value: ${f._1}, sort: ${f._2} }")).onComplete {
            case Failure(failure) => promise.success(OpFailure(failure.getMessage))
            case Success(null)    => promise.success(OpDeferred())
            case Success(view)    => executeQuery(view)
        }

        promise.future
    }

}

// todo: support null in comparisons?
