## Count domains per TLD:

- Map:

        function (doc, meta) {

          if (meta.id.indexOf("/~") === 0 || meta.type !== "json") {
            return;
          }

          var parts = meta.id.split("/")

          if (parts[2] === "domains") {
            emit([parts[1], doc.name || ""], null);
          }
        }

- Reduce:

        _count

> http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_count_domains_per_tld?group=true&group_level=1&reduce=true&stale=false
> ### « [Interrogate!](http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_count_domains_per_tld?group=true&group_level=1&reduce=true&stale=false) »


## List domains per TLD:

- Map:

        function (doc, meta) {

          if (meta.id.indexOf("/~") === 0 || meta.type !== "json") {
            return;
          }

          var parts = meta.id.split("/")

          if (parts[2] === "domains") {
            var o = {};
            o[doc.name || ""] = doc.createDate || "";
            emit(parts[1], o);
          }
        }

- Reduce:

        function(key, values, rereduce) {
          function merge(prev, next) {
            for (var item in next) {
              if (next.hasOwnProperty(item)) {
                prev[item] = next[item];
              }
            }
            return prev
          }
          return values.reduce(merge, {})
        }

> http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_list_domains_per_tld?group=true&group_level=1&reduce=true&stale=false
> ### « [Interrogate!](http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_list_domains_per_tld?group=true&group_level=1&reduce=true&stale=false)»


## Count domains per TLD per Tenant:

- Map:

        function (doc, meta) {
          if (meta.id.indexOf("/~") === 0 || meta.type !== "json") {
            return;
          }

          var parts = meta.id.split("/")

          if (parts[2] === "domains") {
            var internals = doc.internals || {}
            var tenant = internals.tenantId || ""
                emit({tenant: tenant, tld: parts[1]}, doc.name || "");
          }
        }

- Reduce:

        _count

> http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_count_domains_per_tld_per_tennant?group=true&group_level=1&reduce=true&stale=false
> ### « [Interrogate!](http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_count_domains_per_tld_per_tennant?group=true&group_level=1&reduce=true&stale=false) »


## List domains per TLD per Tenant:

- Map:

        function (doc, meta) {
          if (meta.id.indexOf("/~") === 0 || meta.type !== "json") {
            return;
          }

          var parts = meta.id.split("/")

          if (parts[2] === "domains") {
            var internals = doc.internals || {}
            var tenant = internals.tenantId || ""
                emit({tenant: tenant, tld: parts[1]}, doc.name || "");
          }
        }

- Reduce:

        function(key, values, rereduce) {
          if (rereduce) {
            return values.reduce(function(a, b) {return a.concat(b)}).sort()
          } else {
            return values;
          }
        }

> http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_list_domains_per_tld_per_tennant?group=true&group_level=1&reduce=true&stale=false
> ### « [Interrogate!](http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_list_domains_per_tld_per_tennant?group=true&group_level=1&reduce=true&stale=false) »


http://hontldregpersmwbs01.cname.lan:8092/domains/_design/permanent_gastats/_view/permanent_gastats_list_first_domains?startkey=%222014-02-05T18%3A00%3A00.000Z%22&reduce=false&stale=false&limit=30


---
["2014-02-05T16:00:00.000Z"]