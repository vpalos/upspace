Performs a filtering (i.e. a selection) operation over the entire space (database).

### Globbing (i.e. prefix-based filtering)
You can select documents based on a prefix of their paths, e.g. select all documents starting with "/foo/bar/" by using URL "/~search/foo/bar/". This
### Expression-based filtering
You may add filtering criteria based on document fields in the URI query string. You may add multiple such criteria and they will **all** have to match a given document for it to be returned in the search results.

The accepted syntax for a search criteria is on of the following:

    <field>=[<operator>]<value>
    <cast>(<field>)=[<operator>]<value>
    <cast>(<field>).<modifier>=[<operator>]<value>


**Where:**

- **`<field>`**: a dot-separated path of a given field from the document e.g. `movies.sf.Terminator`. You may also use array notation where needed (e.g. if a field's name contains special characters) such as `movies.action["Die Hard"]`;\
- **`<operator>`**: any one of:
    - `=`: field expression must **equal** `<value>` (default if `<operator>` is not given);
    - `!=`: field expression must **not equal** `<value>`;
    - `<`: field expression must be **less than** `<value>`;
    - `<=`: field expression must be **less than or equal** to `<value>`;
    - `>`: field expression must be **greater than** `<value>`;
    - `>=`: field expression must be **greater than or equal** to `<value>`;
    - `~`: field expression must **match** `<value>` which is a **regular expression**.
- **`<cast>`**: given that the stored data is in JSON format which is loosely typed, UpSpace can not guarantee how filtering comparisons are made (e.g. value `11` is **less** than  `101` is considered as numbers but if considered as strings the opposite is true). `<cast>` enables the user to force a filtering comparison to be made using an explicit type; valid cast types are (case **insensitive**):
    - **`string`**: compare values lexicographically;
    - **`number`**: compare values numerically;
    - **`boolean`**: interpret values as booleans;
    - **`date`**: interpret values using the JavaScript Date object;
- **`<modifier>`**: casted fields may have a modifier attached which performs further modifications of the given document field (currently only `string` and `date` casts have modifiers);
    - **`string`**:
        - `length`: compare the length of the field's value (numerically);
        - `lowercase`: lowercases the field's value before comparison;
        - `uppercase`: uppercases the field's value before comparison;
    - **`date`**:
        - `year`, `month`, `day`, `hour`, `minute`, `second`, `millisecond`: extract the corresponding component of the field (as Date);
- **`<value>`**: any given value to compare the field expression against; this value will be interpreted using the same cast as the one used in the field expression (if given).

### Notes:
- All criteria (i.e. left and right sides of '=' sign) must be URL encoded.
- The available implemented modifiers are driver-dependent.