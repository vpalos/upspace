# Preparing the development environment

This document describes the required steps for installing and configuring the
required tools to start/resume development on the UpSpace persistence service
on you local machine.

**Warning**: The following instructions are specific to Ubuntu Linux distributions (13.04);
             on other systems you will most likely have to use alternative commands, although
             the same requirements apply everywhere.

## Install requirements on Ubuntu Linux

- **JDK 1.7**: the Oracle version of the JDK 1.7 is preferred (though OpenJDK could also work)

        $ sudo add-apt-repository ppa:webupd8team/java
        $ sudo apt-get update
        $ sudo apt-get install oracle-java7-installer

- **Couchbase Server 2.0**: a local running instance of Couchbase Server 2.0

        $ wget http://packages.couchbase.com/releases/2.0.1/couchbase-server-community_x86_64_2.0.1.deb
        $ sudo dpkg -i couchbase-server-community_x86_64_2.0.1.deb

    - **Note**: You may have (or want) to use a different file than the one used in here (i.e.
                **"couchbase-server-community_x86_64_2.0.1.deb"**) depending on your architecture
                (i.e. 32 or 64 bits) or latest available version. Check the available files on the
                [Couchbase downloads page](http://www.couchbase.com/download).

- **Maven 3**: UpSpace uses Maven as build system
- **IntelliJ IDEA**: this tutorial uses IntelliJ IDEA 12+ with some plugins
-