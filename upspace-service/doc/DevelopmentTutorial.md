UpSpace Development tutorial	{#welcome}
============================


This a very easy and short document that gets you up and running with **UpSpace** in no time.

> **NOTE:**
> This is a walkthrough for Ubuntu 13.10.

Table of contents

[TOC]


Install CouchBase
-----------------

**3.1** Download the ubuntu package from [CouchBase][5] with.
```
$ cd
$ wget http://packages.couchbase.com/releases/2.1.1/couchbase-server-community_x86_64_2.1.1.deb
$ sudo dpkg -i couchbase-server-community_x86_64_2.1.1.deb
$ rm couchbase-server-community_x86_64_2.1.1.deb
``` 
**3.2** Run couchbase server.

```
$sudo /etc/init.d/couchbase-server start
```

**3.2** Create a bucket.

In order to use UpSpace we should create a bucket. To do that you need to go to http://localhost:8091 (couchbase URL) and follow the instructions there.


Build WAR (Idea IntelliJ)
------------------------

### 1. Install JDK
Open console and run:

```
$ sudo apt-get install -y openjdk-7-jdk
``` 

> If you choose so, you can install [Oracle JDK][3] instead.

### 1. Install Idea
Download and install from [Jetbrains][1] (Duhh!).

### 2. Install Maven
Open console and run:

```
$ sudo apt-get install -y maven
``` 
Among others you should install Open JDK (if not allready installed). If you want you can install Oracle JDK from [Oracle][3].

### 4. Build

**4.1** Install Scala Plugin (Settings -> Plugins -> Install IntelliJ Plugin -> Scala)

**4.2** Import project from SVN at:

> https://svn.1and1.org/svn/domaindev/trunk/domain-registration-system/persistence-service

**4.3** You need to configure UpSpace to use the local couchbase instance. By default, UpSpace is configured to use the 1&1 dev instance. To change, edit the `src/main/resources/reference.conf` to the correct `pools` couchbase destination(E.g. http://localhost:8091/pools).

**4.4** Build war with **Maven**: Maven Project *(Right Panel)* -> upspace -> LifeCycle -> Package

>The WAR is usually built in ./target/upspace.war

> Alternatively run `mvn package` from the command line in the source directory.


Deploy WAR on servlet container
-------------------------------

### 1. Deploy on embedded Jetty (IntelliJ Idea)

You can deploy it via **Cargo** plugin on **Jetty** by running: Maven Project *(Right Panel)* -> upspace -> plugins -> cargo -> **cargo:run**.

### 2. Deploy on TomCat

**4.1** Install TomCat7
```
$ sudo apt-get install tomcat7
```

**4.2** Copy WAR

Copy the WAR from **[upspace folder]/target/upspace.war** to tomcat webapps folder.
```
$ sudo cp /home/*[Path to upspace]*/target/upspace.war  /var/lib/tomcat7/webapps
```

**4.3** Restart TomCat
```
$ sudo /etc/init.d/tomcat7 restart
```

### 3. Run 
You can access the service at http://localhost:10080


UpSpace API
-----------
UpSpace **API documentation** is based on **Swagger** and is accessible directly on service via **/~help** route. The good thing about this is that it can be accessed from anywhere as long as the UpSpace service is up.

To view it, you need to install SwaggerUI and access the UpSpace help URL.

### 1. Install SwaggerUI

**1.1** Download from [GitHub][4]

```
$ mkdir ~/swagger_ui
$ cd ~/swagger_ui
$ wget https://github.com/wordnik/swagger-ui/archive/master.zip && unzip master.zip

```

**1.2** Upload on a http server

You need to use a http server in order to run Swagger UI. If you can also deploy it on tomcat. For the purpose of this tutorial we are using Tomcat 7. 

Run:
```
$ cd /var/lib/tomcat7/webapps/
$ sudo mkdir swagger_ui
$ cd swagger_ui
$ sudo cp -Rf ~/swagger_ui/swagger-ui-master/dist/* . 
$ sudo /etc/init.d/tomcat7 restart
```

### 2. API documentation

Assuming have the upspace service up and running:

Open Swagger UI (Usually at http://localhost:10080/swagger_ui/).
Use the **/~help** url on swagger UI. (E.g.: ``http://localhost:10080/upspace/domains/~help``)

---
*~ Upspace Team ~*

  [1]: http://www.jetbrains.com/idea/
  [2]: http://couchdb.apache.org/
  [3]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
  [4]: https://github.com/wordnik/swagger-ui