#!/bin/bash 

####################################################################################
#  Before using this script please check/set following vars bellow:
#
#	JK_MANAGER_PORT - it's the port on which JK Status Manager can be accessed
#       DEPLOY_PORT - it's the tomcat deployment port for each container 
#
####################################################################################

unset http_proxy

usage() {
      echo "Usage: $0 [deploy|active|batch|check|console|debug|dump|errors|help|info|jvminfo|kill-all|logrotate|orphan|passive|restart|start|status|stop|stop-all] cluster"
      exit 1
}

get_inactive_container() {

local NODE=$1
local PORT=$2

if [ "$#" -ne 2 ]
then
  echo -e "Usage: get_inactive_container cluster_node_hostname apache_port"  >&2
  exit 1
fi

RESP=$(wget -S -O /tmp/$NODE 'http://'${NODE}:${PORT}'/-jkstatus?mime=txt' 2>&1 | grep "HTTP/" | awk '{print $2}')

if [[ $RESP != '200' ]]; then
        echo -e "Can\'t get jkstatus on node " $NODE  >&2
        rm -f /tmp/$NODE
        exit 1
else
 ACT_PORT=$(awk '{if (/ACT/ && /port=/)  {split($5,p,"="); print p[2]}}' /tmp/$NODE)
 DIS_PORT=$(awk '{if (/DIS/ && /port=/)  {split($5,p,"="); print p[2]}}' /tmp/$NODE)
fi 

if [[ ! -z $DIS_PORT  && $DIS_PORT =~ ^[0-9]{2}2[0-9]{2} ]]; then 
 echo  'tomcatB'
elif [[ ! -z $DIS_PORT && $DIS_PORT =~ ^[0-9]{2}1[0-9]{2} ]]; then 
 echo 'tomcatA'
else
 echo -e "Can\'t get disabled container on $NODE. Please check the apache on $NODE"  >&2
 exit 1
fi

rm -f /tmp/$NODE

}


PATH=$PATH:$HOME/software/gsc/bin

CMD="$1"
PROPS="$2"
DEPLOY=webapp-deploy
CONTROL=remote-control

PROPS="$2.properties"

if [ ! -f "$PROPS" ]; then
      echo "Missing file $PROPS"
      exit 1
fi

#source the prop file
. $PROPS

JK_MANAGER_PORT=$JK_STATUS_PORT

echo "Executing $CMD on $2"


if [ -z "$CMD" ]; then
      usage
fi

if [ -z "$2" ]; then
      usage
fi


NODE_LIST=(${SERVERS//,/ })
NODE_COUNT=${#NODE_LIST[@]}
NODE1=${NODE_LIST[0]}
DIS_CONTAINER=$(get_inactive_container $NODE1 $JK_MANAGER_PORT)
[[ -z $DIS_CONTAINER ]] && exit 1

if [[ $NODE_COUNT > 1 ]]; then 
   for i in `seq 1 $(( $NODE_COUNT -1 ))`; do
	NEXT_CONTAINER=$(get_inactive_container ${NODE_LIST[$i]} $JK_MANAGER_PORT)
        [[ -z $NEXT_CONTAINER ]] && exit 1
	if [[ $NEXT_CONTAINER != $DIS_CONTAINER ]]; then 
	  echo -e "Inactive container on ${NODE_LIST[$i]} different ${NODE_LIST[0]}"
	  exit 1	
	fi
   done
fi

echo -e "Disabled container is $DIS_CONTAINER"
echo -e "Start deployment on $DIS_CONTAINER"

TMP=$(mktemp)

if [ "$DIS_CONTAINER" = tomcatA ]; then
     DEPLOY_PORT=$TOMCATA_PORT
else
     DEPLOY_PORT=$TOMCATB_PORT
fi

echo "# Deployment properties to GSC
deploy.container.list: $SERVERS
deploy.tomcat.manager.password: $TOMCAT_MANAGER_PASSWORD
deploy.context: $DEPLOY_CONTEXT
deploy.port: $DEPLOY_PORT
deploy.war.url: $DEPLOY_WAR" > $TMP

case "$CMD" in
      deploy|update|list|reload)
     echo "$DEPLOY $SLOT from from $PROPS"
     DIRECTORY=`dirname $0`
     cd "$DIRECTORY"
     webapp-deploy --ignore-errors -N -p $TMP $CMD ; ret=$?
     [[ $ret != 0 ]] && echo -e "Deployment fail" && exit 1

     $CONTROL --defaults --timeout 180 --ignore-errors -q active "$SERVERS":"$TOOLUSER"/"$DIS_CONTAINER" -Dcontrol.survey.password="$SURVEY_PASSWORD" ; ret=$?
     [[ $ret != 0 ]] && echo -e "Switching the container fail" && exit 1

     ;;

      *)
     usage
     ;;
esac

rm $TMP

exit 0
