function (doc, meta) {

    function extract(target, path) {
        return path.reduce(
            function(hash, field) {
                return hash[field]
            },
            target
        )
    }

    function normalize(value) {
        return (value === undefined || (typeof(value) === 'number' && isNaN(value))) ? null : value;
    }

    function Meta(field) {
        var result = null;
        switch (field) {
            case "id":
                result = meta.id;
            default:
        }
        return result;
    }

    function produce(path, op) {
        var field = extract(doc, path);
        var value = null;
        try {
            field = normalize(field);
            value = field === null ? null : op.call(null, field);
        } catch(e) {}
        value = normalize(value);
        return typeof(value) === 'object' ? null : value;
    }

    var prefix = %%prefix%%;
    if (meta.type !== 'json' ||
        meta.id.indexOf('/~') === 0 ||
        (prefix && meta.id.indexOf(prefix) !== 0)) {
        return
    }

    var fields = %%fields%%;
    var values = [];
    for (var i = 0, l = fields.length; i < l; i++) {
      	var field = fields[i];
        if (field.value === null && !field.sort) {
            return
        }
        values.push(field.value)
    }

    emit(values, null)
}
