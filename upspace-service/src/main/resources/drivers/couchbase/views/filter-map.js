function (doc, meta) {
    if (meta.type !== 'json' || meta.id.indexOf('/~') === 0) {
        return
    }

    try {
        var id = meta.id;
        var document = doc;
        doc = undefined;
        meta = undefined;

        var selector = %%selector%%;

        if (selector(id, document) === true) {
            emit(meta.id, null);
        }
    } catch(e) {}
}