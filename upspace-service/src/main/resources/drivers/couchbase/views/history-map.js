function (doc, meta) {
  if (meta.type === "json" &&
    meta.id.indexOf("/~history/") === 0) {
      
    var co = meta.id.lastIndexOf('/')
    var id = meta.id.slice(0, co)
    var ts = meta.id.slice(co + 1)
    
    emit(id + '/~' + ts, {
      id: meta.id, 
      action: doc.action, 
      expire: new Date(doc.expire).getTime() || 0
    });
  }
}