function(key, values, rereduce) {
  if (rereduce) {
    values = Array.prototype.concat.apply([], values);
  }

  values.sort(function(a, b) {
    return a.id > b.id ? -1 : 1
  });

  var touch = null;
  for (var i = 0, l = values.length; i < l; i++) {
    var value = values[i]
    if (value.action === "touch") {
      touch = value;
    } else {
      return touch ?
        [touch, value] :
        [value];
    }
  }

  return []
}