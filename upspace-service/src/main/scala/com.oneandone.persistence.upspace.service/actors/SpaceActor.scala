package com.oneandone.persistence.upspace.service.actors

import akka.event.{Logging, LoggingAdapter}
import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import com.oneandone.persistence.upspace.service.routes._
import com.typesafe.config._
import org.json4s._
import scala.collection.JavaConverters._
import scala.concurrent._
import spray.http._
import spray.http.MediaTypes._
import spray.httpx.Json4sJacksonSupport
import spray.routing._
import spray.routing.authentication._
import spray.routing.directives._
import java.security.MessageDigest

class SpaceActor(val mount: String, val spaceName: String, spaceConfig: Config)
    extends HttpServiceActor
    with Json4sJacksonSupport
    with CreateRoute
    with ReadRoute
    with UpdateRoute
    with DeleteRoute
    with HistoryRoute
    with SearchRoute
    with RoutingHelper {

    implicit val json4sJacksonFormats = DefaultFormats
    implicit val actorSystem = context.system
    implicit val executionContext = actorSystem.dispatcher
    implicit val log = Logging.getLogger(context.system, this)

    protected val driver = Driver(spaceName, spaceConfig)
    RegistryHelper.drivers += spaceName -> driver

    private val securityConfig = spaceConfig.getConfig("security")
    private val isSecure = securityConfig.getBoolean("enabled")
    private val allowedUsers = ConfigManager.users(securityConfig)

    def receive = runRoute {
        detach() {
            securityVerifier { user =>
                respondWithMediaType(`application/json`) {
                    route(user)
                }
            }
        }
    }

    def securityVerifier(handle: String => Route): Route = {
        if (isSecure) {
            val strategy = securityConfig.getString("strategy")
            val engine = strategy match {
                case "http-basic" => 
                    BasicAuth(credentialsVerifier _, realm = mount)
                case unknown => return complete {
                    log.error(s"Unsupported security strategy: '$unknown'!")
                    HttpResponse(500)
                }
            }
            authenticate(engine) { username =>
                authorize(permissionsVerifier(allowedUsers.get(username).get) _) {
                    handle(username)
                }
            }
        } else {
            handle("")
        }
    }

    def credentialsVerifier(credentials: Option[UserPass]): Future[Option[String]] = Future {
        val valid = credentials.exists { case UserPass(username, password) =>
            allowedUsers.get(username).exists { config =>
                sha1(password).toLowerCase == config.getString("password").toLowerCase
            }
        }
        if (valid)
            Some(credentials.get.user)
        else
            None
    }

    private def sha1(input:String) : String = {
        if(input == null || input.isEmpty) ""
        else MessageDigest.getInstance("SHA-1").digest(input.getBytes).map("%02X".format(_)).mkString
    }

    def permissionsVerifier(userConfig: Config)(request: RequestContext): Boolean = {
        userConfig.getStringList("allowed.methods").asScala.map(_.toLowerCase).contains {
            request.request.method.value.toLowerCase
        }
    }

    val exceptionHandler = ExceptionHandler {

        // fixme: find a better way of responding than throwing exceptions
        case e: Response =>
            complete {
                HttpResponse(200, entity = e.getMessage)
            }
        case e: Deferred =>
            complete {
                HttpResponse(202)
            }
        case e: Complaint =>
            complete {
                HttpResponse(400, entity = e.getMessage)
            }
        case e: Exception =>
            complete {
                log.error(s"Service malfunction: $e")
                HttpResponse(500, entity = e.getMessage)
            }
    }

    // todo: response.timeout
    protected val maximumDepth    = ConfigManager.options.getInt("render.depth.maximum")
    protected val responseTimeout = ConfigManager.options.getMilliseconds("response.timeout")

    private def route(implicit user: String): Route = {
        clientIP { ip =>
            logRequestResponse(requestResponseLogger(ip) _) {

                val mountRoute = separateOnSlashes(mount.stripPrefix("/"))

                pathPrefix(mountRoute) {

                    handleExceptions(exceptionHandler) {

                        decompressRequest() {
                            compressResponseIfRequested() {

                                routeHistoryCommand ~
                                routeSearchCommand ~
                                routeReadDocument ~
                                routeCreateDocument ~
                                routeUpdateDocument ~
                                routeDeleteDocument

                            }
                        }
                    }
                }
            }
        }
    } ~ complete {
        HttpResponse(StatusCodes.MethodNotAllowed)
    }

    def requestResponseLogger(ip: RemoteAddress)(req: HttpRequest): Any => Option[LogEntry] = {
        case res: HttpResponse =>
            val state = s"${res.message.status.value} (${res.message.entity.data.length} bytes)"
            val entry = log.isDebugEnabled match {
                case false => LogEntry(s"($ip) ${req.method} ${req.uri} .. $state", Logging.InfoLevel)
                case true  => LogEntry(s"($ip) $req .. $state", Logging.DebugLevel)
            }
            Some(entry)
        case _ =>
            None
    }

    if (ConfigManager.isRoot) {
        log.info(s"Space '$spaceName' mounted under '$mount' (root mode).")
    } else {
        log.info(s"Space '$spaceName' mounted under '$mount'.")
    }
}

