package com.oneandone.persistence.upspace.service.actors

import akka.actor._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import spray.http._

class DispatcherActor(val prefix: String)
    extends Actor {

    if (ConfigManager.isRoot) {
        val space = ConfigManager.options.getString("root.space")
        val config = ConfigManager.spaces(space)
        val actor = context.actorOf(Props(classOf[SpaceActor], prefix, space, config), "~root")
        RegistryHelper.actors += "~root" -> actor
        actor
    } else {
        ConfigManager.spaces.map {
            case (space, config) =>
                val spaceName = space.tail
                val actor = context.actorOf(Props(classOf[SpaceActor], s"$prefix/$spaceName", space, config), spaceName)
                RegistryHelper.actors += spaceName -> actor
                actor
        }
    }

    if (ConfigManager.isHelp) {
        val actor = context.actorOf(Props(classOf[HelpActor], prefix), "~help")
        RegistryHelper.actors += "~help" -> actor
    }

    // TODO: /~monitor
    // TODO: /~trigger

    private val prefixLength = prefix.length
    private val spacePattern = "^/([^~/]*)/.*$".r
    private val helpPattern  = "^/~help(?:/.*)?$".r

    def receive = {
        case Timedout(request: HttpRequest) =>
            sender ! HttpResponse(503, "Operation took too long to complete!")

        case request: HttpRequest =>

            try {
                val innerPath = request.uri.path.dropChars(prefixLength).toString()

                val target = {
                    innerPath match {
                        case helpPattern() =>
                            "~help"
                        case _ =>
                            if (ConfigManager.isRoot) {
                                "~root"
                            } else {
                                innerPath match {
                                    case spacePattern(s) => s
                                    case _ => throw new Exception(s"URI path '${request.uri.path}' is invalid.")
                                }
                            }
                    }
                }

                context.child(target) match {
                    case Some(actor) =>
                        actor forward request
                    case None =>
                        throw new Exception(s"Invalid URL ('$target' is not mounted).")
                }

            } catch {
                case e: Exception =>
                    sender ! HttpResponse(404, e.getMessage)
            }
    }

}