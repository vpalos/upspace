package com.oneandone.persistence.upspace.service.actors

import akka.event.Logging
import spray.routing._
import com.oneandone.persistence.upspace.service.managers.ConfigManager
import scala.language.implicitConversions
import spray.http.MediaTypes._
import spray.http.HttpResponse
import com.github.jknack.handlebars.io.{ClassPathTemplateLoader}
import com.github.jknack.handlebars.{Helper, Handlebars, Options}
import collection.JavaConverters._

class HelpActor(val mount: String)
    extends HttpServiceActor {

    implicit val log = Logging.getLogger(context.system, this)

    private val basePath: String = s"$mount"

    private val handlebars = new Handlebars(new ClassPathTemplateLoader("/help", ".json"));
    handlebars.registerHelper("array", new Helper[Set[String]]() {
        def apply(elems: Set[String], options: Options) = {
            elems.map(elem => options.apply(options.fn, elem).toString).mkString("[", ",", "]")
        }
    })

    private val apiDocs = prepareApiDocs

    private val apiSpecs = prepareApiSpecs

    /**
     * Load api-docs.json and populate template based on declared spaces.
     *
     * @return swagger resource listing
     */
    def prepareApiDocs(): String = {
        handlebars.compile("api-docs").apply(
            Map(
                "basePath" -> basePath,
                "spaces" -> ConfigManager.spaces.keys
            ).asJava
        )
    }

    /**
     * Load api.json and populate template based on declared spaces.
     *
     * @return swagger api declaration
     */
    def prepareApiSpecs: Map[String, String] = {
        ConfigManager.spaces.map {
            case (space, _) =>
                space.tail -> handlebars.compile("api").apply(
                    Map(
                        "basePath" -> basePath,
                        "space" -> {
                            if (ConfigManager.isRoot) "" else space
                        }
                    ).asJava
                )
        }
    }

    def receive = runRoute {
        val mountRoute = separateOnSlashes(mount.stripPrefix("/"))
        get {
            pathPrefix(mountRoute) {
                detach() {
                    pathPrefix("~help") {
                        pathEndOrSingleSlash {
                            respondWithMediaType(`application/json`) {
                                routeResourceListing
                            }
                        } ~
                            pathPrefix("~swagger") {
                                getFromResourceDirectory("/help/swagger")
                            } ~
                            path(RestPath) {
                                space => respondWithMediaType(`application/json`) {
                                    routeSpaceReference(space.toString)
                                }
                            }
                    }
                }
            }
        }
    }

    def routeResourceListing = {
        complete {
            apiDocs
        }
    }

    def routeSpaceReference(space: String) = {
        apiSpecs.get(space) match {
            case Some(target) =>
                complete {
                    target
                }

            case None =>
                complete {
                    HttpResponse(404)
                }
        }
    }
}

