package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors.SpaceActor
import scala.language.postfixOps

trait DeleteRoute { self: SpaceActor =>

    protected def routeDeleteDocument(implicit user: String) =
        delete {
            extractDocumentPath { path =>

                complete {
                    driver
                        .delete(path)
                        .map(response)
                }
            }
        }

}