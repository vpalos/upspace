package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers._
import org.json4s._
import scala.language.postfixOps
import spray.http._
import spray.http.HttpHeaders._
import spray.httpx.unmarshalling._
import spray.httpx.unmarshalling.FromStringDeserializers._

trait CreateRoute { self: SpaceActor =>

    protected def routeCreateDocument(implicit user: String) =
        post {
            extractDocumentPath { path =>
                extractJsonBody { body =>

                    optionalHeaderValueByName("X-Param-Expire") { headerExpire =>

                        parameters('expire.as[Int] ?) { expire =>
                            requestUri { uri =>

                                val _expire = Expire(expire).orElse(Expire(headerExpire))

                                complete {
                                    driver
                                        .create(path, body, _expire)
                                        .map {
                                            case success: OpSuccess =>
                                                HttpResponse(201, headers = List(Location(uri.path.toString())))
                                            case result: OpResult =>
                                                response(result)
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

}

// TODO: Remove "X-Param-Expire" (deprecated).