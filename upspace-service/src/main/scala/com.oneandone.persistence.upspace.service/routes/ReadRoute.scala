package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers._
import scala.language.postfixOps
import spray.httpx.unmarshalling._
import spray.httpx.unmarshalling.FromStringDeserializers._

trait ReadRoute { self: SpaceActor =>

    protected def routeReadDocument(implicit user: String) =
        get {
            extractDocumentPath { path =>

                optionalHeaderValueByName("X-Param-Expire") { headerExpire =>

                    parameters('expire.as[Int] ?, 'depth.as[Int] ? 1) { (expire, depth: Int) =>

                        if (depth > maximumDepth) {
                            throw new Complaint(s"Maximum allowed depth level for this space is $maximumDepth!")
                        }

                        val _expire = Expire(expire).orElse(Expire(headerExpire))
                        //TODO: deny expire if delete method is denied for current user (also applies to PUT and POST)
                        complete {
                            driver
                                .read(path, depth, _expire)
                                .map(response)
                        }
                    }
                }
            }
        }

}

// TODO: Remove "X-Param-Expire" (deprecated).