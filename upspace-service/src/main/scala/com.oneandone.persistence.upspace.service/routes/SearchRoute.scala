package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.managers._
import org.json4s._
import scala.concurrent.Future
import scala.language.postfixOps
import spray.http._
import spray.httpx.marshalling._
import spray.httpx.unmarshalling._
import spray.httpx.unmarshalling.FromStringDeserializers._
import MediaTypes._

trait SearchRoute
    extends CastHelper
    with SearchHelper { self: SpaceActor =>

    protected def routeSearchCommand(implicit user: String) = {
        def search(path: String) = {

            optionalHeaderValueByName("X-Param-Force-Index") { headerForceIndex =>

                parameters (
                    'depth.as[Int] ? 0,
                    'offset.as[Int] ? 0,
                    'limit.as[Int] ? -1,
                    'count.as[Boolean] ? false,
                    'direction ? "a") {
                    (depth: Int, offset, limit: Int, count: Boolean, direction) =>

                        parameterMultiMap { parameters =>

                            if (depth > maximumDepth) {
                                throw new Complaint(s"Maximum allowed depth level for this space is $maximumDepth!")
                            }

                            val sorts: List[FieldExpression] = parameters.getOrElse("sort", List.empty).map(parseFormula)

                            val criteria = parseCriteria {
                                parameters -- List("depth", "offset", "limit", "count", "direction", "sort")
                            }

                            complete {
                                driver
                                    .search(path,
                                        criteria, sorts,
                                        Direction(direction),
                                        offset, limit, count, depth,
                                        headerForceIndex.getOrElse("false").toLowerCase == "true")
                                    .map(streamResponse)
                            }
                        }
                }
            }
        }

        get {
            specialCommandPath("search") {
                pathEndOrSingleSlash {
                    search("")
                } ~
                extractDocumentPath { path =>
                    search(path)
                }
            }
        }
    }

}
