package com.oneandone.persistence.upspace.service.routes

import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers._
import java.util.Date
import scala.language.postfixOps
import spray.httpx.unmarshalling._
import spray.httpx.unmarshalling.FromStringDeserializers._

trait HistoryRoute
    extends CastHelper { self: SpaceActor =>

    protected def routeHistoryCommand(implicit user: String) =
        get {
            specialCommandPath("history") {
                extractDocumentPath { path =>

                    parameters (
                        'from.as[Date] ? getMinimumDate,
                        'to.as[Date] ? getCurrentDate,
                        'direction ? "d") {
                        (from, to, direction) =>

                            complete {
                                driver
                                    .history(path, from, to, Direction(direction))
                                    .map(response)
                            }
                    }
                }
            } ~
            specialCommandPath("restore") {
                extractDocumentPath { path =>

                    parameters ('date.as[Date] ? getCurrentDate) { date =>

                            complete {
                                driver
                                    .restore(path, date)
                                    .map(response)
                            }
                    }
                }
            }
        }

}
