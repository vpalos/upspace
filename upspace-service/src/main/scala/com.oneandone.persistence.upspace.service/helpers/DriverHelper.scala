package com.oneandone.persistence.upspace.service.helpers

import com.oneandone.persistence.upspace.service.drivers._
import java.util.Date
import org.json4s._
import scala.collection.concurrent._
import scala.io._
import scala.util.matching._
import spray.httpx.marshalling._

/**
 * Operational status responses.
 */
abstract class OpResult
case class OpSuccess    (why: Any = null)     extends OpResult
case class OpComplain   (why: String = "")    extends OpResult
case class OpConflict   (why: String = "")    extends OpResult
case class OpTimeout    (why: String = "")    extends OpResult
case class OpOverload   (why: String = "")    extends OpResult
case class OpMissing    (why: String = "")    extends OpResult
case class OpDeferred   (why: String = "")    extends OpResult
case class OpUnsupported(why: String = "")    extends OpResult
case class OpFailure    (why: String = "")    extends OpResult
case class OpEmpty      ()                    extends OpResult

abstract class OpStreamResult extends OpResult
case class OpStreamArray  (set: Stream[List[JValue]]) extends OpStreamResult
case class OpStreamObject (set: Stream[List[JField]]) extends OpStreamResult

/**
  * Utility methods for drivers.
 */
trait DriverHelper { self: Driver =>
    private val resourceCache = TrieMap.empty[String, String]
    private val driverNamePattern = new Regex("""(?i)\.([a-z0-9]+)Driver$""", "name")

    def loadResource(resourceName: String): String = {
        resourceCache.get(resourceName) match {
            case Some(data) => data
            case None =>
                val capture = driverNamePattern.findFirstMatchIn(getClass.getName).get
                val driverName = capture.group("name").toLowerCase
                val data = Source.fromURL(getClass.getResource(s"/drivers/$driverName/$resourceName")).mkString
                resourceCache += resourceName -> data
                data
        }
    }
}

/**
 * Expiration timestamp support.
 */
abstract class Expire {
    def asDate(sinceMilliSeconds: Long = -1): Date = {
        val absolute = this match {
            case relative: ExpireRelative =>
                val beginning = sinceMilliSeconds match {
                    case -1 => System.currentTimeMillis
                    case _ => sinceMilliSeconds
                }
                beginning + (relative.delta * 1000)
            case absolute: ExpireAbsolute => absolute.when * 1000
            case infinite: ExpireInfinite => 0
        }
        if (absolute != 0) {
            new Date(absolute)
        } else {
            null
        }
    }
    def asSeconds(sinceMilliSeconds: Long = -1): Int = {
        asDate(sinceMilliSeconds) match {
            case null => 0
            case when => (when.getTime / 1000).toInt
        }
    }
}
object Expire {
    def apply(delta: Int): Option[Expire] = {
        if (delta < 0) {
            None
        } else if (delta == 0) {
            Some(ExpireInfinite())
        } else {
            Some(ExpireRelative(delta))
        }
    }
    def apply(when: String): Option[Expire] = {
        try {
            val parsedValue = augmentString(when).toInt
            // TODO: Parse absolute date strings into absolute expires.
            apply(parsedValue)
        } catch {
            case _: Exception => None
        }
    }
    def apply(when: Date): Option[Expire] = {
        apply((when.getTime / 1000).toInt)
    }
    def apply(target: Option[_]): Option[Expire] = target match {
        case Some(value: Int) => apply(value)
        case Some(value: String) => apply(value)
        case Some(value: Date) => apply(value)
        case _ => None
    }
}
case class ExpireRelative(delta: Long) extends Expire
case class ExpireAbsolute(when: Long) extends Expire
case class ExpireInfinite() extends Expire

