package com.oneandone.persistence.upspace.service.helpers

import com.oneandone.persistence.upspace.service.routes._
import java.util.Date
import scala.language.existentials
import scala.language.implicitConversions
import scala.util.parsing.combinator._

/**
 * Sorting direction support.
 */
abstract class Direction
case class Ascending()  extends Direction
case class Descending() extends Direction

object Direction {
    implicit def _stringToSortDirection(value: String): Direction = Direction(value)

    def apply(value: String): Direction = {
        try {
            value.charAt(0).toLower match {
                case 'd' => Descending()
                case 'a' => Ascending()
            }
        } catch {
            case _: Exception => throw new Complaint(s"Failed to interpret '$value' as Direction!")
        }
    }
}

/**
 * Search criteria definition.
 */
case class FieldExpression(transform: String, path: List[String], modifier: String, arguments: List[Any])
case class Criteria(operator: String, operand: String)
case class Rule(formula: FieldExpression, criteria: Criteria)

class SearchParser extends JavaTokenParsers {

    // case insensitive terminals
    private class CIString(str: String) {
        def ci: Parser[String] = ("""(?i)\Q""" + str + """\E""").r
    }
    implicit private def stringToCIString(s: String) = new CIString(s)

    // common terminals
    val boolean   = "false".ci | "true".ci
    val number    = floatingPointNumber | decimalNumber | wholeNumber
    val string    = stringLiteral ^^ { _.replaceAll("(^\"|\\\\|\"$)", "") }
    val date      = """\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z""".r

    // formula definition
    val element   = wholeNumber | string | ident
    val path      = element ~ rep("." ~> element | "[" ~> element <~ "]") ^^ { case a ~ b => a :: b }

    // type cast definition
    val transform =
        ("M".ci ~ opt("eta".ci))    ^^ { _ => "meta" } |
        ("S".ci ~ opt("tring".ci))  ^^ { _ => "string" } |
        ("N".ci ~ opt("umber".ci))  ^^ { _ => "number" } |
        ("B".ci ~ opt("oolean".ci)) ^^ { _ => "boolean" } |
        ("D".ci ~ opt("ate".ci))    ^^ { _ => "date" }

    // modifier call definition
    val argument  = string | number
    val arguments = opt("(" ~> repsep(argument, ",") <~ ")") ^^ { _.getOrElse(List.empty) }
    val modifier  = "." ~> ident ~ arguments

    // TODO: Remove oldWrap (deprecated).
    val oldWrap   = ":" ~> path
    val wrap      = "(" ~> path <~ ")"

    // full formula definition
    val complex   = transform ~ (oldWrap | wrap) ~ opt(modifier) ^^ {
        case _transform ~ _path ~ Some(_modifier) => FieldExpression(_transform, _path, _modifier._1, _modifier._2)
        case _transform ~ _path ~ None            => FieldExpression(_transform, _path, "plain", List.empty)
    }
    val simple    = path ^^ { FieldExpression("string", _, "plain", List.empty) }
    val formula   = complex | simple

    // criteria definition
    val operator  = "=" ~> opt {
        ("=" ~ opt("=")) ^^ { _ => "==" } |
        ("!" ~ opt("=")) ^^ { _ => "!=" } |
        "!~" | "~" |
        ">=" |  ">" | "<=" | "<"
    } ^^ { _.getOrElse("==") }
    val operand   = (string | date | number | boolean) <~ "\\Z".r | "[^\"].*".r | failure("invalid operand...")
    val criteria  = operator ~ operand ^^ {
        case _operator ~ _operand => Criteria(_operator, _operand)
    }

    // equation definition
    val equation  = formula ~ criteria ^^ {
        case _formula ~ _criteria => Rule(_formula, _criteria)
    }
}

/**
 * Adds search operational facilities into the search handler.
 */
trait SearchHelper extends Parsers { self: SearchRoute =>

    private val parser = new SearchParser
    private def parse[C](expression: parser.Parser[C], input: String): C = {
        parser.parseAll(expression, input) match {
            case parser.Success(output, _) => output.asInstanceOf[C]
            case failure: parser.Failure   => throw new Complaint(s"Parsing failure: $failure")
            case unknown                   => throw new Exception(s"${unknown.toString}")
        }
    }
    
    protected def parseFormula(formula: String)   = parse(parser.formula, formula)
    protected def parseCriteria(criteria: String) = parse(parser.criteria, criteria)
    protected def parseRule(equation: String)     = parse(parser.equation, equation)

    protected def parseCriteria(query: Map[String, List[String]]) = {
        val sets = query map {
            case (formula, list) => list map { criteria =>
                parseRule(s"$formula=$criteria")
            }
        }
        sets.flatten.toList
    }
}