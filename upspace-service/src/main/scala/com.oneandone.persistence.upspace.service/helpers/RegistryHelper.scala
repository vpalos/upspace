package com.oneandone.persistence.upspace.service.helpers

import akka.actor._
import com.oneandone.persistence.upspace.service.drivers.Driver
import scala.collection.concurrent.TrieMap

/**
 * Dynamic resource registry.
 */
object RegistryHelper {
    val actors = TrieMap.empty[String, ActorRef]
    val drivers = TrieMap.empty[String, Driver]

    def shutdown() {
        System.out.println("Cleaning-up...")
        drivers.values.foreach(_.shutdown())
        actors.values.foreach(_ ! Kill)
    }
}
