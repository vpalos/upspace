package com.oneandone.persistence.upspace.service.helpers

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}
import org.json4s.DefaultFormats
import scala.language.implicitConversions

/**
 * Implicit conversions.
 */
trait CastHelper {

    protected val dateFormatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    implicit protected val castFormats = new DefaultFormats {
        override def dateFormatter = new SimpleDateFormat(dateFormatString)
    }

    implicit protected def _stringToInt    (value: String): Int     = augmentString(value).toInt
    implicit protected def _stringToInteger(value: String): Integer = _stringToInt(value)
    implicit protected def _stringToDouble (value: String): Double  = augmentString(value).toDouble
    implicit protected def _stringToBoolean(value: String): Boolean = augmentString(value).toBoolean
    implicit protected def _stringToDate   (value: String): Date    = castFormats.dateFormat.parse(value).get
    implicit protected def _dateToBigInt   (value: Date):   BigInt  = value.getTime

    protected def getMinimumDate: Date = new Date(0)
    protected def getCurrentDate: Date = Calendar.getInstance.getTime
}
