package com.oneandone.persistence.upspace.service.helpers

import com.oneandone.persistence.upspace.service.actors._
import java.net.URLDecoder
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import spray.http._
import spray.httpx.marshalling._
import spray.routing._

class Response(data: String) extends RuntimeException(data)
class Deferred()             extends RuntimeException
class Complaint(why: String) extends RuntimeException(why)

trait RoutingHelper { self: SpaceActor =>

    protected def decodeUtf8[T >: String](url: T) = URLDecoder.decode(url.toString, "UTF-8")

    protected def sanitizePath[T >: String](url: T) = "/" + decodeUtf8(url).replaceAll("/{2,}", "/").stripSuffix("/")

    protected def specialCommandPath(command: String)(handle: => Route) = {
        pathPrefix(s"~$command") {
            handle
        }
    }

    protected def extractDocumentPath(handle: String => Route) = {
        val validDocumentPath = "^[^~].*$".r

        def rh = RejectionHandler {
            case _ => throw new Complaint(s"Invalid document path!")
        }
        handleRejections(rh) {
            pathPrefixTest(validDocumentPath) { _ =>
                path(RestPath) {
                    path =>
                        handle(sanitizePath(path))
                }
            }
        }
    }

    protected def extractJsonBody(handle: JObject => Route) = {
        def rh = RejectionHandler {
            case _ => throw new Complaint("Request body is not valid JSON!")
        }
        handleRejections(rh) {
            entity(as[JObject])(handle)
        }
    }

    protected def response(opResult: OpResult): HttpResponse = {
        opResult match {

            case OpSuccess(value: String)   => HttpResponse(200, compact(parse(value).noNulls))
            case OpSuccess(value: JValue)   => HttpResponse(200, compact(value))
            case OpSuccess(null)            => HttpResponse(200)

            case OpComplain(why: String)    => HttpResponse(400, why)
            case OpTimeout(why: String)     => HttpResponse(503, why)
            case OpOverload(why: String)    => HttpResponse(503, why)
            case OpMissing(why: String)     => HttpResponse(404, why)
            case OpConflict(why: String)    => HttpResponse(409, why)
            case OpDeferred(why: String)    => HttpResponse(202, why)
            case OpUnsupported(why: String) => HttpResponse(501, why)

            case OpEmpty()                  => HttpResponse(204)

            case OpFailure(why: String) =>
                log.error(s"Driver malfunction: $why")
                HttpResponse(500, entity = why)

            case _ =>
                log.error(s"Invalid driver response: ${Some(opResult)}")
                HttpResponse(500)
        }
    }

    protected def streamResponse(opResult: OpStreamResult): ToResponseMarshallable = {
        opResult match {

            case OpStreamArray(stream)  => stream
            case OpStreamObject(stream) => stream

            case _ =>
                log.error(s"Invalid driver stream-response: ${Some(opResult)}")
                throw new Exception("Invalid driver stream-response!")
        }
    }

    /**
     * Array serializer (stream of JValues).
     */
    implicit def jsonArrayMarshaller: ToResponseMarshaller[Stream[List[JValue]]] = {

        // important: overrides the Json marshallers in-scope
        import BasicMarshallers._

        Marshaller.delegate[Stream[List[JValue]], Stream[String]](ContentTypes.`application/json`) {
            raw: Stream[List[JValue]] =>
                def joined = raw.zipWithIndex.map { case (value, index) =>
                    val sValue = value.map(compact).mkString(",")
                    if (index > 0)
                        s",$sValue"
                    else
                        sValue
                }
                "[" #:: joined #::: "]" #:: Stream.empty[String]
        }
    }

    /**
     * Object serializer (stream of JFields (i.e. String -> JValue)).
     */
    implicit def jsonObjectMarshaller: ToResponseMarshaller[Stream[List[JField]]] = {

        // important: overrides the Json marshallers in-scope
        import BasicMarshallers._

        Marshaller.delegate[Stream[List[JField]], Stream[String]](ContentTypes.`application/json`) {
            raw: Stream[List[JField]] =>
                def joined = raw.zipWithIndex.map { case (list, index) =>
                    val items = list map { case (key, value) =>
                        val sKey = Serialization.write(key)
                        val sValue = compact(value)
                        s"$sKey:$sValue"
                    }
                    val set = items.mkString(",")
                    if (index > 0)
                        s",$set"
                    else
                        set
                }
                "{" #:: joined #::: "}" #:: Stream.empty[String]
        }
    }
}
