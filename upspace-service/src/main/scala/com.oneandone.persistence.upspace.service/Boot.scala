package com.oneandone.persistence.upspace.service

import akka.actor.{ActorSystem, Props}
import com.oneandone.persistence.upspace.service.actors._
import com.oneandone.persistence.upspace.service.helpers.RegistryHelper
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import javax.servlet._
import spray.servlet._

/**
 * Hook for shutdown in servlet containers.
 */
class AsyncInitializer extends Initializer {
    override def contextDestroyed(e: ServletContextEvent): Unit = {
        RegistryHelper.shutdown()
        super.contextDestroyed(e)
    }
}

/**
 * This enforces the asynchronous support in Tomcat.
 */
class AsyncServlet30ConnectorServlet extends Servlet30ConnectorServlet {
    override def service(hsRequest: HttpServletRequest, hsResponse: HttpServletResponse): Unit = {
        hsRequest.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true)
        super.service(hsRequest, hsResponse)
    }
}

/**
 * Spray bootstrapping class.
 */
class Boot(context: ServletContext) extends WebBoot {

    val system = ActorSystem("upspace")
    val serviceActor = system.actorOf(Props(classOf[DispatcherActor], context.getContextPath), "dispatcher")
    RegistryHelper.actors += "dispatcher" -> serviceActor

    system.registerOnTermination {
        system.shutdown()
        system.awaitTermination()
    }

}

// TODO: Revise the CouchBase Views cache (shoudl expire after X seconds).
// TODO: Undeploy/stop servlet bug (crashes JVM).
