package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.oneandone.persistence.upspace.service.helpers._
import scala.language.existentials
import scala.language.implicitConversions
import scala.reflect.ClassTag
import java.util.Date

/**
 * Couchbase criteria checks implementations.
 */
abstract class Check[T, V](control: T) {
    def matches(value: V): Boolean
}
case class EqualCheck[T](control: T)
                        (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) == 0
}
case class NotEqualCheck[T](control: T)
                           (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) != 0
}
case class GreaterThanCheck[T](control: T)
                              (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) > 0
}
case class GreaterThanOrEqualCheck[T](control: T)
                                     (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) >= 0
}
case class LessThanCheck[T](control: T)
                           (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) < 0
}
case class LessThanOrEqualCheck[T](control: T)
                                  (implicit val ord: Ordering[T]) extends Check[T, T](control) {
    def matches(value: T): Boolean = ord.compare(value, control) <= 0
}
abstract class RegexCheck[T](control: T) extends Check[T, T](control) {
    protected val regex =
        try {
            control.toString.r
        } catch {
            case _: Exception => throw new Complaint(s"Invalid regular expression: '$control'!")
        }
}
case class MatchCheck[T](control: T) extends RegexCheck(control) {
    def matches(value: T): Boolean = regex.findFirstIn(value.toString).nonEmpty
}
case class NotMatchCheck[T](control: T) extends RegexCheck(control) {
    def matches(value: T): Boolean = regex.findFirstIn(value.toString).isEmpty
}

object Check {
    def apply[T](operator: String, control: T)
                (implicit ord: Ordering[T]) = {
        operator match {
            case "==" => EqualCheck[T](control)
            case "!=" => NotEqualCheck[T](control)
            case "<"  => LessThanCheck[T](control)
            case "<=" => LessThanOrEqualCheck[T](control)
            case ">"  => GreaterThanCheck[T](control)
            case ">=" => GreaterThanOrEqualCheck[T](control)
            case "~"  => MatchCheck[T](control)
            case "!~" => NotMatchCheck[T](control)
            case op   => throw new Complaint(s"Unknown operator '$op'!")
        }
    }
}

/**
 * Couchbase formulae and modifiers implementations.
 */
case class Result[T, V](code: String)
                       (implicit _cast: String => T, val ord: Ordering[V], recast: T => V,
                        tagT: ClassTag[T], tagV: ClassTag[V]) {
    def cast(input: Any): V = {
        input match {
            case value: V => value: V
            case value: T => recast(value)
            case _ =>
                try {
                    recast(_cast(input.toString))
                } catch {
                    case _: Exception => throw new Complaint(s"Failed to parse '$input' as required type '$tagT'!")
                }
        }
    }
}

abstract class Transform(formula: FieldExpression) extends CastHelper {

    val prototype = formula.arguments.map(_.getClass)
    val method = try {
        this.getClass.getMethod(formula.modifier, prototype: _*)
    } catch {
        case _: Exception =>
            val invocation = s"${formula.modifier}(${prototype.map(_.getSimpleName).mkString(", ")})"
            throw new Complaint(s"Field transformer '${formula.transform}.$invocation' does not exist!")
    }
    val result = method.invoke(this, formula.arguments.map(_.asInstanceOf[Object]): _*).asInstanceOf[Result[Any, Any]]

    protected def produce(expression: String) = {
        val path = formula.path.map(_.replace("\\", "\\\\").replace("'", "\\'")).mkString("['", "', '", "']")
        s"produce($path, function(field) { return $expression })"
    }
}

case class IdTransform() extends Transform(FieldExpression("id", null, "plain", List.empty)) {
    def plain       = Result[String,  String ] { "meta.id" }
}

case class MetaTransform(formula: FieldExpression) extends Transform(formula) {
    def plain       = Result[String,  String ] { s"Meta('${formula.path.mkString(".")}')" }
}

case class StringTransform(formula: FieldExpression) extends Transform(formula) {
    def plain       = Result[String,  String ] { produce("String(field)") }
    def length      = Result[Integer, Integer] { produce("String(field).length") }
    def lowercase   = Result[String,  String ] { produce("String(field).toLowerCase()") }
    def uppercase   = Result[String,  String ] { produce("String(field).toUpperCase()") }

    // FIXME: Bug!
    def substring(from: Integer, to: Integer)
                    = Result[String,  String ] { produce(s"String(field).substring($from, $to)") }
}

case class BooleanTransform(formula: FieldExpression) extends Transform(formula) {
    def plain       = Result[Boolean, Boolean] { produce("Boolean(field)") }
}

case class NumberTransform(formula: FieldExpression) extends Transform(formula) {
    def plain       = Result[Double,  Double ] { produce("Number(field)") }
}

case class DateTransform(formula: FieldExpression) extends Transform(formula) {
    def plain       = Result[Date,    BigInt ] { produce("new Date(field).getTime()") }
    def year        = Result[Integer, Integer] { produce("new Date(field).getUTCFullYear()") }
    def month       = Result[Integer, Integer] { produce("new Date(field).getUTCMonth() + 1") }
    def day         = Result[Integer, Integer] { produce("new Date(field).getUTCDate()") }
    def hour        = Result[Integer, Integer] { produce("new Date(field).getUTCHours()") }
    def minute      = Result[Integer, Integer] { produce("new Date(field).getUTCMinutes()") }
    def second      = Result[Integer, Integer] { produce("new Date(field).getUTCSeconds()") }
    def millisecond = Result[Integer, Integer] { produce("new Date(field).getUTCMilliseconds()") }
}

/**
 * Enforce the generic query components for the CouchBase driver.
 */
trait Formulae {

    protected def transform(formula: FieldExpression): Transform = {
        formula.transform match {
            case "meta"    => MetaTransform(formula)
            case "string"  => StringTransform(formula)
            case "boolean" => BooleanTransform(formula)
            case "number"  => NumberTransform(formula)
            case "date"    => DateTransform(formula)
            case unknown   => throw new Complaint(s"Unknown field transformer '$unknown'!")
        }
    }

}
