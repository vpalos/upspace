package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import org.json4s.jackson.JsonMethods._
import org.json4s._
import scala.collection.concurrent.TrieMap
import scala.collection.JavaConversions._
import scala.annotation.tailrec

trait Linker {
    self: CouchbaseDriver =>
    private def loadDocuments(names: List[String], cache: TrieMap[String, JValue] = TrieMap.empty, default: JValue = JObject()): Map[String, JValue] = {
        val missingDocuments = names.toSet -- cache.keySet
        if (missingDocuments.size > 0) {
            val newDocuments = client.getBulk(missingDocuments) mapValues (value => {
                parse(value.toString)
            })
            cache ++= newDocuments
        }
        (names map (elem => {
            (elem, cache.getOrElse(elem, default))
        })).toMap
    }

    @tailrec
    private def bulkLoadLinkedDocuments(documents: Map[String, JValue], depth: Int,
                                        cache: TrieMap[String, JValue] = TrieMap.empty): Map[String, JValue] = {

        def extractDependencies(json: JValue): List[String] = json match {
            case JObject(List(("__metadata", JObject(List(("id", JString(path))))))) => List(path)
            case JObject(list) => list.foldLeft(List[String]())((a, i) => {
                a ++ extractDependencies(i._2)
            })
            case JArray(list) => list.foldLeft(List[String]())((a, i) => {
                a ++ extractDependencies(i)
            })
            case _ => List()
        }

        def renderLinkedDocuments(documents: Map[String, JValue]) = {

            def renderLinkedDocument(document: JValue): JValue = {
                document match {
                    case (metaBlock@JObject(List(("__metadata", JObject(List(("id", JString(name)))))))) =>
                        cache.getOrElse(name, metaBlock)

                    case JObject(list) => JObject(list map ((pair) => {
                        (pair._1, renderLinkedDocument(pair._2))
                    }))

                    case JArray(list) => JArray(list map ((elem) => {
                        renderLinkedDocument(elem)
                    }))
                    case x => x
                }
            }

            documents.mapValues(renderLinkedDocument)
        }

        if (depth <= 0) {
            documents
        } else {
            loadDocuments(documents.values.toList.map(extractDependencies).flatten, cache)
            bulkLoadLinkedDocuments(renderLinkedDocuments(documents), depth - 1, cache)
        }
    }

    def renderDocumentDepth(document: String, depth: Int): OpResult = {
        OpSuccess {
            if (depth <= 1) {
                document
            } else {
                render {
                    bulkLoadLinkedDocuments(Map("" -> parse(document)), depth - 1).get("").get
                }
            }
        }
    }

    def renderBulkDocumentDepth(paths: List[String], depth: Int): List[JField] = {
        val cache: TrieMap[String, JValue] = TrieMap.empty
        val documents = loadDocuments(paths, cache)
        val result = bulkLoadLinkedDocuments(documents, depth - 1, cache).toList.map {
            case (id, body) => JField(id, body)
        }
        result
    }
}