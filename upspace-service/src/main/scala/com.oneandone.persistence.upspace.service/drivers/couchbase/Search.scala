package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import org.json4s._
import org.json4s.jackson.Serialization
import org.json4s.jackson.JsonMethods._
import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.language.implicitConversions
import scala.util._
import scala.Some

/**
 * Implements search/filtering functionality for the Couchbase driver.
 */
trait Search extends Formulae { self: CouchbaseDriver with Driver =>

    implicit private val executionContext = actorSystem.dispatcher

    def searchView(prefix: String, fields: List[String]) = {
        val mapCode = {
            loadResource("views/search-map.js")
                .replaceFirst("%%prefix%%", Serialization.write(prefix))
                .replaceFirst("%%fields%%", fields.mkString("[", ",", "]"))
        }
        val salt = digestView(fields.toString())(fields.toString())
        obtainView("search_" + salt, mapCode)
    }

    def search(prefix: String,
               rules: List[Rule],
               sorts: List[FieldExpression],
               direction: Direction = Ascending(),
               offset: Int, limit: Int, count: Boolean, depth: Int = 0,
               forceIndex: Boolean = false)
              (implicit user: String): Future[OpStreamResult] = {

        val promise = Promise[OpStreamResult]()

        val rulesTransforms = rules.groupBy { rule =>
            transform(rule.formula)
        } mapValues {
            _.unzip(rule => (null, rule.criteria))._2
        }
        val sortTransforms = sorts.map(transform)

        val (equalFormulas, equalValues) = {
            rulesTransforms map {
                case (_transform, _criteria) =>
                    _criteria.find(_.operator == "==") match {
                        case Some(criteria) =>
                            (_transform, _transform.result.cast(criteria.operand))
                        case None =>
                            (null, null)
                    }
            }
        }.filter(_._1 != null).toList.sortBy(_._1.result.code).unzip

        val sortFormulas = (sortTransforms :+ IdTransform()).distinct.diff(equalValues)
        
        val filterTransforms = {
            (rulesTransforms -- equalFormulas) map {
                case (_transform, _list) =>
                    _transform -> {
                        _list map { criteria =>
                            Check(criteria.operator, _transform.result.cast(criteria.operand))(_transform.result.ord)
                        }
                    }
            }
        }.toList.sortBy(_._1.result.code)
        val (filterFormulas, _) = filterTransforms.unzip

        val (viewPrefix, startKey, endKey) = sortFormulas.head match {
            case IdTransform() =>
                (null, prefix, s"$prefix\uefff")
            case constraint =>
                val limits = rulesTransforms.getOrElse(constraint, List.empty).map {
                    case Criteria(">", value) => (constraint.result.cast(value), null)
                    case Criteria("<", value) => (null, constraint.result.cast(value))
                    case _ => (null, null)
                }
                val start = limits.find(_._1 != null).getOrElse((null, null))._1
                val end   = limits.find(_._2 != null).getOrElse((null, "\uefff"))._2
                (prefix, start, end)
        }

        val (descending, fromKey, toKey) = direction match {
            case Ascending()  => (false, startKey, endKey)
            case Descending() => (true, endKey, startKey)
        }

        val query = new Query()
        query.setInclusiveEnd(true)
        query.setStale {
            forceIndex match {
                case true => Stale.FALSE
                case false => Stale.UPDATE_AFTER
            }
        }
        query.setDescending(descending)
        query.setIncludeDocs(false)
        query.setReduce(false)

        val rangeStart = Serialization.write {
            if (fromKey != null) {
                equalValues :+ fromKey
            } else {
                equalValues
            }
        }
        val rangeEnd = Serialization.write {
            if (toKey != null) {
                equalValues :+ toKey
            } else {
                equalValues
            }
        }
        query.setRangeStart(rangeStart)
        query.setRangeEnd(rangeEnd)

        val unneeded = equalFormulas.size + sortFormulas.size
        val viewFields = equalFormulas.map(t => (t.result.code, false)) ++
                         sortFormulas.map(t => (t.result.code, true)) ++
                         filterFormulas.map(t => (t.result.code, false))

        val lazyOffset = {
            val result = Math.max(offset, 0)
            if (filterTransforms.size > 0) {
                result
            } else {
                query.setSkip(result)
                0
            }
        }

        searchView(viewPrefix, viewFields.map(f => s"{ value: ${f._1}, sort: ${f._2} }")).onComplete {

            case Failure(failure) =>
                promise.failure(new Exception(failure.getMessage))

            case Success(null) =>
                promise.failure(new Deferred())

            case Success(view) =>
                execute(promise, view, query, lazyOffset, limit, count, depth, unneeded) { values =>
                    values.zip(filterTransforms) forall {
                        case (_value, (transform, checks)) =>
                            val value = transform.result.cast(_value)
                            checks.forall(_.matches(value))
                    }
                }
        }

        promise.future
    }

    def execute(promise: Promise[OpStreamResult],
                view: View, query: Query,
                offset: Int, limit: Int, count: Boolean, depth: Int, unneeded: Int)
               (test: List[Any] => Boolean) = {

        val maximum = 10000
        val segment = if (limit == -1) {
            maximum
        } else {
            Math.min(limit + offset, maximum)
        }

        // fixme: fix the stream approach to support the fully asynchronous paginated query
        /*query.setLimit(segment + 1)
        def asyncAdvance(required: Int, startId: String = null, startKey: String = null) {
            if (startId != null) {
                query.setStartkeyDocID(startId)
                query.setRangeStart(startKey)
            }

            client.asyncQuery(view, query).onComplete {

                case Success(response) =>
                    if (response.size() < segment + 1 || (!count && required <= 0 && limit > 0)) {
                        List.empty
                    } else {
                        val nextRow = response.removeLastElement()

                        val unfiltered = response.map {
                            row => (row.getId, parse(row.getKey).values.asInstanceOf[List[Any]].drop(unneeded))
                        }
                        val filtered = unfiltered.filter {
                            case (_, values) => test(values)
                        }
                        val result = filtered.map(_._1).toList

                        advance(required - result.size, nextRow.getId, nextRow.getKey)
                    }

                case Failure(failure) =>
                    log.error(s"Query error (startDocId='$startId', startKey='$startKey'): ${failure.getMessage}!")
                    /*promise.success(OpFailure(failure.getMessage))*/
            }
        }*/

        val engine = client.paginatedQuery(view, query, segment)

        val timeoutTimer = {
            val period = indexTimeout
            actorSystem.scheduler.scheduleOnce(period) {
                promise.failure(new Deferred())
            }
        }

        def advance: List[String] = {
            if (!engine.hasNext) {
                null.asInstanceOf[List[String]]
            } else {
                val response = engine.next()
                val unfiltered = response.map {
                    row => (row.getId, parse(row.getKey).values.asInstanceOf[List[Any]].drop(unneeded))
                }
                val filtered = unfiltered.filter {
                    case (_, values) => test(values)
                }
                filtered.map(_._1).toList
            }
        }

        @tailrec
        def produce(offset: Int, limit: Int): (List[String], Int, Int) = {
            val result    = {
                val data  = advance

                // cancel deferred (202) timeout
                timeoutTimer.cancel()

                if (data == null || (limit == 0 && data.size == 0)) {
                    return (List.empty, 0, 0)
                }
                data
            }

            val offTarget = result.drop(offset)
            val newOffset = Math.max(offset - result.size, 0)

            if (offTarget.size > 0) {
                val (targeted, remaining) = {
                    if (limit < 0)
                        (segment, limit)
                    else
                        (limit, Math.max(limit - offTarget.size, 0))
                }
                (offTarget.take(targeted), newOffset, remaining)
            } else {
                produce(newOffset, limit)
            }
        }

        def stream(offset: Int, limit: Int): Stream[List[_]] = {
            try {
                val (target, newOffset, newLimit) = produce(offset, limit)
                if (target.isEmpty) {
                    Stream.empty
                } else {
                    if (depth > 0) {
                        renderBulkDocumentDepth(target, depth) #:: stream(newOffset, newLimit)
                    } else {
                        target.map(JString(_)) #:: stream(newOffset, newLimit)
                    }
                }
            } catch {
                case e: Exception =>
                    log.error(s"Document rendering failed: '${e.getMessage}'!")
                    Stream.empty
            }
        }

        if (count) {
            promise.failure {
                new Response(stream(offset, limit).asInstanceOf[Stream[List[JValue]]].toList.flatten.size.toString)
            }
        } else {
            if (depth > 0) {
                promise.success(OpStreamObject(stream(offset, limit).asInstanceOf[Stream[List[JField]]]))
            } else {
                promise.success(OpStreamArray(stream(offset, limit).asInstanceOf[Stream[List[JValue]]]))
            }
        }
    }
}
