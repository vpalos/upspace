package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.persistence.upspace.service.drivers._
import java.security.MessageDigest
import org.apache.commons.codec.binary.Hex
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.concurrent.TrieMap
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.Predef._
import scala.util._

/**
 * Manage the views in a Couchbase space.
 */
trait Views { self: CouchbaseDriver =>

    implicit private val executionContext = actorSystem.dispatcher

    private val viewCache = TrieMap.empty[String, View]

    /**
     * Clean-up stale design-documents/views.
     * This relies on contacting the REST API of the first available node in the chain
     * to get all existing design-documents and then purges all views which have not been
     * used for more than a fixed, pre-established amount of time.
     */
    private def purgeViews() {
        import dispatch._

        val node = nodes.head
        val address = url(s"$node/default/buckets/$bucket/ddocs")
        val future = Http(address OK as.String)

        for (raw <- future) {
            val ddocs: List[String] = (parse(raw) \ "rows" \\ "doc" \\ "meta" \\ "id" \\ classOf[JString]).toList

            /**
             * Preserve permanent indexes explicitly.
             */
            val ids = ddocs
                .map(_.stripPrefix("_design/"))
                .filterNot(_.startsWith("dev_permanent_"))
                .filterNot(_.startsWith("permanent_"))

            if (ids.isEmpty) {
                log.info(s"No design documents found, nothing to check for purging at this time.")
            } else {
                log.info(s"Checking available design documents for purging.")

                val allDDocs = ((ids map ("/~index/" + _)) zip ids).toMap
                client.asyncGetBulk(allDDocs.keySet).onComplete { result =>
                    val oldDDocs = result match {
                        case Success(found)   => (allDDocs -- found.keySet).values.toList
                        case Failure(failure) => allDDocs.values.toList
                    }
                    if (oldDDocs.isEmpty) {
                        log.info(s"No obsolete design documents found, nothing to purge at this time.")
                    } else {
                        log.warning(s"Purging the following obsolete design documents: ${oldDDocs.mkString(", ")}.")
                        for (oldDDoc <- oldDDocs) client.asyncDeleteDesignDoc(oldDDoc)
                        invalidateViews()
                    }
                }
            }
        }
    }

    protected val indexLifetime = connectionConfig.getMilliseconds("index.lifetime").toLong.milliseconds
    protected val indexTimeout  = connectionConfig.getMilliseconds("index.timeout").toLong.milliseconds

    private val purgeTimer = {
        val period = indexLifetime.max(1.day)
        log.info(s"Stale view garbage collection will run every: $period.")
        actorSystem.scheduler.schedule(0 milliseconds, period, new Runnable {
            def run() = {
                purgeViews()
            }
        })
    }

    private val digestCache = TrieMap.empty[String, String]
    protected def digestView(key: String)(bodies: String*): String = {
        digestCache.get(key) match {
            case Some(value) =>
                value
            case None =>
                val messageDigest = MessageDigest.getInstance("SHA")
                val rawDigest = messageDigest.digest(bodies.mkString.getBytes("UTF-8"))
                val encDigest = Hex.encodeHexString(rawDigest).mkString
                digestCache += key -> encDigest
                encDigest
        }
    }

    protected def obtainView(viewName: String, map: String, reduce: String = null, permanent: Boolean = false):
        Future[View] = {

        val designDocumentName = permanent match {
            case true => s"permanent_$viewName"
            case false => s"volatile_$viewName"
        }
        val viewDigest = digestView(viewName)(map, reduce)
        val stampName = s"/~index/$designDocumentName"
        val expireDate = (System.currentTimeMillis / 1000).toInt + indexLifetime.toSeconds
        val promise = Promise[View]()

        def recreateDesignDocument() {
            client.asyncDeleteDesignDoc(designDocumentName).onComplete { result =>
                val designDocument = new DesignDocument(designDocumentName)
                val viewDesign = reduce match {
                    case null => new ViewDesign(viewName, map)
                    case _ => new ViewDesign(viewName, map, reduce)
                }
                designDocument.setView(viewDesign)

                val setFuture = if (permanent) {
                    client.set(stampName, viewDigest)
                } else {
                    client.set(stampName, expireDate.toInt, viewDigest)
                }

                setFuture.onComplete {
                    case Success(_) =>
                        if (client.createDesignDoc(designDocument)) {
                            log.warning(s"Created view '$viewName' in database!")
                            promise.success(null)

                            // trigger update of newly created view
                            Future {
                                client.asyncGetView(designDocumentName, viewName).onComplete {

                                    case Success(view) =>
                                        val query = new Query()
                                        query.setLimit(1)
                                        query.setIncludeDocs(false)
                                        query.setStale(Stale.UPDATE_AFTER)
                                        client.asyncQuery(view, query)

                                    case Failure(failure) =>
                                        log.error(s"Failed to trigger update for view '$viewName'!")
                                }
                            }
                        } else {
                            promise.failure(new Error("Failed to create view!!!!!"))
                        }

                    case Failure(failure) =>
                        promise.failure(failure)
                }
            }
        }

        def retrieveView(retry: Boolean = false) {
            client.asyncGetView(designDocumentName, viewName).onComplete {

                case Success(view) =>
                    log.info(s"Retrieving (and caching) view '$viewName' from database.")
                    viewCache += designDocumentName -> view
                    promise.success(view)

                case Failure(failure) =>
                    if (retry) {
                        recreateDesignDocument()
                    } else {
                        promise.failure(failure)
                    }
            }
        }

        viewCache.get(designDocumentName) match {

            case Some(view) =>
                log.info(s"Retrieving view '$viewName' from cache.")
                if (!permanent) {
                    client.touch(stampName, expireDate.toInt)
                }
                promise.success(view)

            case None =>
                val casFuture = if (permanent) {
                    client.asyncGets(stampName)
                } else {
                    client.asyncGetAndTouch(stampName, expireDate.toInt)
                }
                casFuture.onComplete {

                    case Success(value) =>
                        if (value.getValue.toString != viewDigest) {
                            recreateDesignDocument()
                        } else {
                            retrieveView(retry = true)
                        }

                    case Failure(failure) =>
                        recreateDesignDocument()
                }
        }

        promise.future
    }

    protected def invalidateViews() {
        viewCache.clear()
    }

    protected def shutdownViews() {
        purgeTimer.cancel()
        digestCache.clear()
        invalidateViews()
    }

}
