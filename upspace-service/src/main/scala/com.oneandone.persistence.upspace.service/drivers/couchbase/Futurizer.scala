package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import net.spy.memcached.internal._
import scala.concurrent._
import com.couchbase.client.internal.{HttpFuture, HttpCompletionListener}

trait Futurizer { self: CouchbaseDriver =>
    import scala.language.implicitConversions
    import scala.collection.JavaConverters._

    implicit protected def fromCBFutureToScalaFuture[T](before: GetFuture[T]): Future[T] = {
        val promise = Promise[T]()
        before.addListener(new GetCompletionListener {
            def onComplete(after: GetFuture[_]) {
                val status = after.getStatus
                if (status.isSuccess) {
                    promise.success(before.get)
                } else {
                    promise.failure(new Exception(status.getMessage))
                }
            }
        })
        promise.future
    }

    implicit protected def fromCBFutureToScalaFuture[T](before: OperationFuture[T]): Future[T] = {
        val promise = Promise[T]()
        before.addListener(new OperationCompletionListener {
            def onComplete(after: OperationFuture[_]) {
                val status = after.getStatus
                if (status.isSuccess) {
                    promise.success(before.get)
                } else {
                    promise.failure(new Exception(status.getMessage))
                }
            }
        })
        promise.future
    }

    implicit protected def fromCBFutureToScalaFuture[T](before: HttpFuture[T]): Future[T] = {
        val promise = Promise[T]()
        before.addListener(new HttpCompletionListener {
            def onComplete(after: HttpFuture[_]) {
                val status = after.getStatus
                if (status.isSuccess) {
                    promise.success(before.get)
                } else {
                    promise.failure(new Exception(status.getMessage))
                }
            }
        })
        promise.future
    }

    implicit protected def fromCBFutureToScalaFuture[T](before: BulkFuture[java.util.Map[String, T]]): Future[Map[String, T]] = {
        val promise = Promise[Map[String, T]]()
        before.addListener(new BulkGetCompletionListener {
            def onComplete(after: BulkGetFuture[_]) {
                val status = after.getStatus
                if (status.isSuccess) {
                    promise.success(before.get.asScala.toMap)
                } else {
                    promise.failure(new Exception(status.getMessage))
                }
            }
        })
        promise.future
    }

    private val success = (data: AnyRef) => OpSuccess()

    protected def futurize[T](before: GetFuture[T])(op: T => OpResult = success): Future[OpResult] = {
        val promise = Promise[OpResult]()
        before.addListener(new GetCompletionListener{
            def onComplete(after: GetFuture[_]) {
                promise.success(op(before.get))
            }
        })
        promise.future
    }

    protected def futurize[T](before: OperationFuture[T])(op: T => OpResult = success): Future[OpResult] = {
        val promise = Promise[OpResult]()
        before.addListener(new OperationCompletionListener{
            def onComplete(after: OperationFuture[_]) {
                promise.success(op(before.get))
            }
        })
        promise.future
    }

    protected def futurize[T](before: HttpFuture[T])(op: T => OpResult = success): Future[OpResult] = {
        val promise = Promise[OpResult]()
        before.addListener(new HttpCompletionListener{
            def onComplete(after: HttpFuture[_]) {
                promise.success(op(before.get))
            }
        })
        promise.future
    }

    protected def futurize[T](before: BulkGetFuture[T])(op: Map[String, T] => OpResult = success): Future[OpResult] = {
        val promise = Promise[OpResult]()
        before.addListener(new BulkGetCompletionListener{
            def onComplete(after: BulkGetFuture[_]) {
                promise.success(op(before.get.asScala.toMap))
            }
        })
        promise.future
    }

}
