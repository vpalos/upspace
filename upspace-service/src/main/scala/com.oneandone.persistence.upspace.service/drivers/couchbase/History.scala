package com.oneandone.persistence.upspace.service.drivers.couchbase

import com.couchbase.client.protocol.views._
import com.oneandone.persistence.upspace.service.drivers._
import com.oneandone.persistence.upspace.service.helpers._
import java.util.concurrent.TimeUnit
import java.util.Date
import net.spy.memcached._
import org.json4s._
import org.json4s.jackson._
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.language.implicitConversions
import scala.util._

/**
 * Implements history functionality for the Couchbase driver.
 */
trait History
    extends CastHelper { self: CouchbaseDriver with Driver =>

    implicit private val executionContext = actorSystem.dispatcher

    protected val historyEnabled = historyConfig.getBoolean("enabled")
    protected val historyLifetime = historyConfig.getMilliseconds("lifetime")

    protected def remember(userId: String, path: String, action: String, timestamp: Long, values: (String, Any)*) {
        if (!historyEnabled) {
            return
        }

        Future {
            val givenValues = values.toMap filter(_._2 != null)
            val requiredValues = Map("timestamp" -> new Date(timestamp),
                "space" -> spaceName,
                "action" -> action)

            val json = Serialization.write(givenValues ++ requiredValues ++ {
                if (userId.nonEmpty) Map("user" -> userId) else Map.empty
            })

            val expire = if (historyLifetime.longValue() <= 0L) {
                0
            } else {
                ((System.currentTimeMillis + historyLifetime)/1000).toInt
            }

            val retries: Int = 5
            def attempt(cycle: Int) {
                if (cycle >= retries) {
                    log.error(s"Failed to persist '$action' event for document '$path' as: $json")
                } else {
                    client.add(s"/~history$path/$timestamp.$cycle", expire, json, persistGuarantee, replicateGuarantee).onComplete {
                        case Success(_) =>
                            log.debug(s"Persisted '$action' history event for document '$path' in ${cycle + 1} attempt(s).")
                        case Failure(_) =>
                            attempt(cycle + 1)
                    }
                }
            }
            attempt(0)
        }
    }

    private def retrieveHistoryView = {
        obtainView("history",
            loadResource("views/history-map.js"),
            loadResource("views/history-reduce.js"),
            permanent = true)
    }

    override def history(path: String, from: Date, to: Date, sortDirection: Direction)
                        (implicit user: String): Future[OpResult] = {
        val promise = Promise[OpResult]()

        val query = new Query()
        query.setIncludeDocs(true)
        query.setInclusiveEnd(true)
        query.setStale(Stale.FALSE)
        query.setReduce(false)

        val keyA = s"/~history$path/~${from.getTime}.0"
        val keyB = s"/~history$path/~${to.getTime}.\uefff"
        sortDirection match {
            case Ascending() =>
                query.setDescending(false)
                query.setRangeStart(keyA)
                query.setRangeEnd(keyB)
            case Descending() =>
                query.setDescending(true)
                query.setRangeStart(keyB)
                query.setRangeEnd(keyA)
        }

        retrieveHistoryView.onComplete {

            case Success(null) =>
                promise.success(OpDeferred())

            case Success(view) =>
                client.asyncQuery(view, query).onComplete {

                    case Success(result) =>
                        var historyList = parse("[]")
                        for (row <- result) {
                            historyList = historyList ++ parse(row.getDocument.toString)
                        }
                        promise.success(OpSuccess(historyList))

                    case Failure(failure) =>
                        log.error(s"Error executing query on view 'history': ${failure.getMessage}!")
                        invalidateViews()
                        promise.success(OpFailure(failure.getMessage))
                }

            case Failure(failure) =>
                log.error(s"Failed to obtain view 'history': ${failure.getMessage}!")
                promise.success(OpFailure(failure.getMessage))
        }

        promise.future
    }

    override def restore(path: String, targetDate: Date)
                        (implicit user: String): Future[OpResult] = {
        val promise = Promise[OpResult]()

        val query = new Query()
        query.setIncludeDocs(false)
        query.setStale(Stale.FALSE)
        query.setReduce(true)
        query.setInclusiveEnd(true)
        query.setDescending(false)
        query.setRangeStart(s"/~history$path/~")
        query.setRangeEnd(s"/~history$path/~${targetDate.getTime}.\uefff")

        def extractEntry(entry: String) {
            client.asyncGet(entry).onComplete {

                case Success(data) =>
                    val event = parse(data.toString).extract[Map[String, JValue]]
                    val document = event.getOrElse("finalJson", event.getOrElse("inputJson", JNothing))
                    if (event == null) {
                        promise.success(OpEmpty())
                    } else {
                        promise.success(OpSuccess(document))
                    }

                case Failure(failure) =>
                    promise.success(OpEmpty())
            }
        }

        retrieveHistoryView.onComplete {

            case Success(null) =>
                promise.success(OpDeferred())

            case Success(view) =>
                client.asyncQuery(view, query).onComplete {

                    case Success(result) =>
                        if (result.iterator.hasNext) {
                            val json = parse(result.iterator.next.getValue)
                            val entries = json.extract[List[Map[String, JValue]]]
                            val expireDate = entries.head("expire").extract[BigInt].toLong

                            if (expireDate != 0 && targetDate.getTime > expireDate) {
                                promise.success(OpEmpty())
                            } else {
                                extractEntry(entries.last("id").extract[String])
                            }
                        } else {
                            promise.success(OpEmpty())
                        }

                    case Failure(failure) =>
                        log.error(s"Error executing query on view 'history': ${failure.getMessage}!")
                        invalidateViews()
                        promise.success(OpFailure(failure.getMessage))
                }

            case Failure(failure) =>
                log.error(s"Failed to obtain view 'history': ${failure.getMessage}!")
                promise.success(OpFailure(failure.getMessage))
        }

        promise.future
    }

    // TODO: support multiple date/time formats
}
