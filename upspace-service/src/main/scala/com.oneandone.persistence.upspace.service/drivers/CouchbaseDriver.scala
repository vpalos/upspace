package com.oneandone.persistence.upspace.service.drivers

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import com.couchbase.client._
import com.oneandone.persistence.upspace.service.helpers._
import com.oneandone.persistence.upspace.service.drivers.couchbase._
import com.typesafe.config._
import net.spy.memcached._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.Predef._
import scala.util._
import sun.misc.BASE64Decoder

/**
 * This class hides all Couchbase related functionality.
 * Conceptually the same functionality could be implemented on an alternative
 * database system without the need to refactor the rest of the service code.
 *
 * An important thing to note is that this class must be thread-safe and, if
 * connection pooling is required, it should be implemented here opaquely.
 *
 * Furthermore, this driver must always take care to restore dead connections
 * which were closed automatically after long periods of idleness (timeouts)
 * and should not silence connection exceptions (i.e. should abort).
 */
class CouchbaseDriver(val spaceName: String, val connectionConfig: Config, val historyConfig: Config)
                     (implicit val log: LoggingAdapter, val actorSystem: ActorSystem)
    extends {
        protected val nodes = connectionConfig.getStringList("nodes")
        protected val bucket = connectionConfig.getString("bucket")

        protected val operationTimeout = connectionConfig.getMilliseconds("operation.timeout")

        protected val persistGuarantee = connectionConfig.getInt("guarantee.persist") match {
            case 0 => PersistTo.ZERO
            case 1 => PersistTo.ONE
            case 2 => PersistTo.TWO
            case 3 => PersistTo.THREE
            case 4 => PersistTo.FOUR
        }
        protected val replicateGuarantee = connectionConfig.getInt("guarantee.replicate") match {
            case 0 => ReplicateTo.ZERO
            case 1 => ReplicateTo.ONE
            case 2 => ReplicateTo.TWO
            case 3 => ReplicateTo.THREE
        }

        protected val client = {
            System.setProperty("viewmode", "production")
            //bucket password is base64 encoded in the config file
            val password = new String(new BASE64Decoder().decodeBuffer(connectionConfig.getString("password")))
            val uris = nodes map java.net.URI.create
            new CouchbaseClient(uris, bucket, password)
        }
    }
    with Driver
    with DriverHelper
    with CastHelper
    with Futurizer
    with Views
    with Linker
    with Search
    with History {

    implicit private val executionContext = actorSystem.dispatcher

    override def read(path: String, depth: Int, expire: Option[Expire] = None)
                    (implicit user: String): Future[OpResult] = {
        val nowMilliSeconds = System.currentTimeMillis

        expire match {
            case None =>
                futurize(client.asyncGet(path)) {
                    case null => OpMissing()
                    case document => renderDocumentDepth(document.toString, depth)
                }

            case Some(deadline) =>
                futurize(client.asyncGetAndTouch(path, deadline.asSeconds(nowMilliSeconds))) {
                    case null =>
                        OpMissing()
                    case cas: CASValue[_] =>
                        remember(user, path, "touch", nowMilliSeconds,
                            "expire" -> deadline.asDate(nowMilliSeconds)
                        )
                        // fixme: renderDocumentDepth should be completely asynchronous (event-based)
                        renderDocumentDepth(cas.getValue.toString, depth)
                }
        }
    }

    override def create(path: String, json: JValue, expire: Option[Expire] = None)
                       (implicit user: String): Future[OpResult] = {
        val inputJson = json.noNulls
        val nowMilliSeconds = System.currentTimeMillis

        futurize {
            expire match {
                case None =>
                    client.add(path, compact(render(inputJson)), persistGuarantee, replicateGuarantee)
                case Some(ts) =>
                    client.add(path, ts.asSeconds(nowMilliSeconds), compact(render(inputJson)), persistGuarantee, replicateGuarantee)
            }
        } {
            _.booleanValue() match {
                case true =>
                    expire match {
                        case Some(deadline) =>
                            remember(user, path, "create", nowMilliSeconds,
                                "inputJson" -> inputJson,
                                "expire" -> deadline.asDate(nowMilliSeconds)
                            )
                        case None =>
                            remember(user, path, "create", nowMilliSeconds,
                                "inputJson" -> inputJson)
                    }
                    OpSuccess()
                case false =>
                    OpConflict()
            }
        }
    }

    override def update(path: String, json: JValue,
                        expire: Option[Expire] = None, create: Boolean = false,
                        merge: Boolean = false, replaceLists: Boolean = true)
                       (implicit user: String): Future[OpResult] = {
        val nowMilliSeconds = System.currentTimeMillis
        val promise = Promise[OpResult]()

        def _set(mustExist: Boolean) = {
            val body = compact(render(json.noNulls))
            val setFuture = mustExist match {
                case true =>
                    expire match {
                        case None => client.replace(path, body, persistGuarantee, replicateGuarantee)
                        case Some(deadline) => client.replace(path, deadline.asSeconds(nowMilliSeconds), body, persistGuarantee, replicateGuarantee)
                    }
                case false =>
                    expire match {
                        case None => client.set(path, body, persistGuarantee, replicateGuarantee)
                        case Some(deadline) => client.set(path, deadline.asSeconds(nowMilliSeconds), body, persistGuarantee, replicateGuarantee)
                    }
            }
            setFuture.onComplete {

                case Success(result) =>
                    expire match {
                        case Some(deadline) =>
                            remember(user, path, "update", nowMilliSeconds,
                                "inputJson" -> json,
                                "create" -> merge,
                                "merge" -> merge,
                                "replaceLists" -> replaceLists,
                                "expire" -> deadline.asDate(nowMilliSeconds)
                            )
                        case None =>
                            remember(user, path, "update", nowMilliSeconds,
                                "create" -> merge,
                                "merge" -> merge,
                                "replaceLists" -> replaceLists,
                                "inputJson" -> json)
                    }
                    promise.success(OpSuccess())

                case Failure(failure) =>
                    promise.success(OpMissing())
            }
        }

        def _create() = {
            _set(mustExist = !create)
        }

        def _swap(newJson: JValue, casCode: Long) = {
            val body = compact(render(newJson))
            val swapFuture = expire match {
                case None =>
                    client.asyncCAS(path, casCode, body, client.getTranscoder)
                case Some(deadline) =>
                    client.asyncCAS(path, casCode, deadline.asSeconds(nowMilliSeconds), body, client.getTranscoder)
            }
            swapFuture.onComplete {

                case Success(result) =>
                    expire match {
                        case Some(deadline) =>
                            remember(user, path, "update", nowMilliSeconds,
                                "inputJson" -> json,
                                "finalJson" -> newJson,
                                "create" -> create,
                                "merge" -> merge,
                                "replaceLists" -> replaceLists,
                                "expire" -> deadline.asDate(nowMilliSeconds)
                            )
                        case None =>
                            remember(user, path, "update", nowMilliSeconds,
                                "inputJson" -> json,
                                "finalJson" -> newJson,
                                "create" -> create,
                                "merge" -> merge,
                                "replaceLists" -> replaceLists)
                    }
                    promise.success(OpSuccess())

                case Failure(failure) =>
                    create match {
                        case true => _create()
                        case false => promise.success(OpConflict())
                    }
            }
        }

        def _read() = {
            client.asyncGets(path).onComplete {

                case Success(result) =>
                    val oldJson = parse(result.getValue.toString)
                    val casCode = result.getCas

                    val newJson = {
                        replaceLists match {
                            case true =>
                                val noArray = json.transformField {
                                    case JField(name, JArray(_)) => (name, JNull)
                                }
                                oldJson merge noArray merge json
                            case false => oldJson merge json
                        }
                    }.noNulls
                    _swap(newJson, casCode)

                case Failure(failure) =>
                    _create()
            }
        }

        merge match {
            case true => _read()
            case false => _create()
        }

        promise.future
    }

    override def delete(path: String)
                       (implicit user: String): Future[OpResult] = {
        val nowMilliSeconds = System.currentTimeMillis
        futurize(client.delete(path, persistGuarantee, replicateGuarantee)) { result =>
            result.booleanValue() match {
                case true =>
                    remember(user, path, "delete", nowMilliSeconds)
                    OpSuccess()
                case false =>
                    OpMissing()
            }
        }
    }

    override def shutdown() {
        shutdownViews()
        client.shutdown()
    }

}
