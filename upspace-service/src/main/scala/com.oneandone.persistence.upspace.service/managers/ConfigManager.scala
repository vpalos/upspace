package com.oneandone.persistence.upspace.service.managers

import com.typesafe.config._
import scala.collection.JavaConversions._

/**
 * Loads and ensures global access to the UpSpace configuration file(s).
 */
object ConfigManager {

    private def mapEnabledConfigs(targetConfig: Config, path: String, fallbackConfig: Config = null) = {
        val items = targetConfig.getObject(path).keySet.toSet
        items.foldLeft(Map[String, Config]()) {
            (map, key) => {
                val itemConfig = targetConfig.getConfig(s"$path." + key)
                val enabled = itemConfig.getBoolean("enabled")
                if (enabled) {
                    map + (key -> itemConfig.withFallback(fallbackConfig))
                } else {
                    map
                }
            }
        }
    }

    def defaults = ConfigFactory.load("defaults")
    def reference = ConfigFactory.load()

    def options = reference.getConfig("options").withFallback(defaults.getConfig("options"))
    def spaces = mapEnabledConfigs(reference, "spaces", defaults.getConfig("space"))
    def users(securityConfig: Config) = mapEnabledConfigs(securityConfig, "users", defaults.getConfig("user"))

    val isHelp = options.getBoolean("help.enabled")
    val isRoot = options.getBoolean("root.enabled")

    private val validSpacePattern = "^/([^~/][^/]*)?$".r
    for (key <- spaces.keys) {
        assert(validSpacePattern.findFirstIn(key).nonEmpty, s"Illegal space name in configuration '$key'!")
    }

}
