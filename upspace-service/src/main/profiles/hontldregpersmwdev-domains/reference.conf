#
# UpSpace configuration.
#
# Notes:
# - See here for HOCON value formats (durations, sizes etc.):
#   https://github.com/typesafehub/config/blob/master/HOCON.md
#

#
# Service options.
#
options {

    #
    # Expose a Swagger JSON reference under the "/~help" URL
    # (regardless of space configuration). Interpret this URL
    # with a swagger-ui instance (http://swagger.wordnik.com).
    #
    help.enabled = true

    #
    # This is the maximum amount of time allowed to pass until the
    # response to the incoming request starts (default 20 seconds).
    #
    response.timeout = 20 seconds

    #
    # Limit the maximum allowed level of sub-document links.
    # The depth query parameter can be no higher than this option.
    #
    render.depth.maximum = 5

    #
    # When the root mode is enabled, the UpSpace service will only
    # expose the specified space at the root of the incoming URLs,
    # i.e. it will no longer read the space from first segment of
    # the URLs and all operations will be applied onto the space
    # specified as root; all other space definitions are discarded.
    #
    root {
        enabled = false
        space = "/domains"
    }

}

#
# Configure the spaces to be exposed by the UpSpace service.
# Each space definition contains its own distinct connection
# information, so the same UpSpace instance can expose spaces
# residing on different infrastructures.
#
spaces {

    /domains {
        enabled = true

        #
        # The connection sub section of a space configuration is specific to
        # a chosen driver (i.e. different drivers mean different properties.
        #
        connection {

            #
            # Couchbase driver connection configuration.
            #
            driver = couchbase

            #
            # Driver connection configuration.
            #
            nodes = [ "http://hontldregpersmwdev01.cname.lan:8091/pools",
                      "http://hontldregpersmwdev02.cname.lan:8091/pools" ]
            bucket = "domains"
            password = ""

            #
            # Purge search indexes if unused for longer than this period.
            # Default is one month (30 days). If this value is set to -1
            # then the indexes are never purged.
            #
            index.lifetime = 1 hour

            #
            # If querying an index takes more than this amount of time then
            # a 202 Accepted result is returned (the index is being updated).
            # The default is 10 seconds.
            #
            index.timeout = 10 seconds

            #
            # Maximum amount of time allowed for a single database operation.
            # Beware that some operations may be retried (default is 60 seconds).
            #
            operation.timeout = 60 seconds

            #
            # These are the guarantees enforced by UpSpace/Couchbase when performing
            # write operations (the operation blocks until the guarantees are met).
            # Beware that these options can have considerable impact on performance!
            #
            guarantee {

                #
                # Block until the data is persisted to disk on this many nodes.
                # Allowed values: 0, 1, 2, 3, 4. Default is 1.
                #
                persist = 0

                #
                # Block until the data is replicated (i.e. copied to but not
                # necessarily persisted to disk) on this many *additional* nodes.
                # Be aware that setting this value to more than 0 (zero) requires
                # the same amount of extra nodes for replication, otherwise write
                # operations will fail. Allowed values: 0, 1, 2, 3. Default is 1.
                #
                replicate = 0
            }
        }

        history {
            enabled = true
        }

    }

}
