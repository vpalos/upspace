import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.Keys._

name := "upspace-cli"

version := "1.0"

packageArchetype.java_application

packageDescription in Debian := "UpSpace Cli Utility Script"

maintainer in Debian := "Valeriu Paloş"

organization := "com.oneandone.persistence.upspace.cli"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.2.0"

libraryDependencies += "com.lambdaworks" % "scrypt" % "1.4.0"

