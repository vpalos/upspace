package com.oneandone.persistence.upspace.cli

case class Options(mode:     String = "",
                   action:   String = "",
                   cypher:   String = null,
                   password: String = "")