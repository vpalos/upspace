package com.oneandone.persistence.upspace.cli

import scopt._
import com.lambdaworks.crypto.SCryptUtil

object Main {

    val Name = "UpSpace CLI Utility Toolkit"
    val Version = "0.1"
    val Title = s"\n[ $Name, Version $Version ]\n"

    val parser = new OptionParser[Options](s"upspace-cli") {

        head(Title)

        cmd("scrypt")
            .valueName("<uri>")
            .text("Encrypt/verify passwords using SCrypt algorithm.")
            .action { (_, c) => c.copy(mode = "scrypt") }
            .children {

                opt[String]("hash")
                    .valueName("<cypher>")
                    .text("The SCrypt-encrypted hash to check against.")
                    .action { (x, c) => c.copy(cypher = x) }

                opt[String]("check")
                    .valueName("<password>")
                    .text("Check password against then given (SCrypt-encrypted) hash (i.e. --hash).")
                    .action { (x, c) => c.copy(action = "check", password = x) }

                opt[String]("encrypt")
                    .valueName("<password>")
                    .text("Encode password using SCrypt into an MCF-encoded string.")
                    .action { (x, c) => c.copy(action = "encrypt", password = x) }

        }

        version("version").text("prints the current version")

        help("help").text("print this usage information")

    }

    def main(args: Array[String]) {

        //println(SCryptUtil.check("testpwd", "$s0$e0801$55dx7ZAWyQpT4xOyaNqjpQ==$IXz2EB1EUYe8FNAtPf6kUERwRmXVTuJ51hd2Jkw1YZE="))
        //println(SCryptUtil.check("secret", "$s0$e0801$epIxT/h6HbbwHaehFnh/bw==$7H0vsXlY8UxxyW/BWx/9GuY7jEvGjT71GFd6O4SZND0="))

        parser.parse(args, Options()) map { options =>
            println(Title)

            options.mode match {
                case "scrypt" =>

                    options.action match {
                        case "encrypt" =>
                            println(SCryptUtil.scrypt(options.password, 16384, 8, 1))

                        case "check" =>
                            if (options.cypher == null) {
                                throw new IllegalArgumentException(s"Must specify --hash for --check action!")
                            }
                            SCryptUtil.check(options.password, options.cypher) match {
                                case true => println("Password matches successfully!")
                                case _ => println("Password does NOT match!")
                            }

                        case _ =>
                            throw new IllegalArgumentException(s"Unknown action: ${options.action}!")
                    }

                case _ =>
                    throw new IllegalArgumentException(s"Unknown command: ${options.mode}!")
            }

        } getOrElse {
            throw new IllegalArgumentException(s"Unable to understand options. Try '--help' for details!")
        }

    }

}
